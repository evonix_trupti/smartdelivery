package link.smartfood.deliveryperson.service;


import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.dashboard.HomeVM;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static link.smartfood.deliveryperson.network.Constant.DELIVERY_TRACKING_INTERVAL;
import static link.smartfood.deliveryperson.network.Constant.DELIVERY_TRACKING_SENT_INTERVAL;
import static link.smartfood.deliveryperson.network.Constant.IS_LOCATION_SERVICE_RUNNING;


public class LocationMonitoringService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = LocationMonitoringService.class.getSimpleName();
    EvonixUtil mEvonixUtil;
    GoogleApiClient mLocationClient;
    LocationRequest mLocationRequest = new LocationRequest();
    Location mLocation = null;
    Handler handler = new Handler();

    public static final String ACTION_LOCATION_BROADCAST = LocationMonitoringService.class.getName() + "LocationBroadcast";
    public static final long LOCATION_INTERVAL = 1000;
    public static final long FASTEST_LOCATION_INTERVAL = 1000;

    //Comment: For getting mLocation data per second
    private JsonObject jsonObjLocation = new JsonObject();
    private JsonArray jsonArrLocation = new JsonArray();
    private String strBatteryLevel = "", strBatteryState = "Battery Info";
    //Comment: For uploading mLocation data per 10 second
    private JsonObject jsonObjectMain = new JsonObject();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mEvonixUtil = new EvonixUtil(this);
        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest.setInterval(DELIVERY_TRACKING_INTERVAL);
        mLocationRequest.setFastestInterval(DELIVERY_TRACKING_INTERVAL);

        if (IS_LOCATION_SERVICE_RUNNING) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mLocation != null) {
                        set10SecondsRecall();
                    }
                    handler.postDelayed(this, DELIVERY_TRACKING_SENT_INTERVAL);
                }
            }, DELIVERY_TRACKING_SENT_INTERVAL);  //the time is in miliseconds
        } else {
            if (handler != null) {
                handler.removeMessages(0);
                handler.removeCallbacks(null);
                mLocation = null;
            }
        }

        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY; //by default
        //PRIORITY_BALANCED_POWER_ACCURACY, PRIORITY_LOW_POWER, PRIORITY_NO_POWER are the other priority modes

        mLocationRequest.setPriority(priority);
        mLocationClient.connect();
        //Make it stick to the notification panel so it is less prone to get cancelled by the Operating System.
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /*
     * LOCATION CALLBACKS
     */
    @Override
    public void onConnected(Bundle dataBundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            Log.d(TAG, "== Error On onConnected() Permission not granted");
            //Permission not granted by user so cancel the further execution.

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);

        Log.d(TAG, "Connected to Google API");
    }

    /*
     * Called by Location Services if the connection to the
     * mLocation client drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection suspended");
    }

    //to get the mLocation change
    @Override
    public void onLocationChanged(final Location location) {
        registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        Log.d(TAG, "Location changed");
        if (location != null) {
            Log.d(TAG, "== mLocation != null");
            //Send result to activities
            if (IS_LOCATION_SERVICE_RUNNING) {
                sendMessageToUI(location);
            } else {
                if (handler != null) {
                    handler.removeMessages(0);
                    handler.removeCallbacks(null);
                    mLocation = null;
                }
            }
        }
    }

    private void sendMessageToUI(final Location location) {
        mLocation = location;
        Log.d(TAG, "Sending info...");
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        jsonObjLocation.addProperty("latitude", location.getLatitude());
        jsonObjLocation.addProperty("longitude", location.getLongitude());
        jsonObjLocation.addProperty("altitude", location.getAltitude());
        jsonObjLocation.addProperty("timestamp", location.getTime());
        jsonObjLocation.addProperty("speed", location.getSpeed());
        jsonObjLocation.addProperty("horizontal_accuracy", location.getAccuracy());
        jsonObjLocation.addProperty("vertical_accuracy", 0);
        jsonObjLocation.addProperty("battery_state", strBatteryState);
        jsonObjLocation.addProperty("battery_level", strBatteryLevel);
        jsonObjLocation.add("orders", HomeVM.getOrderArray());
        jsonArrLocation.add(jsonObjLocation);
        Log.e("ArrayOfLocation", String.valueOf(jsonArrLocation));
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            int deviceStatus;
            deviceStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            int batteryLevel = (int) (((float) level / (float) scale) * 100.0f);/*
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);*/
            strBatteryLevel = (String.valueOf(batteryLevel)) + "%";

            if (deviceStatus == BatteryManager.BATTERY_STATUS_CHARGING) {
                strBatteryState = "Charging";
            }
            if (deviceStatus == BatteryManager.BATTERY_STATUS_DISCHARGING) {
                strBatteryState = "Discharging";
            }
            if (deviceStatus == BatteryManager.BATTERY_STATUS_FULL) {
                strBatteryState = "Battery Full";
            }
            if (deviceStatus == BatteryManager.BATTERY_STATUS_UNKNOWN) {
                strBatteryState = "Unknown";
            }
            if (deviceStatus == BatteryManager.BATTERY_STATUS_NOT_CHARGING) {
                strBatteryState = "Not Charging";
            }
        }
    };

    public void set10SecondsRecall() {
        mEvonixUtil.LE("10 seconds completed");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        String strTimestamp = simpleDateFormat.format(new Date());
        jsonObjectMain = new JsonObject();

        jsonObjectMain.addProperty("restaurant_id", mEvonixUtil.getSPString(getResources().getString(R.string.evonix_util_restaurant_id), ""));
        jsonObjectMain.addProperty("restaurant_address", mEvonixUtil.getSPString(getResources().getString(R.string.evonix_restaurant_address), ""));
        jsonObjectMain.addProperty("delivery_person_id", mEvonixUtil.getSPString(getResources().getString(R.string.evonix_util_login_id), ""));
        jsonObjectMain.addProperty("delivery_person_email", mEvonixUtil.getSPString(getResources().getString(R.string.evonix_util_email), ""));
        jsonObjectMain.addProperty("device_imei", mEvonixUtil.getSPString(getResources().getString(R.string.evonix_util_phone_imei), ""));
        jsonObjectMain.addProperty("current_latitude", mLocation.getLatitude());
        jsonObjectMain.addProperty("current_longitude", mLocation.getLongitude());
        jsonObjectMain.addProperty("current_timestamp", strTimestamp);
        jsonObjectMain.addProperty("current_altitude", mLocation.getAltitude());
        jsonObjectMain.addProperty("current_speed", mLocation.getSpeed());
        jsonObjectMain.add("locations", jsonArrLocation);
        Log.e("DUMMY_JSON", jsonObjectMain.toString());
        if (IS_LOCATION_SERVICE_RUNNING) {
            submitData();
        } else {
            if (handler != null) {
                handler.removeMessages(0);
                handler.removeCallbacks(null);
                mLocation = null;
            }
        }
    }

    public void submitData() {
        try {
            Call<String> call = RestAdapter.createAPI2().postLocationData(jsonObjectMain);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        jsonObjLocation = new JsonObject();
                        jsonArrLocation = new JsonArray();
                        mEvonixUtil.LE(String.valueOf(response.message() + " " + " Success"));
                    } else {
                        mEvonixUtil.LE(String.valueOf(response.message() + " " + " faaaill"));
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    mEvonixUtil.TES(t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Failed to connect to Google API");
    }

    @Override
    public void onDestroy() {
        try {
            if (this.mBatInfoReceiver != null)
                unregisterReceiver(this.mBatInfoReceiver);
            if (handler != null)
                handler.removeCallbacks(null);
        } catch (Exception e) {
        }
        super.onDestroy();
    }
}
