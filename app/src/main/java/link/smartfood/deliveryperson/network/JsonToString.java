package link.smartfood.deliveryperson.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.client.Response;


/**
 * Class Name        :  <b>JsonToString.java<b/>
 * Purpose           :  JsonToString .
 * Developed By      :  <b>@</b>
 * Created Date      :  <b>1/27/2016</b>
 * <p>
 * Modified Reason   :  <b></b>
 * Modified By       :  <b></b>
 * Modified Date     :  <b></b>
 * <p>
 */
public class JsonToString {
    private static JsonToString singleton = new JsonToString();

    private JsonToString() {
    }

    //TODO Static 'instance' method
    public static JsonToString getInstance() {
        return singleton;
    }

    //TODO Other methods protected by singleton-ness
    public static String convertToString(Response result) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String response = sb.toString();
        return response;
    }
}
