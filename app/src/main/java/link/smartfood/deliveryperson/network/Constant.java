package link.smartfood.deliveryperson.network;


import retrofit2.Retrofit;

/**
 * Class Name        :  <b>Constant.java<b/>
 * Purpose           :  Constant .
 * Developed By      :  <b>@</b>
 * Created Date      :  <b>1/27/2016</b>
 * <p>
 * Modified Reason   :  <b></b>
 * Modified By       :  <b></b>
 * Modified Date     :  <b></b>
 * <p>
 */
public class Constant {
//    public static final String BASE_URL = "http://democheck.in/smartfood/";
    //    public static final String DEV_BASE_URL = "http://wethealumni.com/smartfood/";
    public static final String DEV_BASE_URL = "http://demo-link.evonix.co/DeliveryApp/";
//    public static final String DEV_BASE_URL = "http://democheck.in/smartfood/";
    public static final String PROD_BASE_URL = "http://democheck.in/smartfood/";
    public static final String AWS_BASE_URL = "https://geo.smartfood.link/api/v1/tracking/";
    public static final String APP_TYPE = "ANDROID";

    public static final int RESULT_OK = 200;
    public static final int RESULT_FAIL = 515;
    public static final int RESULT_LOGOUT = 520;
    public static final int MY_REQUEST_CODE = 280;
    public static final int MY_REQUEST_OK = 281;
    public static final int MY_REQUEST_FAIL = 282;
    public static String ANDROID = "ANDROID";
    public static long DELIVERY_TRACKING_INTERVAL = 1000;
    public static long DELIVERY_TRACKING_SENT_INTERVAL = 10000;
    public static boolean IS_LOCATION_SERVICE_RUNNING = false;
    public static int TIME_OUT = 20;
    public static int TIME_OUT_20 = 20;
    public static int TIME_OUT_30 = 30;
    public static int TIME_OUT_40 = 40;
    public static int TIME_OUT_50 = 50;
    public static int READ_TIME_OUT = 20;
    // Retrofit Builder Object
    private static Retrofit.Builder retrofit_download = null, retrofit_new = null;
    private static boolean isDev = true;//isDev for the testing

    public static Retrofit.Builder getRetrofitInstanceNew() {
        if (retrofit_new == null) {
            retrofit_new = new Retrofit.Builder();
        }
        return retrofit_new;
    }

    public static Retrofit.Builder getRetrofitInstanceDownloadFile() {
        if (retrofit_download == null) {
            retrofit_download = new Retrofit.Builder();
        }
        return retrofit_download;
    }

    public static String getBasePath() {
        return ((isDev) ? DEV_BASE_URL : PROD_BASE_URL);
    }

    public static boolean isIsDev() {
        return isDev;
    }
}//end of class
