package link.smartfood.deliveryperson.network;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import link.smartfood.deliveryperson.activity.model.login.ChangePasswordWrapper;
import link.smartfood.deliveryperson.activity.model.login.CountryCodeWrapper;
import link.smartfood.deliveryperson.activity.model.login.ForgotPasswordWrapper;
import link.smartfood.deliveryperson.activity.model.login.LoginWrapper;
import link.smartfood.deliveryperson.activity.model.login.NetworkCodeWrapper;
import link.smartfood.deliveryperson.activity.model.login.ResetPasswordWrapper;
import link.smartfood.deliveryperson.activity.model.login.TermsofUseItem;
import link.smartfood.deliveryperson.activity.model.notfication.NotificationListWrapper;
import link.smartfood.deliveryperson.activity.model.order.OrderWrapper;
import link.smartfood.deliveryperson.activity.model.profile.DeliveryBoyDetailsWrapper;
import link.smartfood.deliveryperson.activity.model.profile.UpdateDeliveryBoyStatusWrapper;
import link.smartfood.deliveryperson.activity.model.profile.UpdateProfileWrapper;
import link.smartfood.deliveryperson.activity.model.status.OrderStatusWrapper;
import link.smartfood.deliveryperson.activity.model.status.UpdateStatusWrapper;
import link.smartfood.deliveryperson.activity.model.tracking.TrackingIntervalsWrapper;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Class Name        :  <b>RetrofitApi.java<b/>
 * Purpose           :  RetrofitApi .
 * Developed By      :  <b>@Shruti</b>
 * Created Date      :  <b>07/09/2019</b>
 * <p>
 * Modified Reason   :  <b></b>
 * Modified By       :  <b></b>
 * Modified Date     :  <b></b>
 * <p>
 */
public interface RetrofitApi {

    //Comment: Login
    @FormUrlEncoded
    @POST("deliveryBoy/deliverBoyLogin")
    Call<LoginWrapper> getLogin(
            @Field("app_type") String app_type,
            @Field("email") String email,
            @Field("password") String password,
            @Field("fcm_token") String fcm_token,
            @Field("imeino") String imeino,
            @Field("app_version") String app_version
    );

    //Comment: Verify Email
    @FormUrlEncoded
    @POST("deliveryBoy/verifyEmail")
    Call<JsonElement> getverifyEmail(
            @Field("app_type") String app_type,
            @Field("emailOtp") String emailOtp,
            @Field("mobileOtp") String mobileOtp,
            @Field("id") String id);

    //Comment: Forgot Password
    @FormUrlEncoded
    @POST("forgotPassword")
    Call<ForgotPasswordWrapper> forgotPassword(
            @Field("app_type") String app_type,
            @Field("mobileno") String mobileno,
            @Field("country_id") String country_id,
            @Field("network_id") String network_id,
            @Field("type") String type
    );

    //Comment: Change Password
    @FormUrlEncoded
    @POST("deliveryBoy/changePasssword")
    Call<ChangePasswordWrapper> changePasssword(
            @Field("app_type") String app_type,
            @Field("token") String token,
            @Field("oldpassword") String oldpassword,
            @Field("newpassword") String newpassword,
            @Field("newpassword_confirmation") String newpassword_confirmation
    );

    //Comment: Reset Password
    @FormUrlEncoded
    @POST("resetPassword")
    Call<ResetPasswordWrapper> resetPassword(
            @Field("app_type") String app_type,
            @Field("user_id") String user_id,
            @Field("OTP") String OTP,
            @Field("password") String password
            );

    //Comment: Terms And Cond
    @FormUrlEncoded
    @POST("contentTermsAndCond")
    Call<JsonElement> getContentTermsAndCond(
            @Field("app_type") String app_type,
            @Field("type") String type);

    //Comment: Privacy policy
    @FormUrlEncoded
    @POST("contentPrivatePolicy")
    Call<JsonElement> getContentPrivatePolicy(
            @Field("app_type") String app_type,
            @Field("type") String type);

    //Comment: Country Code
    @FormUrlEncoded
    @POST("countryCode")
    Call<CountryCodeWrapper> countryCode(
            @Field("app_type") String app_type);

    //Comment: Network Code
    @FormUrlEncoded
    @POST("networkCode")
    Call<NetworkCodeWrapper> networkCode(
            @Field("app_type") String app_type,
            @Field("country_id") String country_id
    );

    //Comment: Status List
    @FormUrlEncoded
    @POST("deliveryBoy/getStatus")
    Call<OrderStatusWrapper> getStatus(
            @Field("app_type") String appType,
            @Field("token") String token
    );

    //Comment: Order List
    @FormUrlEncoded
    @POST("deliveryBoy/getOrderList")
    Call<OrderWrapper> getOrderList(
            @Field("app_type") String appType,
            @Field("token") String token,
            @Field("orderType") String orderType,
            @Field("type") String type,
            @Field("offset") int offset,
            @Field("filter") String filter
    );

    //Comment: Update Order Status
    @FormUrlEncoded
    @POST("deliveryBoy/updateOrderStatus")
    Call<UpdateStatusWrapper> updateOrderStatus(
            @Field("app_type") String appType,
            @Field("token") String token,
            @Field("orderId") String orderId,
            @Field("statusId") String statusId,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude,
            @Field("comment") String comment
    );

    //Comment: Delivery Boy Details
    @FormUrlEncoded
    @POST("deliveryBoy/getDeliveryBoyDetails")
    Call<DeliveryBoyDetailsWrapper> getDeliveryBoyDetails(
            @Field("app_type") String appType,
            @Field("token") String token
    );

    //Comment: Update Delivery Status
    @FormUrlEncoded
    @POST("deliveryBoy/updateDeliveryPersonStatus")
    Call<UpdateDeliveryBoyStatusWrapper> updateDeliveryPersonStatus(
            @Field("app_type") String appType,
            @Field("token") String token,
            @Field("status") String status,
            @Field("comment") String comment           //(1 for active 4 for inactive 3 - vacation)
    );

    //Comment: Delivery Boy profile update
    @Multipart
    @POST("deliveryBoy/updateDeliveryBoy")
    Call<UpdateProfileWrapper> updateDeliveryBoyProfile(
            @Part("app_type") RequestBody appType,
            @Part("token") RequestBody token,
            @Part("firstName") RequestBody firstName,
            @Part("lastName") RequestBody lastName,
            @Part("country_id") RequestBody country_id,
            @Part("networkId") RequestBody networkId,
            @Part("mobile") RequestBody mobile,
            @Part("vehicleNo") RequestBody vehicleNo,
            @Part("address") RequestBody address,
            @Part("email") RequestBody email,
            @Part MultipartBody.Part file
    );

    //Comment: Notification List
    @FormUrlEncoded
    @POST("deliveryBoy/getNotification")
    Call<NotificationListWrapper> getNotification(
            @Field("app_type") String appType,
            @Field("token") String token,
            @Field("filter") String filter,
            @Field("offset") int offset

    );

    //Comment: App update
    @FormUrlEncoded
    @POST("updateApplicationAndroid")
    Call<JsonElement> updateApplicationAndroid(
            @Field("application_type") String application_type,//(0 for android 1 for IOS)
            @Field("app_version_code") String app_version_code,
            @Field("app_version_name") String app_version_name,
            @Field("app_build_version") String app_build_version,//(IOS)
            @Field("app_version_no") String app_version_no,//(IOS)
            @Field("update_priority") String update_priority,//(0 normal 1 high )
            @Field("type") String type // (typ-1 consumer or 2 restaurant)
    );

    //Comment: Location update
    @POST("store")
    Call<String> postLocationData(
            @Body JsonObject body);

    //Comment: Location update
    @POST("delivery/location/history")
    Call<JsonElement> getHistoryLocationData(
            @Body JsonObject body);

    //Comment: Tracking Intervals
    @FormUrlEncoded
    @POST("deliveryBoy/getTrackingIntervals")
    Call<TrackingIntervalsWrapper> getTrackingIntervals(
            @Field("app_type") String appType,
            @Field("token") String token
    );
}//end of interface
