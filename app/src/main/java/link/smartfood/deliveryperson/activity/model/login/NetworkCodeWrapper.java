package link.smartfood.deliveryperson.activity.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NetworkCodeWrapper {
    @SerializedName("status")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("networkCodeList")
    @Expose
    private List<NetworkCode> networkCode = new ArrayList();

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NetworkCode> getNetworkCode() {
        return networkCode;
    }

    public void setNetworkCode(List<NetworkCode> networkCode) {
        this.networkCode = networkCode;
    }

    public class NetworkCode {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("network_code")
        @Expose
        private String network_code;

        @SerializedName("master_country_id")
        @Expose
        private String master_country_id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNetwork_code() {
            return network_code;
        }

        public void setNetwork_code(String network_code) {
            this.network_code = network_code;
        }

        public String getMaster_country_id() {
            return master_country_id;
        }

        public void setMaster_country_id(String master_country_id) {
            this.master_country_id = master_country_id;
        }

    }
}
