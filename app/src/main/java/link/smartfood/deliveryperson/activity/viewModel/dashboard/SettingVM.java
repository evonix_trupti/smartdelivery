package link.smartfood.deliveryperson.activity.viewModel.dashboard;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import link.smartfood.deliveryperson.activity.view.auth.AccountInfoActivity;
import link.smartfood.deliveryperson.activity.view.auth.ChangePasswordActivity;
import link.smartfood.deliveryperson.databinding.FragmentSettingBinding;
import link.smartfood.deliveryperson.util.EvonixUtil;

public class SettingVM extends ViewModel {
    public FragmentSettingBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;


    public SettingVM(Activity mContext, FragmentSettingBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    public void onClick(int click) {
        switch (click) {
            case 1:
                mContext.startActivity(new Intent(mContext, AccountInfoActivity.class));
                break;
            case 2:
                mContext.startActivity(new Intent(mContext, ChangePasswordActivity.class));
                break;
        }
    }
}
