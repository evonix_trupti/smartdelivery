package link.smartfood.deliveryperson.activity.view.dashboard;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.view.auth.LoginActivity;
import link.smartfood.deliveryperson.activity.viewModel.dashboard.DashboardVM;
import link.smartfood.deliveryperson.activity.viewModel.dashboard.HomeVM;
import link.smartfood.deliveryperson.activity.viewModel.dashboard.OrderHistoryVM;
import link.smartfood.deliveryperson.adapter.DrawerAdapter;
import link.smartfood.deliveryperson.databinding.ActivityDashboardBinding;
import link.smartfood.deliveryperson.databinding.LeftDrawerLayoutBinding;
import link.smartfood.deliveryperson.util.EvonixBaseActivity;
import link.smartfood.deliveryperson.util.EvonixUtil;
import com.what3words.androidwrapper.What3WordsV3;
import com.what3words.javawrapper.request.Coordinates;
import com.what3words.javawrapper.response.ConvertTo3WA;

public class DashboardActivity extends EvonixBaseActivity {
    public static Handler h;
    ActivityDashboardBinding mBinding;
    static LeftDrawerLayoutBinding leftDrawerLayoutBinding;
    DashboardVM mViewModel;
    Menu menu;
    DrawerAdapter adapter;
    //navigation
    private FragmentManager fragmentManager;
    private Fragment fragment;
    private boolean isOutSideClicked;
    String strNavigationSelection = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bindingOfWidgets();
        try {
            finishByMSG();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void finishByMSG() {
        h = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                menu.findItem(R.id.action_cart).setVisible(false);
                menu.findItem(R.id.action_settings).setVisible(false);
                menu.findItem(R.id.action_edit).setVisible(false);
                strNavigationSelection = mEvonixUtil.getSPString(R.string.evonix_util_navigation_selection, "");
                Log.d("strNavigationSelection", strNavigationSelection);
                switch (strNavigationSelection) {
                    case "Home":
                        mBinding.toolbar.setTitle("Smart Delivery");
                        menu.findItem(R.id.action_settings).setVisible(true);
//                        fragment = SampleFragment.newInstance("f", "s");
                        fragment = HomeFragment.newInstance("f", "s");
                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                        break;
                    case "Order Delivery History":
                        mBinding.toolbar.setTitle("Order Delivery History");
                        menu.findItem(R.id.action_settings).setVisible(true);
                        fragment = OrderHistoryFragment.newInstance("OrderHistory", "s");
                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit);
                        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

                        break;
                    case "Settings":
                        mBinding.toolbar.setTitle("Settings");
                        fragment = SettingFragment.newInstance("Setting", "s");
                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                        break;
                    case "Notification":
                        mBinding.toolbar.setTitle("Notification");
                        fragment = NotificationFragment.newInstance("Notification", "s");
                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                        break;

                    case "Profile":
                        mBinding.toolbar.setTitle("Profile");
                        fragment = ProfileFragment.newInstance("Profile", "s");
                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                        break;

                    case "Share App":
                        try {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                            String sAux = "\nLet me recommend you this application\n\n";
                            sAux = sAux + "https://play.googleicon.com/store/apps/details?id=the.package.id\n\n";
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "choose one"));
                        } catch (Exception e) {
                            //e.toString();
                        }
                        break;
                    case "Logout":
                        try {
                            finish();
                            mEvonixUtil.setLogout();
                            startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
                        } catch (Exception e) {
                            //e.toString();
                        }
                        break;
                }
                mBinding.drawerLayout.closeDrawer(GravityCompat.START);
            }

        };
    }

    private void bindingOfWidgets() {
        mContext = DashboardActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new DashboardVM(((Activity) mContext), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(DashboardVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setDashboardVM(mViewModel);
        leftDrawerLayoutBinding = mBinding.leftLayout;
        mEvonixUtil = new EvonixUtil(mContext);
        initUI();
    }

    private void initUI() {
        mViewModel.requestMultiplePermissions();
        initNavigation();
        leftDrawerLayoutBinding.txtRestaurantName.setText(mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_restaurant_name), ""));
        leftDrawerLayoutBinding.txtUserName.setText(mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_full_name), ""));
        leftDrawerLayoutBinding.txtEmail.setText(mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_email), ""));

//        fragment = SampleFragment.newInstance("f", "s");
        fragment = HomeFragment.newInstance("f", "s");
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        leftDrawerLayoutBinding.setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.toolbar.setTitle("Settings");
                menu.findItem(R.id.action_edit).setVisible(false);
                fragment = SettingFragment.newInstance("f", "s");
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                mBinding.drawerLayout.closeDrawer(GravityCompat.START);
            }
        });
    }

    private void initNavigation() {
        mBinding.toolbar.setTitle("Smart Delivery");
        setSupportActionBar(mBinding.toolbar);

        if (mBinding.toolbar != null && mBinding.drawerLayout != null) {
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(DashboardActivity.this,
                    mBinding.drawerLayout, mBinding.toolbar, 0, 0) {
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    try {
                        mEvonixUtil.hideKeyboard();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            mBinding.drawerLayout.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
            try {
                adapter = new DrawerAdapter(DashboardActivity.this, getNavigationList());
                final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                leftDrawerLayoutBinding.drawerRecyclerView.setLayoutManager(layoutManager);
                leftDrawerLayoutBinding.drawerRecyclerView.setAdapter(adapter);
                setClickEvent();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.dashboard, menu);
        menu.findItem(R.id.action_cart).setVisible(false);
        menu.findItem(R.id.action_settings).setVisible(true);
        menu.findItem(R.id.action_edit).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            if (mEvonixUtil.getSPString(R.string.evonix_util_navigation_selection, "").equalsIgnoreCase("Home")) {
                HomeVM.h.sendEmptyMessage(0);
            } else {
                OrderHistoryVM.h.sendEmptyMessage(0);
            }
            return true;
        }
        if (id == R.id.action_cart) {
            menu.findItem(R.id.action_cart).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(false);
            menu.findItem(R.id.action_edit).setVisible(false);
            mBinding.toolbar.setTitle("Notification");
            fragment = NotificationFragment.newInstance("Notification", "s");
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setClickEvent() {
        if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) { //Your code here to check whether mBinding.drawerLayout is open or not.
                int[] contentLocation = new int[2];
                mBinding.drawerLayout.getLocationOnScreen(contentLocation);
                Rect rect = new Rect(contentLocation[0],
                        contentLocation[1],
                        contentLocation[0] + mBinding.drawerLayout.getWidth(),
                        contentLocation[1] + mBinding.drawerLayout.getHeight());

                if (!(rect.contains((int) event.getX(), (int) event.getY()))) {
                    isOutSideClicked = true;
                } else {
                    isOutSideClicked = false;
                }

            } else {
                return super.dispatchTouchEvent(event);
            }
        } else if (event.getAction() == MotionEvent.ACTION_DOWN && isOutSideClicked) {
            isOutSideClicked = false;
            return super.dispatchTouchEvent(event);
        } else if (event.getAction() == MotionEvent.ACTION_MOVE && isOutSideClicked) {
            return super.dispatchTouchEvent(event);
        }

        if (isOutSideClicked) {
            return true; //restrict the touch event here
        } else {
            return super.dispatchTouchEvent(event);
        }
    }

    public JSONArray getNavigationList() {
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 1; i < 8; i++) {
                JSONObject object = new JSONObject();
                object.put("id", i);
                object.put("status", "0");
                object.put("parent", i);
                object.put("icon", "");

                switch (i) {
                    case 1:
                        object.put("title", "Home");
                        object.put("icon", mContext.getResources().getString(R.string.font_home));
                        break;
                    case 2:
                        object.put("title", "Order Delivery History");
                        object.put("icon", mContext.getResources().getString(R.string.font_tags));
                        break;
                    case 3:
                        object.put("title", "Settings");
                        object.put("icon", mContext.getResources().getString(R.string.font_gear));
                        break;
                    case 4:
                        object.put("title", "Notification");
                        object.put("icon", mContext.getResources().getString(R.string.font_bell));
                        break;
                    case 5:
                        object.put("title", "Profile");
                        object.put("icon", mContext.getResources().getString(R.string.font_user));
                        break;
                    case 6:
                        object.put("title", "Share App");
                        break;
                    case 7:
                        object.put("title", "Logout");
                        break;
                }
                jsonArray.put(object);
            }
            //TODO 1 Home

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    @Override
    public void onBackPressed() {
//        finishAffinity();
        AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this);
        builder.setTitle("Are you sure you want to exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finishAffinity();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            leftDrawerLayoutBinding.txtUserName.setText(mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_full_name), ""));
            leftDrawerLayoutBinding.txtEmail.setText(mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_email), ""));
            if (!mEvonixUtil.getSPString(mContext.getResources().getString(R.string.profile_delivery_boy_profile_image), "").equals("")) {
                Picasso.get().load(mEvonixUtil.getSPString(mContext.getResources().getString(R.string.profile_delivery_boy_profile_image), "")).placeholder(R.drawable.ic_user).into(leftDrawerLayoutBinding.imageView);
            }
        } catch (Exception e) {
        }
    }

    public static void setProfileImage() {
        try {
            if (!mEvonixUtil.getSPString(mContext.getResources().getString(R.string.profile_delivery_boy_profile_image), "").equals("")) {
                Picasso.get().load(mEvonixUtil.getSPString(mContext.getResources().getString(R.string.profile_delivery_boy_profile_image), "")).placeholder(R.drawable.ic_user).into(leftDrawerLayoutBinding.imageView);
            }
        } catch (Exception e) {
        }
    }
}






