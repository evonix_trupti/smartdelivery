package link.smartfood.deliveryperson.activity.viewModel.auth;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;

import org.json.JSONObject;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.login.ResetPasswordWrapper;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.activity.view.auth.LoginActivity;
import link.smartfood.deliveryperson.databinding.ActivityResetPasswordBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordVM extends ViewModel {
    public ActivityResetPasswordBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;

    public ResetPasswordVM(Activity mContext, ActivityResetPasswordBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    public void onClick() {
        if (validate()) {
            getResetPassword();
        }
    }

    public TextWatcher newPassWatcher() {
            return new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (mBinding.inputPassword.getText().toString().trim().length() < 6) {
                        mBinding.inputPassword.setError("Password should be of 6 digits");
                        mBinding.inputLayoutPassword.setPasswordVisibilityToggleEnabled(false);
                    } else {
                        mBinding.inputLayoutPassword.setPasswordVisibilityToggleEnabled(true);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            };
    }

    public TextWatcher confirmPassWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!mBinding.inputPassword.getText().toString().trim().equals(mBinding.inputConfirmPassword.getText().toString().trim())) {
                    mBinding.inputConfirmPassword.setError("Password and confirm password must be same");
                    mBinding.inputLayoutConfirmPassword.setPasswordVisibilityToggleEnabled(false);
                } else {
                    mBinding.inputLayoutConfirmPassword.setPasswordVisibilityToggleEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
    }

    public TextWatcher otpWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!mBinding.inputOTP.getText().toString().equalsIgnoreCase("")) {
                    mBinding.inputLayoutOTP.setPasswordVisibilityToggleEnabled(true);
                } else {
                    mBinding.inputLayoutOTP.setPasswordVisibilityToggleEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
    }

    private boolean validate() {
        if (mBinding.inputPassword.getText().toString().trim().equals("")) {
            mBinding.inputPassword.requestFocus();
            mBinding.inputPassword.setFocusable(true);
            mBinding.inputPassword.setError("Please enter password");
                    return false;
        } else if (mBinding.inputConfirmPassword.getText().toString().trim().equals("")) {
            mBinding.inputConfirmPassword.requestFocus();
            mBinding.inputConfirmPassword.setFocusable(true);
            mBinding.inputConfirmPassword.setError("Please re-enter password");
                    return false;
        } else if (mBinding.inputOTP.getText().toString().trim().equals("")) {
            mBinding.inputOTP.requestFocus();
            mBinding.inputOTP.setFocusable(true);
            mBinding.inputOTP.setError("Please enter OTP");
                    return false;
        } else if (mBinding.inputPassword.getText().toString().trim().length() < 6) {
            mBinding.inputPassword.requestFocus();
            mBinding.inputPassword.setFocusable(true);
            mBinding.inputPassword.setError("Password should be of 6 digits");
                    return false;
        } else if (!mBinding.inputPassword.getText().toString().trim().equals(mBinding.inputConfirmPassword.getText().toString().trim())) {
            mBinding.inputConfirmPassword.requestFocus();
            mBinding.inputConfirmPassword.setFocusable(true);
            mBinding.inputConfirmPassword.setError("Password and confirm password must be same");
                    return false;
                }
                return true;
            }

    private void getResetPassword() {
        Call<ResetPasswordWrapper>
                call = RestAdapter.createAPI().resetPassword(
                Constant.ANDROID,
                mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_forgot_user_id), ""),
                mBinding.inputOTP.getText().toString().trim(),
                mBinding.inputConfirmPassword.getText().toString().trim());

        call.enqueue(new Callback<ResetPasswordWrapper>() {
            @Override
            public void onResponse(Call<ResetPasswordWrapper> call, Response<ResetPasswordWrapper> response) {
                try {
                    if (response.isSuccessful()) {
                        ResetPasswordWrapper mWrapper = response.body();
                        if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK) {
                            try {
                                mEvonixUtil.TES(mWrapper.getMessage());
                                mContext.startActivity(new Intent(mContext, LoginActivity.class));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        if (rootObject.getString("status").equals("520")) {
                            mEvonixUtil.setLogout();
                            mContext.finish();
                            mContext.startActivity(new Intent(mContext, SplashActivity.class));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
    }

}
