package link.smartfood.deliveryperson.activity.view.auth;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.auth.LoginVM;
import link.smartfood.deliveryperson.databinding.ActivityLoginBinding;
import link.smartfood.deliveryperson.util.EvonixBaseActivity;

public class LoginActivity extends EvonixBaseActivity {
    Typeface fontFamilyBold;
    LoginVM mViewModel;
    ActivityLoginBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingOfWidgets();
    }

    private void bindingOfWidgets() { mContext = LoginActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new LoginVM(((Activity) mContext), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(LoginVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setLoginViewModel(mViewModel);
        initUI();
    }

    public void initUI() {

    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}
