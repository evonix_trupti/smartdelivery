package link.smartfood.deliveryperson.activity.view.auth;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.auth.ResetPasswordVM;
import link.smartfood.deliveryperson.databinding.ActivityResetPasswordBinding;
import link.smartfood.deliveryperson.util.EvonixBaseActivity;

public class ResetPasswordActivity extends EvonixBaseActivity {
    ResetPasswordVM mViewModel;
    ActivityResetPasswordBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingOfWidgets();
    }

    private void bindingOfWidgets() {
        mContext = ResetPasswordActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new ResetPasswordVM(((Activity) mContext), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(ResetPasswordVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setResetPassword(mViewModel);
        initUI();
    }

    public void initUI() {
        setToolBar();
    }

    private void setToolBar() {
        setSupportActionBar(mViewModel.mBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mViewModel.mBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
