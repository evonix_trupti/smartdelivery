package link.smartfood.deliveryperson.activity.viewModel.auth;

import android.app.Activity;
import android.app.AlertDialog;
import androidx.lifecycle.ViewModel;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.login.CountryCodeWrapper;
import link.smartfood.deliveryperson.activity.model.login.ForgotPasswordWrapper;
import link.smartfood.deliveryperson.activity.model.login.NetworkCodeWrapper;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.activity.view.auth.ResetPasswordActivity;
import link.smartfood.deliveryperson.databinding.ActivityForgotPasswordBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordVM extends ViewModel {
    public ActivityForgotPasswordBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    private String strCountryCode = "", strNetworkCode = "";
    ArrayList<String> listNetwork = new ArrayList<String>();

    public ForgotPasswordVM(Activity mContext, ActivityForgotPasswordBinding binding) {
        this.mBinding = binding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
        mobileWatcher();
    }

    public void onClick() {
        if (validate()) {
            if (mEvonixUtil.isNetworkConnected()) {
                getForgetPassword();
            } else {
                mEvonixUtil.TES(mContext.getResources().getString(R.string.internet_is_not_connected));
            }
        }
    }

    public TextWatcher mobileWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mBinding.inputMobileNo.getText().toString().trim().length() < 7) {
                    mBinding.inputMobileNo.setError("Mobile Number should be minimum 7 digits");
                }
                /*if (mBinding.inputMobileNo.getText().toString().equalsIgnoreCase("0")) {
                    mBinding.inputMobileNo.setError("Mobile Number should be not start from 0");
                }*/
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
    }

    private boolean validate() {
        if (mBinding.inputMobileNo.getText().toString().trim().equalsIgnoreCase("")) {
            mBinding.inputMobileNo.requestFocus();
            mBinding.inputMobileNo.setFocusable(true);
            mBinding.inputMobileNo.setError("Please enter the Mobile number");
            return false;
        } else if (mBinding.inputMobileNo.getText().toString().trim().length() < 7) {
            mBinding.inputMobileNo.requestFocus();
            mBinding.inputMobileNo.setFocusable(true);
            mBinding.inputMobileNo.setError("Mobile Number should be minimum 7 digits");
            return false;
        } /*else if (mBinding.inputMobileNo.getText().toString().trim().startsWith("0")) {
            mBinding.inputMobileNo.requestFocus();
            mBinding.inputMobileNo.setFocusable(true);
            mBinding.inputMobileNo.setError("Mobile Number should be not start from 0");
            return false;
        }*/
        return true;
    }


    private void getForgetPassword() {
        Call<ForgotPasswordWrapper>
                call = RestAdapter.createAPI().forgotPassword(
                Constant.ANDROID,
                mBinding.inputMobileNo.getText().toString().trim(),
                strCountryCode,
                strNetworkCode,
                "3");                   //Comment: for delivery boy

        call.enqueue(new Callback<ForgotPasswordWrapper>() {
            @Override
            public void onResponse(Call<ForgotPasswordWrapper> call, Response<ForgotPasswordWrapper> response) {
                try {
                    if (response.isSuccessful()) {
                        ForgotPasswordWrapper mWrapper = response.body();
                        if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK && mWrapper.getForgotPassword() != null) {
                            try {
                                List<ForgotPasswordWrapper.ForgotPassword> forgotPasswordList = mWrapper.getForgotPassword();
                                for (ForgotPasswordWrapper.ForgotPassword forgotPassword : forgotPasswordList) {
                                    mEvonixUtil.setSPString(R.string.evonix_forgot_user_id, forgotPassword.getUser_id());
                                    Log.e("OTPPPP", forgotPassword.getOTP());
                                }
                                mContext.startActivity(new Intent(mContext, ResetPasswordActivity.class));//.putExtra("user_id", user_id));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        if (rootObject.getString("status").equals("520")) {
                            mEvonixUtil.setLogout();
                            mContext.finish();
                            mContext.startActivity(new Intent(mContext, SplashActivity.class));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
    }

    public void getCountryCode() {
        try {
            Call<CountryCodeWrapper>
                    call = RestAdapter.createAPI().countryCode(
                    Constant.ANDROID);

            call.enqueue(new Callback<CountryCodeWrapper>() {
                @Override
                public void onResponse(Call<CountryCodeWrapper> call, Response<CountryCodeWrapper> response) {
                    try {
                        if (response.isSuccessful()) {
                            CountryCodeWrapper mWrapper = response.body();
                            if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK && mWrapper.getCountryCode() != null) {
                                try {
                                    List<CountryCodeWrapper.CountryCode> countryList = mWrapper.getCountryCode();
                                    Gson gson = new Gson();
                                    String listString = gson.toJson(
                                            countryList,
                                            new TypeToken<ArrayList<CountryCodeWrapper.CountryCode>>() {
                                            }.getType());

                                    JSONArray jsonArray = new JSONArray(listString);
                                    setCountrySpinner(jsonArray);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            JSONObject rootObject = new JSONObject(response.errorBody().string());
                            mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void setCountrySpinner(final JSONArray jsonArray) {
                    try {
                        ArrayList<String> listCountry = new ArrayList<String>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            listCountry.add(jsonArray.getJSONObject(i).getString("code"));
                        }
                        ArrayAdapter aa = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item, listCountry);
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        mBinding.spCountryid.setAdapter(aa);
                        mBinding.spCountryid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                try {
                                    listNetwork.clear();
                                    strCountryCode = jsonArray.getJSONObject(position).getString("id");
                                    getNetworkCode(jsonArray.getJSONObject(position).getString("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                strCountryCode = "1";
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CountryCodeWrapper> call, Throwable t) {
                    mEvonixUtil.hideLoader();
                    mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
                }
            });
        } catch (Exception e) {
            mEvonixUtil.LE(e.getMessage());
        }
    }


    public void getNetworkCode(String strCountryCode) {
        try {
            Call<NetworkCodeWrapper>
                    call = RestAdapter.createAPI().networkCode(
                    Constant.ANDROID,
                    strCountryCode);

            call.enqueue(new Callback<NetworkCodeWrapper>() {
                @Override
                public void onResponse(Call<NetworkCodeWrapper> call, Response<NetworkCodeWrapper> response) {
                    try {
                        if (response.isSuccessful()) {
                            NetworkCodeWrapper mWrapper = response.body();
                            if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK && mWrapper.getNetworkCode() != null) {
                                try {
                                    listNetwork = new ArrayList<String>();
                                    List<NetworkCodeWrapper.NetworkCode> countryList = mWrapper.getNetworkCode();
                                    Gson gson = new Gson();
                                    String listString = gson.toJson(
                                            countryList,
                                            new TypeToken<ArrayList<NetworkCodeWrapper.NetworkCode>>() {
                                            }.getType());

                                    JSONArray jsonArray = new JSONArray(listString);
                                    setNetworkSpinner(jsonArray);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            JSONObject rootObject = new JSONObject(response.errorBody().string());
                            mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void setNetworkSpinner(final JSONArray jsonArray) {
                    try {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            listNetwork.add(jsonArray.getJSONObject(i).getString("network_code"));
                        }
                        ArrayAdapter aa = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item, listNetwork);
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        mBinding.spNetworkid.setAdapter(aa);
                        mBinding.spNetworkid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                try {
                                    strNetworkCode = jsonArray.getJSONObject(position).getString("id");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                strNetworkCode = "1";
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<NetworkCodeWrapper> call, Throwable t) {
                    mEvonixUtil.hideLoader();
                    mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));

                }
            });
        } catch (Exception e) {
            mEvonixUtil.LE(e.getMessage());
        }
    }


    public void showAlertDialog(String title, String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(
                mContext,
                R.style.AlertDialogCustom_Destructive)
                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        mContext.finish();
                    }
                })
                .setCancelable(false)
                .setNegativeButton("Re-check", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (mEvonixUtil.isNetworkConnected()) {
                            dialogInterface.cancel();
                            //CountryCode();
                        } else {
                            showAlertDialog("Check Your Internet Connection ", "No Internet connection available at this moment!Please check the internet connection.");
                        }

                    }
                })
                .setTitle(title).setMessage(message).create();
        alertDialog.show();
    }
}
