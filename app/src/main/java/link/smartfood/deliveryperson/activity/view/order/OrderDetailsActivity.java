package link.smartfood.deliveryperson.activity.view.order;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.order.OrderList;
import link.smartfood.deliveryperson.activity.viewModel.OrderDetailsVM;
import link.smartfood.deliveryperson.databinding.ActivityOrderDetailsBinding;
import link.smartfood.deliveryperson.util.EvonixBaseActivity;

public class OrderDetailsActivity extends EvonixBaseActivity {
    OrderDetailsVM mViewModel;
    ActivityOrderDetailsBinding mBinding;
    private OrderList orderList;
    private String strTimer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingOfWidgets();
        setToolBar();
    }

    private void bindingOfWidgets() {
        mContext = OrderDetailsActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_details);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new OrderDetailsVM(((Activity) mContext), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(OrderDetailsVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setOrderDetailViewModel(mViewModel);
        getIntentData();
        initUI();
    }

    private void getIntentData() {
        orderList = (OrderList) getIntent().getSerializableExtra("listData");
        strTimer = getIntent().getStringExtra("timer");
    }

    public void initUI() {
        OrderList dataModel = orderList;
        mBinding.setOrderWrapper(dataModel);
        mViewModel.setTimer(dataModel,strTimer);
        mViewModel.setRecycler(dataModel.getFoodList());
        mViewModel.setStatusColor(dataModel);
    }



    private void setToolBar() {
        setSupportActionBar(mBinding.toolbar);
        if (getSupportActionBar() != null) {
            mBinding.toolbar.setTitle("Order Details");
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
