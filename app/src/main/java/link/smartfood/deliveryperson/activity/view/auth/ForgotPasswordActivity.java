package link.smartfood.deliveryperson.activity.view.auth;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.auth.ForgotPasswordVM;
import link.smartfood.deliveryperson.databinding.ActivityForgotPasswordBinding;
import link.smartfood.deliveryperson.util.EvonixBaseActivity;

public class ForgotPasswordActivity extends EvonixBaseActivity {
    ForgotPasswordVM mViewModel;
    ActivityForgotPasswordBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingOfWidgets();
    }

    private void bindingOfWidgets() {
        mContext = ForgotPasswordActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new ForgotPasswordVM(((Activity) mContext), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(ForgotPasswordVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setForgotPassword(mViewModel);
        initUI();
    }

    public void initUI() {
        setToolBar();
        mViewModel.getCountryCode();
    }

    private void setToolBar() {
        setSupportActionBar(mViewModel.mBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mViewModel.mBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        mEvonixUtil.hideKeyboard();
        startActivity(new Intent(this, LoginActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
