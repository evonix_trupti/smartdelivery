package link.smartfood.deliveryperson.activity.view.dashboard;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.dashboard.HomeVM;
import link.smartfood.deliveryperson.activity.viewModel.dashboard.SampleVM;
import link.smartfood.deliveryperson.databinding.HomeFragmentBinding;
import link.smartfood.deliveryperson.databinding.SampleFragmentBinding;
import link.smartfood.deliveryperson.util.EvonixUtil;

public class SampleFragment extends Fragment {
    private SampleVM mViewModel;
    SampleFragmentBinding mBinding;
    private Handler handler;
    private Runnable runnable;
    private EvonixUtil mEvonixUtil;
    private Context mContext;

    public static SampleFragment newInstance(String param1, String param2) {
        return new SampleFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.sample_fragment, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new SampleVM(((Activity) getActivity()), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(SampleVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setSampleViewModel(mViewModel);
        initUI();
    }

    private void initUI() {
        mContext = getActivity();
        mEvonixUtil = new EvonixUtil(mContext);
        mViewModel.setShowCase(getActivity());
        mViewModel.finishByMSG();
        mViewModel.setRecycler(this);
//        setOrderHandler(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewModel.checkInternet();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (handler != null) {
            handler.removeMessages(0);
            handler.removeCallbacks(null);
        }
    }

    public void setOrderHandler(final SampleFragment homeFragment) {
        handler = new Handler();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                //Do your refreshing
                try {
                    if (mEvonixUtil.isNetworkConnected()) {
                        mViewModel.h.sendEmptyMessage(2);

                    } else {
                        mEvonixUtil.TES(mContext.getResources().getString(R.string.internet_is_not_connected));
                    }
                    handler.postDelayed(runnable, 10000);
                    Log.e("In HANDLER", ":(");
                } catch (Exception e) {

                }
            }//end of run method

        }, 10000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        mViewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
