package link.smartfood.deliveryperson.activity.view.auth;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.auth.TermsOfUseViewModel;
import link.smartfood.deliveryperson.databinding.ActivityPrivacyPolicyBinding;
import link.smartfood.deliveryperson.util.EvonixBaseActivity;

public class TermsOfUseActivity extends EvonixBaseActivity {
    TermsOfUseViewModel mViewModel;
    ActivityPrivacyPolicyBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingOfWidgets();
        setToolBar();
    }

    private void bindingOfWidgets() {
        mContext = TermsOfUseActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_privacy_policy);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new TermsOfUseViewModel(((Activity) mContext), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(TermsOfUseViewModel.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setTnCViewModel(mViewModel);
        initUI();
    }

    private void initUI() {
        mViewModel.setContentView();
    }

    private void setToolBar() {
        setSupportActionBar(mBinding.toolbar);
        if (getSupportActionBar() != null) {
            mBinding.toolbar.setTitle("Term of Use");
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }
}
