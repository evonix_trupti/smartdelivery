package link.smartfood.deliveryperson.activity.model.login;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class LoginWrapper {
    @SerializedName("status")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Login> loginData = new ArrayList();

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Login> getLoginData() {
        return loginData;
    }

    public void setLoginData(List<Login> loginData) {
        this.loginData = loginData;
    }

    public class Login {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("deliveryBoyId")
        @Expose
        private String deliveryBoyId;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("token")
        @Expose
        private String token;

        @SerializedName("verified")
        @Expose
        private String verified;

        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("phone")
        @Expose
        private String phone;

        @SerializedName("restaurant_id")
        @Expose
        private String restaurant_id;

        @SerializedName("restaurantName")
        @Expose
        private String restaurantName;

        @SerializedName("restaurantAddress")
        @Expose
        private String restaurantAddress;

        @SerializedName("image")
        @Expose
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDeliveryBoyId() {
            return deliveryBoyId;
        }

        public void setDeliveryBoyId(String deliveryBoyId) {
            this.deliveryBoyId = deliveryBoyId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getVerified() {
            return verified;
        }

        public void setVerified(String verified) {
            this.verified = verified;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getRestaurantName() {
            return restaurantName;
        }

        public void setRestaurantName(String restaurantName) {
            this.restaurantName = restaurantName;
        }

        public String getRestaurantAddress() {
            return restaurantAddress;
        }

        public void setRestaurantAddress(String restaurantAddress) {
            this.restaurantAddress = restaurantAddress;
        }
        public String getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(String restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
