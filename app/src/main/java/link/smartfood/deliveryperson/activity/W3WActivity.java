package link.smartfood.deliveryperson.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import link.smartfood.deliveryperson.R;
import com.what3words.androidwrapper.What3WordsV3;
import com.what3words.javawrapper.request.Coordinates;
import com.what3words.javawrapper.response.APIResponse;
import com.what3words.javawrapper.response.Autosuggest;
import com.what3words.javawrapper.response.ConvertTo3WA;
import com.what3words.javawrapper.response.ConvertToCoordinates;

public class W3WActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_w3w );

//        What3WordsV3 api = new What3WordsV3( "what3words-4RGXD5E5", getApplicationContext() );
//
//
//        // Convert coordinates to a 3 word address
//        ConvertTo3WA words = api.convertTo3wa( new Coordinates( 51.508344, -0.12549900 ) )
//                .language( "en" )
//                .execute();
//        System.out.println( "Words: " + words );

//        ConvertToCoordinates coordinates = api.convertToCoordinates("filled.count.soap")
//                .execute();
//        System.out.println("Coordinates: " + coordinates);


        ((Button) findViewById( R.id.btnSet )).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new Thread( new Runnable() {
//                    public void run() {
//
//                         What3WordsV3 api = new What3WordsV3( "4RGXD5E5", getApplicationContext() );
//
//                        ConvertTo3WA words = api.convertTo3wa(new Coordinates(18.6011, 73.7641))
//                                .language("en")
//                                .execute();
//                        System.out.println("Words: " + words);
//                        // For all requests a what3words API key is needed
////                        What3WordsV3 api = new What3WordsV3( "4RGXD5E5", W3WActivity.this );
////
////                        Autosuggest autosuggest = api.autosuggest( "freshen.overlook.clo" )
////                                .clipToCountry( "FR" )
////                                .focus( new Coordinates( 48.856618, 2.3522411 ) )
////                                .nResults( 1 )
////                                .execute();
////
////                        if (autosuggest.isSuccessful()) {
////                            String words = autosuggest.getSuggestions().get( 0 ).getWords();
////                            System.out.printf( "Top 3 word address match: %s%n", words );
////
////                            ConvertToCoordinates convertToCoordinates = api.convertToCoordinates( words ).execute();
////                            if (convertToCoordinates.isSuccessful()) {
////                                System.out.printf( "WGS84 Coordinates: %f, %f%n",
////                                        convertToCoordinates.getCoordinates().getLat(),
////                                        convertToCoordinates.getCoordinates().getLng() );
////                                System.out.printf( "Nearest Place: %s%n", convertToCoordinates.getNearestPlace() );
////                            } else {
////                                APIResponse.What3WordsError error = autosuggest.getError();
////                                if (error == APIResponse.What3WordsError.INTERNAL_SERVER_ERROR) { // Server Error
////                                    System.out.println( "InternalServerError: " + error.getMessage() );
////
////                                } else if (error == APIResponse.What3WordsError.NETWORK_ERROR) { // Network Error
////                                    System.out.println( "NetworkError: " + error.getMessage() );
////
////                                }
////                            }
////                        } else {
////                            APIResponse.What3WordsError error = autosuggest.getError();
////
////                            if (error == APIResponse.What3WordsError.BAD_CLIP_TO_COUNTRY) { // Invalid country clip is provided
////                                System.out.println( "BadClipToCountry: " + error.getMessage() );
////
////                            } else if (error == APIResponse.What3WordsError.BAD_FOCUS) { // Invalid focus
////                                System.out.println( "BadFocus: " + error.getMessage() );
////
////                            } else if (error == APIResponse.What3WordsError.BAD_N_RESULTS) { // Invalid number of results
////                                System.out.println( "BadNResults: " + error.getMessage() );
////
////                            } else if (error == APIResponse.What3WordsError.INTERNAL_SERVER_ERROR) { // Server Error
////                                System.out.println( "InternalServerError: " + error.getMessage() );
////
////                            } else if (error == APIResponse.What3WordsError.NETWORK_ERROR) { // Network Error
////                                System.out.println( "NetworkError: " + error.getMessage() );
////
////                            } else {
////                                System.out.println( error + ": " + error.getMessage() );
////
////                            }
////                        }
////                    }
////                } ).start();
//            }
//        } );
    }

});
    }
}
