package link.smartfood.deliveryperson.activity.viewModel.dashboard;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.order.OrderList;
import link.smartfood.deliveryperson.activity.model.order.OrderWrapper;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.activity.view.dashboard.OrderHistoryFragment;
import link.smartfood.deliveryperson.adapter.HomeAdapter;
import link.smartfood.deliveryperson.databinding.FragmentMyOrderBinding;
import link.smartfood.deliveryperson.databinding.LayoutListBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EndlessRecyclerViewScrollListener;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderHistoryVM extends ViewModel {
    public FragmentMyOrderBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    private ArrayList<OrderList> orderList = new ArrayList<>();
    private MutableLiveData<ArrayList<OrderList>> mutableLiveData = new MutableLiveData<>();
    private String searchField = "",strFilter = "";
    public static Handler h;
    private int offSet = 0, totalRecord = 0;
    HomeAdapter adapter;
    private ArrayList<OrderList> orderLists;
    LayoutListBinding layoutListBinding;
    private LinearLayoutManager linearLayoutManager;
    Call<OrderWrapper> call = null;


    public OrderHistoryVM(Activity mContext, FragmentMyOrderBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
        layoutListBinding = mBinding.layoutList;
        initSearchView();
    }

    public void finishByMSG() {
        try {
            h = new Handler() {
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case 0:
                            setSearchView();
                            break;
                    }
                }
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSearchView() {
        //Comment: Search for recyclerview
        if (layoutListBinding.searchView.getVisibility() == View.VISIBLE) {
            offSet = 0;
            layoutListBinding.searchView.setVisibility(View.GONE);
            layoutListBinding.searchView.setQuery("", false);
            searchField = "";
            mEvonixUtil.hideKeyboard();
        } else {
            layoutListBinding.searchView.setVisibility(View.VISIBLE);
            searchField = "";
        }
    }

    private void initSearchView() {
        layoutListBinding.searchView.setQueryHint("Enter at least 3 alphabets for better search");
        layoutListBinding.searchView.setIconified(true);
        layoutListBinding.searchView.onActionViewExpanded();
        layoutListBinding.searchView.clearFocus();
        layoutListBinding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 2) {
                    if (call != null && call.isExecuted()) {
                        if (newText.trim().equals("")) {
                            return true;
                        }
                        call.cancel();
                    }
                    searchField = newText.trim();
                    offSet = 0;
                    mEvonixUtil.hideKeyboard();
                    notifyList();
                    mEvonixUtil.showLoader(mContext);
                    getMutableLiveData();
                }
                return true;
            }
        });
        layoutListBinding.searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                return false;
            }
        });
    }

    public void onClick(String click) {
        strFilter = click;
        switch (click) {
            case "":
                setLayout(mBinding.txtAll,mBinding.txtToday,mBinding.txtLastWeek,mBinding.txtYesterday,mBinding.txtLastMonth,mBinding.txtLastYear);
                break;
            case "today":
                setLayout(mBinding.txtToday,mBinding.txtAll,mBinding.txtLastWeek,mBinding.txtYesterday,mBinding.txtLastMonth,mBinding.txtLastYear);
                break;
            case "yesterday":
                setLayout(mBinding.txtYesterday,mBinding.txtToday,mBinding.txtLastWeek,mBinding.txtAll,mBinding.txtLastMonth,mBinding.txtLastYear);
                break;
            case "week":
                setLayout(mBinding.txtLastWeek,mBinding.txtToday,mBinding.txtYesterday,mBinding.txtAll,mBinding.txtLastMonth,mBinding.txtLastYear);
                break;
            case "month":
                setLayout(mBinding.txtLastMonth,mBinding.txtToday,mBinding.txtLastWeek,mBinding.txtAll,mBinding.txtYesterday,mBinding.txtLastYear);
                break;
            case "year":
                setLayout(mBinding.txtLastYear,mBinding.txtToday,mBinding.txtLastWeek,mBinding.txtAll,mBinding.txtLastMonth,mBinding.txtYesterday);
                break;
        }
    }

    private void setLayout(TextView txtSelected, TextView txtRest1, TextView txtRest2, TextView txtRest3, TextView txtRest4, TextView txtRest5) {
        offSet = 0;
        getAllOrder();
        txtSelected.setBackground(mContext.getResources().getDrawable(R.drawable.button_border));
        txtRest1.setBackground(mContext.getResources().getDrawable(R.drawable.rectangular_border));
        txtRest2.setBackground(mContext.getResources().getDrawable(R.drawable.rectangular_border));
        txtRest3.setBackground(mContext.getResources().getDrawable(R.drawable.rectangular_border));
        txtRest4.setBackground(mContext.getResources().getDrawable(R.drawable.rectangular_border));
        txtRest5.setBackground(mContext.getResources().getDrawable(R.drawable.rectangular_border));

        txtSelected.setTextColor(mContext.getResources().getColor(R.color.white_1000));
        txtRest1.setTextColor(mContext.getResources().getColor(R.color.black_1000));
        txtRest2.setTextColor(mContext.getResources().getColor(R.color.black_1000));
        txtRest3.setTextColor(mContext.getResources().getColor(R.color.black_1000));
        txtRest4.setTextColor(mContext.getResources().getColor(R.color.black_1000));
        txtRest5.setTextColor(mContext.getResources().getColor(R.color.black_1000));
    }


    public LiveData<ArrayList<OrderList>> getAllOrder() {
        mEvonixUtil.showLoader(mContext);
        return getMutableLiveData();
    }

    public void setRecycler(OrderHistoryFragment homeFragment) {
        getAllOrder().observe(homeFragment, new Observer<ArrayList<OrderList>>() {
            @Override
            public void onChanged(@Nullable ArrayList<OrderList> orderLists) {
                mEvonixUtil.setSPString(mContext.getResources().getString(R.string.dashboard_list_from),"history");
                linearLayoutManager = new LinearLayoutManager(mContext);
                layoutListBinding.progressBarLoadMore.setVisibility(View.GONE);
                layoutListBinding.recyclerView.setLayoutManager(linearLayoutManager);
                adapter = new HomeAdapter(mContext, orderLists);
                layoutListBinding.recyclerView.setAdapter(adapter);
                layoutListBinding.swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_green_light,
                        android.R.color.holo_blue_bright,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
                layoutListBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        layoutListBinding.searchView.setVisibility(View.GONE);
                        searchField = "";
                        offSet = 0;
                        getMutableLiveData();
                    }
                });

                layoutListBinding.recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        if (offSet <= totalRecord) {
                            offSet = offSet + visibleThreshold;
                            getMutableLiveData();
                        }
                    }
                });
            }
        });
    }

    public MutableLiveData<ArrayList<OrderList>> getMutableLiveData() {
        if (offSet < 1) {
            layoutListBinding.progressBarLoadMore.setVisibility(View.GONE);
        } else if (offSet <= totalRecord) {
            layoutListBinding.progressBarLoadMore.setVisibility(View.VISIBLE);
        } else {
            return mutableLiveData;
        }
        call = RestAdapter.createAPI().getOrderList(
                Constant.ANDROID,
                mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token), ""),
                "history",
                searchField,
                offSet,
                strFilter);

        call.enqueue(new Callback<OrderWrapper>() {
            @Override
            public void onResponse(Call<OrderWrapper> call, Response<OrderWrapper> response) {
                OrderWrapper mBlogWrapper = response.body();
                try {
                    if (response.isSuccessful()) {
                        if (mBlogWrapper != null) {
                            offSet = mBlogWrapper.getOffset();
                            totalRecord = mBlogWrapper.getTotal();
                            if (mBlogWrapper.getOrderList() != null) {
                                if (mBlogWrapper.getOffset() < 1) {
                                    orderList = (ArrayList<OrderList>) mBlogWrapper.getOrderList();
                                    mutableLiveData.setValue(orderList);
                                } else {
                                    for (int i = 0; i < mBlogWrapper.getOrderList().size(); i++)
                                        orderList.add(mBlogWrapper.getOrderList().get(i));
                                }
                            }else {
                                orderList = (ArrayList<OrderList>) mBlogWrapper.getOrderList();
                            }
                        }
                        notifyList();
                    } else {
                        mEvonixUtil.hideLoader();
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        if (rootObject.getString("status").equals("520")) {
                            mEvonixUtil.setLogout();
                            mContext.finish();
                            mContext.startActivity(new Intent(mContext, SplashActivity.class));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<OrderWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
        return mutableLiveData;
    }

    public void notifyList() {
        if (orderList != null) {
            layoutListBinding.recyclerView.setVisibility(View.VISIBLE);
            adapter.setList(orderList);
            layoutListBinding.rlNoData.setVisibility(View.GONE);
        } else {
            layoutListBinding.recyclerView.setVisibility(View.GONE);
            layoutListBinding.rlNoData.setVisibility(View.VISIBLE);
        }
        mEvonixUtil.hideLoader();
        layoutListBinding.swipeRefreshLayout.setRefreshing(false);
        layoutListBinding.progressBarLoadMore.setVisibility(View.GONE);
    }
}
