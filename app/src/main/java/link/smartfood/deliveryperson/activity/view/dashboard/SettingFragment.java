package link.smartfood.deliveryperson.activity.view.dashboard;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.dashboard.SettingVM;
import link.smartfood.deliveryperson.databinding.FragmentSettingBinding;
import link.smartfood.deliveryperson.util.EvonixUtil;

public class SettingFragment extends Fragment {
    SettingVM mViewModel;
    FragmentSettingBinding mBinding;
    private String mParam1;
    private String mParam2;

    public static SettingFragment newInstance(String param1, String param2) {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        args.putString(EvonixUtil.ARG_PARAM1, param1);
        args.putString(EvonixUtil.ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(EvonixUtil.ARG_PARAM1);
            mParam2 = getArguments().getString(EvonixUtil.ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new SettingVM(((Activity) getActivity()), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(SettingVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setSettingViewModel(mViewModel);
        initUI();
    }

    private void initUI() {
    }

}
