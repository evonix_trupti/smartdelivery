package link.smartfood.deliveryperson.activity.viewModel.auth;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONObject;

import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.login.LoginWrapper;
import link.smartfood.deliveryperson.activity.view.auth.ForgotPasswordActivity;
import link.smartfood.deliveryperson.activity.view.auth.PrivacyPolicy;
import link.smartfood.deliveryperson.activity.view.auth.TermsOfUseActivity;
import link.smartfood.deliveryperson.activity.view.dashboard.DashboardActivity;
import link.smartfood.deliveryperson.databinding.ActivityLoginBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginVM extends ViewModel {
    public ActivityLoginBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    public String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+", imeiNo = "0000";

    public LoginVM(Activity mContext, ActivityLoginBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
        FirebaseApp.initializeApp(mContext);
    }

   //Comment:
    public void onClick(int click) {
        switch (click) {
            case 1:
                if (validate()) {
                    mEvonixUtil.hideKeyboard();
                    if (mBinding.chkboxTermsPrivacy.isChecked()) {
                        getLoginData();
                    } else {
                        mEvonixUtil.TES("Please agree to SmartFood Privacy Policy and Terms of Use");
                    }
                }
                break;
            case 2:
                mContext.finish();
                mContext.startActivity(new Intent(mContext, ForgotPasswordActivity.class));
                mContext.overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case 3:
                mContext.startActivity(new Intent(mContext, PrivacyPolicy.class));
                mContext.overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case 4:
                mContext.startActivity(new Intent(mContext, TermsOfUseActivity.class));
                mContext.overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
        }
    }

    public TextWatcher emailWatcher() {
        return new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mBinding.inputEmail.getText().toString().trim().matches(emailPattern)) {
                } else {
                    mBinding.inputEmail.setError("Invalid Email Address");
                }
            }

            @Override public void afterTextChanged(Editable editable) {
            }
        };
    }

    public TextWatcher passwordWatcher() {
        return new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mBinding.inputPassword.getText().toString().trim().length() < 6) {
                    mBinding.inputLayoutPassword.setPasswordVisibilityToggleEnabled(false);
                    mBinding.inputPassword.setError("Password should be of 6 digits");
                } else {
                    mBinding.inputLayoutPassword.setPasswordVisibilityToggleEnabled(true);
                }
            }

            @Override public void afterTextChanged(Editable editable) {
            }
        };
    }

    public void showAlertDialog(String title, String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(
                mContext,
                R.style.AlertDialogCustom_Destructive)
                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Delete Action
                        dialogInterface.cancel();
                        mContext.finish();
                    }
                })
                .setNegativeButton("Re-check", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        isNetworkConnected();
                        dialogInterface.cancel();
                    }
                })
                .setTitle(title).setMessage(message).create();
        alertDialog.show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public boolean validate() {
        if (mBinding.inputEmail.getText().toString().trim().equals("")) {
            mBinding.inputEmail.requestFocus();
            mBinding.inputEmail.setFocusable(true);
            mBinding.inputEmail.setError("Please enter an Email");
            return false;
        } else if (!mBinding.inputEmail.getText().toString().trim().matches(emailPattern)) {
            mBinding.inputEmail.requestFocus();
            mBinding.inputEmail.setFocusable(true);
            mBinding.inputEmail.setError("Invalid Email Address");
            return false;
        } else if (mBinding.inputPassword.getText().toString().trim().equals("")) {
            mBinding.inputPassword.requestFocus();
            mBinding.inputPassword.setFocusable(true);
            mBinding.inputPassword.setError("Please enter Password");
            mBinding.inputLayoutPassword.setPasswordVisibilityToggleEnabled(false);
            return false;
        } else if (mBinding.inputPassword.getText().toString().trim().length() < 6) {
            mBinding.inputPassword.requestFocus();
            mBinding.inputPassword.setFocusable(true);
            mBinding.inputPassword.setError("Password should be of 6 digits");
            mBinding.inputLayoutPassword.setPasswordVisibilityToggleEnabled(false);
            return false;
        } else {
            mBinding.inputLayoutPassword.setPasswordVisibilityToggleEnabled(true);
        }
        return true;
    }

    public void getLoginData() {
        mEvonixUtil.showLoader(mContext);
        getImeiAndFcmToken();
        Call<LoginWrapper>
                call = RestAdapter.createAPI().getLogin(
                Constant.ANDROID,
                mBinding.inputEmail.getText().toString().trim(),
                mBinding.inputPassword.getText().toString().trim(),
                mEvonixUtil.getSPString(R.string.evonix_util_fcm, ""),
                imeiNo,
                "1");

        call.enqueue(new Callback<LoginWrapper>() {
            @Override
            public void onResponse(Call<LoginWrapper> call, Response<LoginWrapper> response) {
                try {
                    if (response.isSuccessful()) {
                        mEvonixUtil.hideLoader();
                        LoginWrapper mWrapper = response.body();
                        if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK && mWrapper.getLoginData() != null) {
                            try {
                                List<LoginWrapper.Login> loginList = mWrapper.getLoginData();
                                for (LoginWrapper.Login login : loginList) {
                                    mEvonixUtil.setSPString(R.string.evonix_util_login_id, login.getDeliveryBoyId());
                                    mEvonixUtil.setSPString(R.string.evonix_util_token, login.getToken());
                                    mEvonixUtil.setSPString(R.string.evonix_mobile, login.getPhone());
                                    mEvonixUtil.setSPString(R.string.evonix_util_email, login.getEmail());
                                    mEvonixUtil.setSPString(R.string.evonix_restaurant_name, login.getRestaurantName());
                                    mEvonixUtil.setSPString(R.string.evonix_restaurant_address, login.getRestaurantAddress());
                                    mEvonixUtil.setSPString(R.string.evonix_util_restaurant_id, login.getRestaurant_id());
                                    mEvonixUtil.setSPString(R.string.evonix_util_full_name, login.getName());
                                    mEvonixUtil.setSPString(mContext.getResources().getString(R.string.profile_delivery_boy_profile_image),login.getImage());
                                    mEvonixUtil.setSPBoolean(mContext.getResources().getString(R.string.evonix_util_is_login), true);
                                    mEvonixUtil.LE(login.getToken());
                                }
                                Intent intent = new Intent(mContext, DashboardActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        mEvonixUtil.hideLoader();
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LoginWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
    }

    private void getImeiAndFcmToken() {
        try {
            imeiNo = Settings.Secure.getString(mContext.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            mEvonixUtil.setSPString(R.string.evonix_util_phone_imei, imeiNo);
            mEvonixUtil.LE("imeiNo: " + imeiNo);


            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                return;
                            }

                            // Get new Instance ID token
                            mEvonixUtil.setSPString(R.string.evonix_util_fcm, task.getResult().getToken());
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            mEvonixUtil.LE("Error to imeiNo :  " + e.toString());
        }
    }
}

