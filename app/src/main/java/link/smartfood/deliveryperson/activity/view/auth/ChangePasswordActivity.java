package link.smartfood.deliveryperson.activity.view.auth;

import android.app.Activity;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import androidx.annotation.NonNull;

import android.view.View;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.auth.ChangePasswordVM;
import link.smartfood.deliveryperson.databinding.ActivityChangePasswordBinding;
import link.smartfood.deliveryperson.util.EvonixBaseActivity;

public class ChangePasswordActivity extends EvonixBaseActivity {
    ChangePasswordVM mViewModel;
    ActivityChangePasswordBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingOfWidgets();
    }

    private void bindingOfWidgets() {
        mContext = ChangePasswordActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new ChangePasswordVM(((Activity) mContext), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(ChangePasswordVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setChangePasswordViewModel(mViewModel);
        initUI();
    }

    public void initUI() {
        setToolBar();
        mViewModel.setTextWatcherMethods();
    }

    private void setToolBar() {
        setSupportActionBar(mBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.exit, R.anim.enter);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

}
