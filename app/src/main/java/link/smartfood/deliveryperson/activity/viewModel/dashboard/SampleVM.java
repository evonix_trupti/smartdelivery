package link.smartfood.deliveryperson.activity.viewModel.dashboard;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import link.smartfood.deliveryperson.BuildConfig;
import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.order.OrderList;
import link.smartfood.deliveryperson.activity.model.order.OrderWrapper;
import link.smartfood.deliveryperson.activity.model.status.UpdateStatusWrapper;
import link.smartfood.deliveryperson.activity.view.NoInternetActivity;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.activity.view.dashboard.HomeFragment;
import link.smartfood.deliveryperson.activity.view.dashboard.SampleFragment;
import link.smartfood.deliveryperson.adapter.HomeAdapter;
import link.smartfood.deliveryperson.adapter.SampleAdapter;
import link.smartfood.deliveryperson.databinding.DialogStatusAlertBinding;
import link.smartfood.deliveryperson.databinding.HomeFragmentBinding;
import link.smartfood.deliveryperson.databinding.LayoutListBinding;
import link.smartfood.deliveryperson.databinding.SampleFragmentBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EndlessRecyclerViewScrollListener;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import link.smartfood.deliveryperson.activity.view.order.MapsActivity;
//import link.smartfood.deliveryperson.service.LocationMonitoringService;

public class SampleVM extends ViewModel {
    public SampleFragmentBinding mBinding;
    LayoutListBinding listBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    Call<OrderWrapper> call = null;
    private LinearLayoutManager linearLayoutManager;
    private static ArrayList<OrderList> orderList = new ArrayList<>();
    private MutableLiveData<ArrayList<OrderList>> mutableLiveData = new MutableLiveData<>();
    SampleAdapter adapter;
    private String searchField = "";
    public static Handler h;
    private int offSet = 0, totalRecord = 0, countFAb = 0;
    private Intent intent = null;

    //Location
// location last updated time
    private static final String TAG = SampleVM.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;


    public SampleVM(Activity mContext, SampleFragmentBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
        listBinding = mBinding.layoutList;
        initSearchView();
    }

    public void checkInternet() {
        if (!mEvonixUtil.isConnectingToInternet()) {
            mContext.startActivity(new Intent(mContext, NoInternetActivity.class));
        }
    }

    public void finishByMSG() {
        try {
            h = new Handler() {
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case 0:
                            setSearchView();
                            break;
                        case 1:
                            setFabUI();
                            break;
                        case 2:
                            offSet = 0;
                            mEvonixUtil.hideKeyboard();
                            notifyList();
                            getMutableLiveData();
                            break;
                    }
                }
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSearchView() {
        //Comment: Search for recyclerview
        if (listBinding.searchView.getVisibility() == View.VISIBLE) {
            offSet = 0;
            listBinding.searchView.setVisibility(View.GONE);
            listBinding.searchView.setQuery("", false);
            searchField = "";
            mEvonixUtil.hideKeyboard();
        } else {
            listBinding.searchView.setVisibility(View.VISIBLE);
            searchField = "";
        }
    }

    private void initSearchView() {
        listBinding.searchView.setQueryHint("Enter at least 3 alphabets for better search");
        listBinding.searchView.setIconified(false);
        listBinding.searchView.onActionViewExpanded();
        listBinding.searchView.clearFocus();
        listBinding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 2) {
                    if (call != null && call.isExecuted()) {
                        if (newText.trim().equals("")) {
                            return true;
                        }
                        call.cancel();
                    }
                    searchField = newText.trim();
                    offSet = 0;
                    mEvonixUtil.hideKeyboard();
                    notifyList();
                    mEvonixUtil.showLoader(mContext);
                    getMutableLiveData();
                }
                return true;
            }
        });
        listBinding.searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                return true;
            }
        });
    }

    public void onClick(int click) {
//        if (!adapter.getOrderId().equals("") && adapter.getOrderId() != null) {
//            switch (click) {
//                case 1:
//                    if (adapter.listCheckStatus.contains("16")) {
//                        showAlertDialog(mBinding.txtOutForDelivery.getText().toString(), "7"); //Comment: confirmation for Out For Delivery
//                    } else {
//                        mEvonixUtil.TEL("To make order status " + mBinding.txtOutForDelivery.getText().toString() + ", the order status should be Ready for pickup");
//                    }
//                    break;
//                case 2:
//                    if (adapter.listCheckStatus.contains("15") || adapter.listCheckStatus.contains("7")) {
//                        showAlertDialog(mBinding.txtDelivered.getText().toString(), "8"); //Comment: confirmation for Delivered
//                    } else {
//                        mEvonixUtil.TEL("To make order status " + mBinding.txtDelivered.getText().toString() + ", the order status should be Overdue or Out for delivery");
//                    }
//                    break;
//                case 3:
//                    if (adapter.listCheckStatus.contains("15") || adapter.listCheckStatus.contains("7")) {
//                        showAlertDialog(mBinding.txtCustUnavail.getText().toString(), "13"); //Comment:confirmation for Cust Unavail
//                    } else {
//                        mEvonixUtil.TEL("To make order status " + mBinding.txtCustUnavail.getText().toString() + ", the order status should be Overdue or Out for delivery");
//                    }
//                    break;
//                case 4:
//                    if (adapter.listCheckStatus.contains("15") || adapter.listCheckStatus.contains("7")) {
//                        showAlertDialog(mBinding.txtFakeOrder.getText().toString(), "14"); //Comment: confirmation for Fake Order
//                    } else {
//                        mEvonixUtil.TEL("To make order status as " + mBinding.txtFakeOrder.getText().toString() + ", the order status should be Overdue or Out for delivery");
//                    }
//                    break;
//            }
//        } else {
//            mEvonixUtil.TES("Please select order to change status");
//        }
    }

    public void onFabClick(int click) {
        switch (click) {
            case 1:
                if (countFAb > 0) {
//                    Intent intent = new Intent(mContext, MapsActivity.class);
//                    intent.putExtra("listData", orderList);
//                    mContext.startActivity(intent);
//                    ((Activity) mContext).overridePendingTransition(R.anim.enter, R.anim.exit);

                    Uri gmmIntentUri = Uri.parse("google.streetview:cbll=46.414382,10.013988");

                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    mContext.startActivity(mapIntent);
                } else {
                    mEvonixUtil.TEL("No order is Out for delivery, kindly select order for out for delivery");
                }
                break;
        }
    }

    private void showAlertDialog(final String strStatus, final String strId) {
        final Dialog dialog = new Dialog(mContext);
        final DialogStatusAlertBinding dialogStatusAlertBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_status_alert, null, false);
        dialog.setContentView(dialogStatusAlertBinding.getRoot());
        dialogStatusAlertBinding.llChangeOrderStatus.setVisibility(View.VISIBLE);
        dialogStatusAlertBinding.llChangeDeliveryBoyStatus.setVisibility(View.GONE);
        dialogStatusAlertBinding.txtMessage.setText("Can mark an order " + " " + strStatus + "?");
        dialogStatusAlertBinding.txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialogStatusAlertBinding.txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadStatusUpdate();
            }

            private void loadStatusUpdate() {
                dialog.dismiss();
                mEvonixUtil.showLoader(mContext);
                Call<UpdateStatusWrapper>
                        call = RestAdapter.createAPI().updateOrderStatus(
                        Constant.ANDROID,
                        mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token), ""),
                        adapter.getOrderId(),
                        strId,
                        strId,
                        strId,
                        "");

                call.enqueue(new Callback<UpdateStatusWrapper>() {
                    @Override
                    public void onResponse(Call<UpdateStatusWrapper> call, Response<UpdateStatusWrapper> response) {
                        try {
                            if (response.isSuccessful()) {
                                mEvonixUtil.hideLoader();
                                UpdateStatusWrapper mWrapper = response.body();
                                if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK) {
                                    try {
                                       /* if (strStatus.equalsIgnoreCase("out for delivery")) {
                                            startStep1();
                                        } else {
                                            mContext.stopService(new Intent(mContext, LocationMonitoringService.class));
                                        }*/
                                        adapter.listCheckStatus.clear();
                                        searchField = "";
                                        offSet = 0;
                                        getMutableLiveData();
                                        mEvonixUtil.TES(mWrapper.getMessage());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                mEvonixUtil.hideLoader();
                                dialog.dismiss();
                                JSONObject rootObject = new JSONObject(response.errorBody().string());
                                mEvonixUtil.TES(rootObject.getString("errorMessage"));
                                if (rootObject.getString("status").equals("520")) {
                                    mEvonixUtil.setLogout();
                                    mContext.finish();
                                    mContext.startActivity(new Intent(mContext, SplashActivity.class));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateStatusWrapper> call, Throwable t) {
                        mEvonixUtil.hideLoader();
                        mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
                    }
                });
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }

    public LiveData<ArrayList<OrderList>> getAllOrder() {
        mEvonixUtil.showLoader(mContext);
        return getMutableLiveData();
    }

  /* //Comment: Client requirement-> take static statuses not from api -> 11Sept2019
   public LiveData<List<OrderStatusWrapper.Status>> getStatus() {
        return getStatusLiveData();
    }*/

    public void setRecycler(SampleFragment homeFragment) {
        /*//Comment: Client requirement-> take static statuses not from api -> 11Sept2019
        getStatus().observe(homeFragment, new Observer<List<OrderStatusWrapper.Status>>() {
            @Override
            public void onChanged(@Nullable List<OrderStatusWrapper.Status> statusList) {
                StatusAdapter adapter = new StatusAdapter(mContext, statusList);
                mBinding.recyclerView.setAdapter(adapter);
            }
        });*/

        getAllOrder().observe(homeFragment, new Observer<ArrayList<OrderList>>() {
            @Override
            public void onChanged(@Nullable ArrayList<OrderList> orderLists) {
                linearLayoutManager = new LinearLayoutManager(mContext);
                listBinding.progressBarLoadMore.setVisibility(View.GONE);
                listBinding.recyclerView.setLayoutManager(linearLayoutManager);
                adapter = new SampleAdapter(mContext, orderLists);
                listBinding.recyclerView.setAdapter(adapter);
                listBinding.swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_green_light,
                        android.R.color.holo_blue_bright,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
                listBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        listBinding.searchView.setVisibility(View.GONE);
                        searchField = "";
                        offSet = 0;
                        getMutableLiveData();
                    }
                });

                listBinding.recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        if (offSet <= totalRecord) {
                            offSet = offSet + visibleThreshold;
                            getMutableLiveData();
                        }
                    }
                });
            }
        });
    }

    public MutableLiveData<ArrayList<OrderList>> getMutableLiveData() {
        if (offSet < 1) {
            listBinding.progressBarLoadMore.setVisibility(View.GONE);
        } else if (offSet <= totalRecord) {
            listBinding.progressBarLoadMore.setVisibility(View.VISIBLE);
        } else {
            return mutableLiveData;
        }
        call = RestAdapter.createAPI().getOrderList(
                Constant.ANDROID,
                mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token), ""),
                "home",
                searchField,
                offSet,
                "");

        call.enqueue(new Callback<OrderWrapper>() {
            @Override
            public void onResponse(Call<OrderWrapper> call, Response<OrderWrapper> response) {
                OrderWrapper mBlogWrapper = response.body();
                mEvonixUtil.hideLoader();
                OrderList orderModel = null;
                int isOutForDelivery = 0;
                try {
                    if (response.isSuccessful()) {
                        if (mBlogWrapper != null) {
                            offSet = mBlogWrapper.getOffset();
                            totalRecord = mBlogWrapper.getTotal();
                            if (mBlogWrapper.getOrderList() != null) {
                                if (mBlogWrapper.getOffset() < 1) {
                                    orderList = (ArrayList<OrderList>) mBlogWrapper.getOrderList();
                                    mutableLiveData.setValue(orderList);
                                } else {
                                    for (int i = 0; i < mBlogWrapper.getOrderList().size(); i++)
                                        orderList.add(mBlogWrapper.getOrderList().get(i));
                                }
                                for (int i = 0; i < orderList.size(); i++) {
                                    orderModel = orderList.get(i);
                                    if (orderModel.getStatus_id().equals("7")) {
                                        isOutForDelivery++;
                                    }
                                }
                                if (isOutForDelivery != 0) {
                                    Log.e("service", "started");
                                    startStep1();
                                } else {
                                    if (intent != null) {
//                                        Intent intent = new Intent(mContext, LocationMonitoringService.class);
//                                        mContext.stopService(intent);
//                                        IS_LOCATION_SERVICE_RUNNING = false;
////                                        mContext.stopService(new Intent(mContext, LocationMonitoringService.class));
//                                        Log.e("service", "stopped");
                                    }
                                }
                            } else {
                                orderList = (ArrayList<OrderList>) mBlogWrapper.getOrderList();
                            }
                        }
                        notifyList();
                        setFabUI();
                    } else {
                        mEvonixUtil.hideLoader();
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        if (rootObject.getString("status").equals("520")) {
                            mEvonixUtil.setLogout();
                            mContext.finish();
                            mContext.startActivity(new Intent(mContext, SplashActivity.class));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<OrderWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
        return mutableLiveData;
    }

    public void setFabUI() {
        if (orderList != null) {
            for (int i = 0; i < orderList.size(); i++) {
                OrderList orderModel = orderList.get(i);
                if (orderModel.getOrder_status().equalsIgnoreCase("out for delivery")) {
                    countFAb++;
                }
            }
            if (countFAb > 0 /*|| adapter.listCheckId.size() != 0*/) {
                mBinding.fabMap.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.colorAccent)));
            } else {
                mBinding.fabMap.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.grey_700)));
            }
        }
    }

    public static JsonArray getOrderArray() {
        //Do something after 20 seconds
        if (orderList != null) {
            JsonObject jonOrder = new JsonObject();
            JsonArray jsonArrayOrder = new JsonArray();
            for (int i = 0; i < orderList.size(); i++) {
                OrderList orderModel = orderList.get(i);
                if (orderModel.getStatus_id().equalsIgnoreCase("7")) {
                    jonOrder.addProperty("order_id", orderModel.getId());
                    jonOrder.addProperty("consumer_id", orderModel.getConsumer_id());
                    jonOrder.addProperty("consumer_name", orderModel.getName());
                    jonOrder.addProperty("delivery_address", orderModel.getDelivery_address());
                    jonOrder.addProperty("consumer_email", orderModel.getConsumer_email());
                }
            }
            jsonArrayOrder.add(jonOrder);
            return jsonArrayOrder;
        }
        return null;
    }

    @SuppressLint("RestrictedApi")
    public void notifyList() {
        if (orderList != null) {
            listBinding.recyclerView.setVisibility(View.VISIBLE);
            adapter.setList(orderList);
            listBinding.rlNoData.setVisibility(View.GONE);
            mBinding.llStatus.setVisibility(View.VISIBLE);
            mBinding.fabMap.setVisibility(View.GONE);
        } else {
            mBinding.llStatus.setVisibility(View.GONE);
            listBinding.recyclerView.setVisibility(View.GONE);
            listBinding.rlNoData.setVisibility(View.VISIBLE);
            mBinding.fabMap.setVisibility(View.GONE);
        }
        mEvonixUtil.hideLoader();
        mEvonixUtil.hideKeyboard();
        listBinding.swipeRefreshLayout.setRefreshing(false);
        listBinding.progressBarLoadMore.setVisibility(View.GONE);
    }

    public void setShowCase(FragmentActivity activity) {
        if (mEvonixUtil.getSPBoolean("isTapTargetShow", true)) {
            mEvonixUtil.setSPBoolean("isTapTargetShow", false);
            new TapTargetSequence(mContext)
                    .targets(
                            TapTarget.forView(mBinding.llStatus, "Update order status ", "Select orders by clicking check box and update the status")
                                    .outerCircleColor(R.color.colorPrimary)
                                    .outerCircleAlpha(0.99f)
                                    .transparentTarget(true)
                                    .titleTextSize(20)
                                    .titleTextColor(R.color.white_1000)
                                    .descriptionTextSize(18)
                                    .descriptionTextColor(R.color.white_1000)
                                    .textTypeface(Typeface.SANS_SERIF)
                                    .dimColor(R.color.black_1000)
                                    .drawShadow(true)
                                    .cancelable(true)
                                    .tintTarget(false)
                                    .transparentTarget(false)
                                    .targetRadius(100))
//        ,
//                            TapTarget.forView(mBinding.fabMap, "Order locations on Map ", "When you make order \"Out for Delivery\", this button will be available to see your delivery locations on map")
//                                    .outerCircleColor(R.color.colorPrimary)
//                                    .outerCircleAlpha(0.99f)
//                                    .transparentTarget(true)
//                                    .titleTextSize(20)
//                                    .titleTextColor(R.color.white_1000)
//                                    .descriptionTextSize(18)
//                                    .descriptionTextColor(R.color.white_1000)
//                                    .textTypeface(Typeface.SANS_SERIF)
//                                    .dimColor(R.color.black_1000)
//                                    .drawShadow(true)
//                                    .cancelable(true)
//                                    .tintTarget(false)
//                                    .transparentTarget(false)
//                                    .targetRadius(70))
                    .listener(new TapTargetSequence.Listener() {
                        @Override
                        public void onSequenceFinish() {
                            mEvonixUtil.setSPBoolean("isTapTargetShow", false);
                        }

                        @Override
                        public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        }

                        @Override
                        public void onSequenceCanceled(TapTarget lastTarget) {

                        }
                    }).start();
        }
    }

    /**
     * Step 1: Check Google Play services
     */
    private void startStep1() {
        //Check whether this user has installed Google play service which is being used by Location updates.
        if (isGooglePlayServicesAvailable()) {
            //Passing null to indicate that it is executing for the first time.
            startStep2(null);
        } else {
            Toast.makeText(mContext, "Google service unavailable", Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Step 2: Check & Prompt Internet connection
     */
    private Boolean startStep2(DialogInterface dialog) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            promptInternetConnect();
            return false;
        }


        if (dialog != null) {
            dialog.dismiss();
        }

        //Yes there is active internet connection. Next check Location is granted by user or not.
        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.
            startStep3();
        } else {  //No user has not granted the permissions yet. Request now.
            requestPermissions();
        }
        return true;
    }

    /**
     * Show A Dialog with button to refresh the internet state.
     */
    private void promptInternetConnect() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("No Internet");
        builder.setMessage("No Internet");

        String positiveText = "Refresh";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Block the Application Execution until user grants the permissions
                        if (startStep2(dialog)) {
                            //Now make sure about location permission.
                            if (checkPermissions()) {
                                startStep3();
                            } else if (!checkPermissions()) {
                                requestPermissions();
                            }
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Step 3: Start the Location Monitor Service
     */
    private void startStep3() {
//        intent = new Intent(mContext, LocationMonitoringService.class);
//        mContext.startService(intent);
//        IS_LOCATION_SERVICE_RUNNING = true;
    }

    /**
     * Return the availability of GooglePlayServices
     */
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(mContext);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(mContext, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionState2 = ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Start permissions requests.
     */
    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(mContext,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(mContext,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        if (shouldProvideRationale || shouldProvideRationale2) {
            showSnackbar("Permission needed",
                    "OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(mContext,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            ActivityCompat.requestPermissions(mContext,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void showSnackbar(final String mainTextStringId, final String actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                mBinding.llStatus,
                mainTextStringId,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(actionStringId, listener).show();
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Permission granted, updates requested, starting location updates");
                startStep3();

            } else {
                showSnackbar("Needed permission",
                        "Settings", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(intent);
                            }
                        });
            }
        }
    }

   /* public void onDestroy() {
        mContext.stopService(new Intent(mContext, LocationMonitoringService.class));
    }*/
}
