package link.smartfood.deliveryperson.activity.viewModel.auth;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModel;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.login.LoginWrapper;
import link.smartfood.deliveryperson.activity.model.profile.DeliveryBoyDetailsWrapper;
import link.smartfood.deliveryperson.activity.model.profile.UpdateProfileWrapper;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.activity.view.dashboard.DashboardActivity;
import link.smartfood.deliveryperson.databinding.ActivityUpdateAccountBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EvonixUtil;
import link.smartfood.deliveryperson.util.ImagePickerActivity;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static okhttp3.MediaType.parse;

public class AccountInfoVM extends ViewModel {
    public ActivityUpdateAccountBinding mBinding;
    String NamePattern = "[a-zA-Z ]+", VehiclePattern = "^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$+";
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    private String userChoosenTask = "", strImgPath = "";
    public static final int REQUEST_CAMERA = 00, SELECT_FILE = 11;
    public static final int REQUEST_IMAGE = 100;

    public AccountInfoVM(Activity mContext, ActivityUpdateAccountBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    public void requestMultiplePermissions() {
        Dexter.withActivity(mContext)
                .withPermissions(Manifest.permission.CALL_PHONE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    public void onClick(int click) {
        switch (click) {
            case 1:
                requestMultiplePermissions();
                break;
            case 2:
                if (validate()) {
                    updateDeliveryBoyProfile();
                }
                break;
        }

    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(mContext, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(mContext, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        mContext.startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(mContext, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        mContext.startActivityForResult(intent, REQUEST_IMAGE);
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                String selectedImage = data.getStringExtra("path");
                loadProfile(selectedImage);
            }
        }
    }

    private void loadProfile(String selectedImage) {
        try {
            Bitmap bitmapImage = BitmapFactory.decodeFile(selectedImage);
            int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);
            mBinding.imgProfile.setImageBitmap(scaled);
            strImgPath = String.valueOf(selectedImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.dialog_permission_title));
        builder.setMessage(mContext.getResources().getString(R.string.dialog_permission_message));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                openSettings();
            }
        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
        intent.setData(uri);
        mContext.startActivityForResult(intent, 101);
    }


    private boolean validate() {
        if (mBinding.inputFirstName.getText().toString().trim().equalsIgnoreCase("")) {
            mBinding.inputFirstName.requestFocus();
            mBinding.inputFirstName.setFocusable(true);
            mBinding.inputFirstName.setError("Please enter First Name");
            return false;
        } else if (!mBinding.inputFirstName.getText().toString().trim().matches(NamePattern)) {
            mBinding.inputFirstName.requestFocus();
            mBinding.inputFirstName.setFocusable(true);
            mBinding.inputFirstName.setError("FirstName should be characters only");

            return false;
        } else if (mBinding.inputLastName.getText().toString().trim().equalsIgnoreCase("")) {
            mBinding.inputLastName.requestFocus();
            mBinding.inputLastName.setFocusable(true);
            mBinding.inputLastName.setError("Please enter Last Name");
            return false;
        } else if (!mBinding.inputLastName.getText().toString().trim().matches(NamePattern)) {
            mBinding.inputLastName.requestFocus();
            mBinding.inputLastName.setFocusable(true);
            mBinding.inputLastName.setError("LastName should be characters only");
            return false;
        } /*else if (mBinding.inputMobileNo.getText().toString().trim().length() < 7) {
            mBinding.inputMobileNo.requestFocus();
            mBinding.inputMobileNo.setFocusable(true);
            mBinding.inputMobileNo.setError("Mobile number should be minimum 7 digits");
            return false;
        } else if (mBinding.inputMobileNo.getText().toString().trim().startsWith("0")) {
            mBinding.inputMobileNo.requestFocus();
            mBinding.inputMobileNo.setFocusable(true);
            mBinding.inputMobileNo.setError("Mobile number should not start from 0");
            return false;
        } else if (mBinding.spCountryid.getSelectedItem().toString().trim().equalsIgnoreCase("")) {
            mEvonixUtil.TEL("Country code does not present");
            return false;
        } else if (mBinding.spNetworkid.getSelectedItem().toString().trim().equalsIgnoreCase("")) {
            mEvonixUtil.TEL("Network code does not present for these country code");
            return false;
        }*/ else if (mBinding.inputAddress.getText().toString().trim().equalsIgnoreCase("")) {
            mBinding.inputAddress.requestFocus();
            mBinding.inputAddress.setFocusable(true);
            mBinding.inputAddress.setError("Please enter Address");
            return false;
        } else if (mBinding.inputVehical.getText().toString().trim().equalsIgnoreCase("")) {
            mBinding.inputVehical.requestFocus();
            mBinding.inputVehical.setFocusable(true);
            mBinding.inputVehical.setError("Please enter Vehicle Number");
            return false;
        } else if (mBinding.inputVehical.getText().toString().trim().length() < 7) {
            mBinding.inputVehical.setError("Vehicle number should be minimum 7 digits");
            return false;
        }
        return true;
    }

    public void loadAccountData() {
        mEvonixUtil.showLoader(mContext);
        Call<DeliveryBoyDetailsWrapper>
                call = RestAdapter.createAPI().getDeliveryBoyDetails(
                Constant.ANDROID,
                mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token), ""));

        call.enqueue(new Callback<DeliveryBoyDetailsWrapper>() {
            @Override
            public void onResponse(Call<DeliveryBoyDetailsWrapper> call, Response<DeliveryBoyDetailsWrapper> response) {
                try {
                    if (response.isSuccessful()) {
                        mEvonixUtil.hideLoader();
                        DeliveryBoyDetailsWrapper mWrapper = response.body();
                        if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK && mWrapper.getDeliveryBoyDetailsList() != null) {
                            try {
                                List<DeliveryBoyDetailsWrapper.DeliveryBoyDetails> deliveryBoyDetails = mWrapper.getDeliveryBoyDetailsList();
                                for (DeliveryBoyDetailsWrapper.DeliveryBoyDetails data : deliveryBoyDetails) {
//                                    setData(data);
                                    mBinding.setDeliveryBoyDetails(data);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        mEvonixUtil.hideLoader();
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DeliveryBoyDetailsWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
    }

    public void updateDeliveryBoyProfile() {
        mEvonixUtil.showLoader(mContext);
        RequestBody appType = RequestBody.create(parse("multipart/form-data"), Constant.ANDROID);
        RequestBody auth_token = RequestBody.create(parse("multipart/form-data"), mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token), ""));
        RequestBody firstName = RequestBody.create(parse("multipart/form-data"), mBinding.inputFirstName.getText().toString().trim());
        RequestBody lastName = RequestBody.create(parse("multipart/form-data"), mBinding.inputLastName.getText().toString().trim());
        RequestBody country = RequestBody.create(parse("multipart/form-data"), mBinding.inputCountry.getText().toString().trim());
        RequestBody network = RequestBody.create(parse("multipart/form-data"), mBinding.inputNetwork.getText().toString().trim());
        RequestBody mobile = RequestBody.create(parse("multipart/form-data"), mBinding.inputMobileNo.getText().toString().trim());
        RequestBody vehicleNo = RequestBody.create(parse("multipart/form-data"), mBinding.inputVehical.getText().toString().trim());
        RequestBody address = RequestBody.create(parse("multipart/form-data"), mBinding.inputAddress.getText().toString().trim());
        RequestBody email = RequestBody.create(parse("multipart/form-data"), mBinding.inputEmail.getText().toString().trim());
        MultipartBody.Part imgBody = null;
        if (strImgPath != "") {
            File file = new File(strImgPath);
            RequestBody requestFile = RequestBody.create(parse("multipart/form-data"), file);
            imgBody = MultipartBody.Part.createFormData("profile_img", file.getName(), requestFile);
        } else {

        }
        Call<UpdateProfileWrapper>
                call = RestAdapter.createAPI().updateDeliveryBoyProfile(
                appType,
                auth_token,
                firstName,
                lastName,
                country,
                network,
                mobile,
                vehicleNo,
                address,
                email,
                imgBody);

        call.enqueue(new Callback<UpdateProfileWrapper>() {
            @Override
            public void onResponse(Call<UpdateProfileWrapper> call, Response<UpdateProfileWrapper> response) {
                try {
                    if (response.isSuccessful()) {
                        mEvonixUtil.hideLoader();
                        UpdateProfileWrapper mWrapper = response.body();
                        if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK && mWrapper.getProfileData() != null) {
                            try {
                                List<UpdateProfileWrapper.UpdateProfile> profileList = mWrapper.getProfileData();
                                for (UpdateProfileWrapper.UpdateProfile profile : profileList) {
                                    mEvonixUtil.setSPString(R.string.evonix_util_full_name, profile.getName());
                                    mEvonixUtil.setSPString(R.string.profile_delivery_boy_profile_image, profile.getImage());
                                    mEvonixUtil.setSPString(R.string.evonix_util_email, profile.getEmail());
                                }
                                mEvonixUtil.TES(mWrapper.getMessage());
                                mContext.onBackPressed();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        mEvonixUtil.hideLoader();
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        if (rootObject.getString("status").equals("520")) {
                            mEvonixUtil.setLogout();
                            mContext.finish();
                            mContext.startActivity(new Intent(mContext, SplashActivity.class));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
    }

    public void setData(List<DeliveryBoyDetailsWrapper.DeliveryBoyDetails> data) {
        mBinding.inputFirstName.setText(data.get(0).getFirstName());
        mBinding.inputLastName.setText(data.get(0).getLastName());
        mBinding.inputMobileNo.setText(data.get(0).getMobile());
        mBinding.inputAddress.setText(data.get(0).getAddress());
        mBinding.inputVehical.setText(data.get(0).getVehicleNo());
    }

    public void textmatchermethods() {
        mBinding.inputFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mBinding.inputFirstName.getText().toString().matches(NamePattern)) {
                } else {
                    mBinding.inputFirstName.setError("FirstName should be characters only");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mBinding.inputLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mBinding.inputLastName.getText().toString().matches(NamePattern)) {
                } else {
                    mBinding.inputLastName.setError("LastName should be characters only");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mBinding.inputMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mBinding.inputMobileNo.getText().toString().length() < 7) {
                    mBinding.inputMobileNo.setError("Mobile number should be 7 digits");
                } /*else if (mBinding.inputMobileNo.getText().toString().startsWith("0")) {
                    mBinding.inputMobileNo.requestFocus();
                    mBinding.inputMobileNo.setFocusable(true);
                    mBinding.inputMobileNo.setError("Mobile number should not start from 0");

                } else {

                }*/
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.inputVehical.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mBinding.inputVehical.getText().toString().length() < 7) {
                    mBinding.inputVehical.setError("Vehicle number should be minimum 7 digits");
                } else {

                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
}
