package link.smartfood.deliveryperson.activity.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.view.auth.LoginActivity;
import link.smartfood.deliveryperson.activity.view.dashboard.DashboardActivity;
import link.smartfood.deliveryperson.util.EvonixUtil;

public class NoInternetActivity extends AppCompatActivity {

    private Activity mContext;
    private EvonixUtil mEvonixUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);
        initUI();
    }

    private void initUI() {
        mContext = NoInternetActivity.this;
        mEvonixUtil = new EvonixUtil(mContext);
        ((TextView) findViewById(R.id.txtTryAgain)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEvonixUtil.getSPBoolean(mContext.getResources().getString(R.string.evonix_util_is_login), false)) {
                    mContext.startActivity(new Intent(mContext, DashboardActivity.class));
                } else {
                    mContext.startActivity(new Intent(mContext, LoginActivity.class));
                }
            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}
