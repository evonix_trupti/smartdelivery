package link.smartfood.deliveryperson.activity.model.profile;

import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;
import android.widget.ImageView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import link.smartfood.deliveryperson.R;

public class DeliveryBoyDetailsWrapper {
     @SerializedName("status")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("deliveryBoyInfo")
    @Expose
    private List<DeliveryBoyDetails> deliveryBoyDetailsList = new ArrayList();

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DeliveryBoyDetails> getDeliveryBoyDetailsList() {
        return deliveryBoyDetailsList;
    }

    public void setDeliveryBoyDetailsList(List<DeliveryBoyDetails> deliveryBoyDetailsList) {
        this.deliveryBoyDetailsList = deliveryBoyDetailsList;
    }

    public class DeliveryBoyDetails extends BaseObservable {
        @SerializedName("firstName")
        @Expose
        private String firstName;

        @SerializedName("lastName")
        @Expose
        private String lastName;

        @SerializedName("networkId")
        @Expose
        private String networkId;

        @SerializedName("country_id")
        @Expose
        private String country_id;

        @SerializedName("mobile")
        @Expose
        private String mobile;

        @SerializedName("vehicleNo")
        @Expose
        private String vehicleNo;

        @SerializedName("address")
        @Expose
        private String address;

        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("image")
        @Expose
        private String image;

        @SerializedName("totalCount")
        @Expose
        private String totalCount = "0";

        @SerializedName("deliveredCount")
        @Expose
        private String deliveredCount;

        @SerializedName("undeliveredCount")
        @Expose
        private String undeliveredCount;

        @SerializedName("pendingCount")
        @Expose
        private String pendingCount;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getNetworkId() {
            return networkId;
        }

        public void setNetworkId(String networkId) {
            this.networkId = networkId;
        }

        public String getCountry_id() {
            return country_id;
        }

        public void setCountry_id(String country_id) {
            this.country_id = country_id;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getVehicleNo() {
            return vehicleNo;
        }

        public void setVehicleNo(String vehicleNo) {
            this.vehicleNo = vehicleNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }


        public String getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(String totalCount) {
            this.totalCount = totalCount;
        }

        public String getDeliveredCount() {
            return deliveredCount;
        }

        public void setDeliveredCount(String deliveredCount) {
            this.deliveredCount = deliveredCount;
        }

        public String getUndeliveredCount() {
            return undeliveredCount;
        }

        public void setUndeliveredCount(String undeliveredCount) {
            this.undeliveredCount = undeliveredCount;
        }

        public String getPendingCount() {
            return pendingCount;
        }

        public void setPendingCount(String pendingCount) {
            this.pendingCount = pendingCount;
        }

    }

    @BindingAdapter({"android:profileImage"})
    public static void loadImage(ImageView view, String imageUrl) {
         Picasso.get().load(imageUrl).placeholder(R.drawable.ic_user).into(view);
    }
}
