package link.smartfood.deliveryperson.activity.viewModel.dashboard;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.notfication.NotificationListWrapper;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.activity.view.dashboard.NotificationFragment;
import link.smartfood.deliveryperson.adapter.NotificationAdapter;
import link.smartfood.deliveryperson.databinding.FragmentNotificationBinding;
import link.smartfood.deliveryperson.databinding.LayoutListBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EndlessRecyclerViewScrollListener;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationVM extends ViewModel {
    public FragmentNotificationBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    LayoutListBinding layoutListBinding;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<NotificationListWrapper.NotificationList> notificationList = new ArrayList<>();
    private MutableLiveData<List<NotificationListWrapper.NotificationList>> mutableLiveData = new MutableLiveData<>();
    NotificationAdapter adapter;
    private String strFilter = "";
    private int offSet = 0, totalRecord = 0;

    public NotificationVM(Activity mContext, FragmentNotificationBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
        layoutListBinding = mBinding.layoutList;
    }

    public void onClick(String click) {
        strFilter = click;
        switch (click) {
            case "":
                setLayout(mBinding.txtAll, mBinding.txtToday, mBinding.txtLastWeek, mBinding.txtYesterday, mBinding.txtLastMonth, mBinding.txtLastYear);
                break;
            case "today":
                setLayout(mBinding.txtToday, mBinding.txtAll, mBinding.txtLastWeek, mBinding.txtYesterday, mBinding.txtLastMonth, mBinding.txtLastYear);
                break;
            case "yesterday":
                setLayout(mBinding.txtYesterday, mBinding.txtToday, mBinding.txtLastWeek, mBinding.txtAll, mBinding.txtLastMonth, mBinding.txtLastYear);
                break;
            case "week":
                setLayout(mBinding.txtLastWeek, mBinding.txtToday, mBinding.txtYesterday, mBinding.txtAll, mBinding.txtLastMonth, mBinding.txtLastYear);
                break;
            case "month":
                setLayout(mBinding.txtLastMonth, mBinding.txtToday, mBinding.txtLastWeek, mBinding.txtAll, mBinding.txtYesterday, mBinding.txtLastYear);
                break;
            case "year":
                setLayout(mBinding.txtLastYear, mBinding.txtToday, mBinding.txtLastWeek, mBinding.txtAll, mBinding.txtLastMonth, mBinding.txtYesterday);
                break;
        }
    }


    private void setLayout(TextView txtSelected, TextView txtRest1, TextView txtRest2, TextView txtRest3, TextView txtRest4, TextView txtRest5) {
        offSet = 0;
        getAllOrder();
        txtSelected.setBackground(mContext.getResources().getDrawable(R.drawable.button_border));
        txtRest1.setBackground(mContext.getResources().getDrawable(R.drawable.rectangular_border));
        txtRest2.setBackground(mContext.getResources().getDrawable(R.drawable.rectangular_border));
        txtRest3.setBackground(mContext.getResources().getDrawable(R.drawable.rectangular_border));
        txtRest4.setBackground(mContext.getResources().getDrawable(R.drawable.rectangular_border));
        txtRest5.setBackground(mContext.getResources().getDrawable(R.drawable.rectangular_border));

        txtSelected.setTextColor(mContext.getResources().getColor(R.color.white_1000));
        txtRest1.setTextColor(mContext.getResources().getColor(R.color.black_1000));
        txtRest2.setTextColor(mContext.getResources().getColor(R.color.black_1000));
        txtRest3.setTextColor(mContext.getResources().getColor(R.color.black_1000));
        txtRest4.setTextColor(mContext.getResources().getColor(R.color.black_1000));
        txtRest5.setTextColor(mContext.getResources().getColor(R.color.black_1000));
    }

    public LiveData<List<NotificationListWrapper.NotificationList>> getAllOrder() {
        mEvonixUtil.showLoader(mContext);
        return getMutableLiveData();
    }

    public void setRecycler(NotificationFragment notificationFragment) {
        getAllOrder().observe(notificationFragment, new Observer<List<NotificationListWrapper.NotificationList>>() {
            @Override
            public void onChanged(@Nullable List<NotificationListWrapper.NotificationList> notificationList) {
                mEvonixUtil.setSPString(mContext.getResources().getString(R.string.dashboard_list_from), "history");
                linearLayoutManager = new LinearLayoutManager(mContext);
                layoutListBinding.progressBarLoadMore.setVisibility(View.GONE);
                layoutListBinding.recyclerView.setLayoutManager(linearLayoutManager);
                adapter = new NotificationAdapter(mContext, notificationList);
                layoutListBinding.recyclerView.setAdapter(adapter);

                layoutListBinding.swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_green_light,
                        android.R.color.holo_blue_bright,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
                layoutListBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        layoutListBinding.swipeRefreshLayout.setRefreshing(false);
                        layoutListBinding.searchView.setVisibility(View.GONE);
                        layoutListBinding.searchView.setQuery("", false);
                        offSet = 0;
                        getMutableLiveData();
                    }
                });

                layoutListBinding.recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        if (offSet <= totalRecord) {
                            offSet = offSet + visibleThreshold;
                            getMutableLiveData();
                        }
                    }
                });
            }
        });
    }

    public MutableLiveData<List<NotificationListWrapper.NotificationList>> getMutableLiveData() {
        if (offSet < 1) {
            layoutListBinding.progressBarLoadMore.setVisibility(View.GONE);
        } else if (offSet <= totalRecord) {
            layoutListBinding.progressBarLoadMore.setVisibility(View.VISIBLE);
        } else {
            return mutableLiveData;
        }
        Call<NotificationListWrapper>
                call = RestAdapter.createAPI().getNotification(
                Constant.ANDROID,
                mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token), ""),
                strFilter,
                offSet);

        call.enqueue(new Callback<NotificationListWrapper>() {
            @Override
            public void onResponse(Call<NotificationListWrapper> call, Response<NotificationListWrapper> response) {
                NotificationListWrapper mBlogWrapper = response.body();
                mEvonixUtil.hideLoader();
                try {
                    if (response.isSuccessful()) {
                        if (mBlogWrapper != null) {
                            offSet = mBlogWrapper.getOffset();
                            totalRecord = mBlogWrapper.getTotal();
                            if (mBlogWrapper.getNotificationList() != null) {
                                if (mBlogWrapper.getOffset() < 1) {
                                    notificationList = (ArrayList<NotificationListWrapper.NotificationList>) mBlogWrapper.getNotificationList();
                                    mutableLiveData.setValue(notificationList);
                                } else {
                                    for (int i = 0; i < mBlogWrapper.getNotificationList().size(); i++)
                                        notificationList.add(mBlogWrapper.getNotificationList().get(i));
                                }
                            }else {
                                notificationList = (ArrayList<NotificationListWrapper.NotificationList>) mBlogWrapper.getNotificationList();
                            }
                        }
                        notifyList();
                    } else {
                        mEvonixUtil.hideLoader();
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        if (rootObject.getString("status").equals("520")) {
                            mEvonixUtil.setLogout();
                            mContext.finish();
                            mContext.startActivity(new Intent(mContext, SplashActivity.class));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<NotificationListWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
        return mutableLiveData;
    }

    public void notifyList() {
        if (notificationList != null) {
            layoutListBinding.recyclerView.setVisibility(View.VISIBLE);
            adapter.setList(notificationList);
            layoutListBinding.rlNoData.setVisibility(View.GONE);
//            mBinding.llTabdelivery.setVisibility(View.VISIBLE);
        } else {
            layoutListBinding.recyclerView.setVisibility(View.GONE);
            layoutListBinding.rlNoData.setVisibility(View.VISIBLE);
//            mBinding.llTabdelivery.setVisibility(View.GONE);
        }
        mEvonixUtil.hideLoader();
        layoutListBinding.swipeRefreshLayout.setRefreshing(false);
        layoutListBinding.progressBarLoadMore.setVisibility(View.GONE);
    }
}

