package link.smartfood.deliveryperson.activity.viewModel.dashboard;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;


import org.json.JSONObject;

import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.profile.DeliveryBoyDetailsWrapper;
import link.smartfood.deliveryperson.activity.model.profile.UpdateDeliveryBoyStatusWrapper;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.activity.view.auth.AccountInfoActivity;
import link.smartfood.deliveryperson.activity.view.dashboard.DashboardActivity;
import link.smartfood.deliveryperson.databinding.DialogStatusAlertBinding;
import link.smartfood.deliveryperson.databinding.FragmentProfileBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragmentVM extends ViewModel {
    public FragmentProfileBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    DeliveryBoyDetailsWrapper mWrapper;
    private String strStatusId = "";


    public ProfileFragmentVM(Activity mContext, FragmentProfileBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    public void getLocalData() {
        if (mEvonixUtil.getSPString(mContext.getResources().getString(R.string.profile_delivery_boy_status), "").equals("1")) {
            mBinding.switchStatus.setChecked(true);
            mBinding.txtStatus.setText("Available");
        } else if (mEvonixUtil.getSPString(mContext.getResources().getString(R.string.profile_delivery_boy_status), "").equals("3")) {
            mBinding.switchStatus.setChecked(false);
            mBinding.txtStatus.setText("Vacation");
        } else if (mEvonixUtil.getSPString(mContext.getResources().getString(R.string.profile_delivery_boy_status), "").equals("4")) {
            mBinding.switchStatus.setChecked(false);
            mBinding.txtStatus.setText("Unavailable");
        } else {
            mBinding.switchStatus.setChecked(true);
        }
    }
    public void getAllOrder() {
        loadAccountData();
    }

    public void onClick(int click) {
        switch (click) {
            case 1:
                mContext.overridePendingTransition(R.anim.enter, R.anim.exit);
                mContext.startActivity(new Intent(mContext, AccountInfoActivity.class));
                break;
            case 2:
                showOptions();
                break;
            case 3:
                mEvonixUtil.setSPString(R.string.evonix_util_navigation_selection, "Order Delivery History");
                DashboardActivity.h.sendEmptyMessage(0);
                break;
            case 4:
                mEvonixUtil.setSPString(R.string.evonix_util_navigation_selection, "Order Delivery History");
                DashboardActivity.h.sendEmptyMessage(0);
                break;
            case 5:
                mEvonixUtil.setSPString(R.string.evonix_util_navigation_selection, "Home");
                DashboardActivity.h.sendEmptyMessage(0);
                break;
            case 6:
                mEvonixUtil.setSPString(R.string.evonix_util_navigation_selection, "Order Delivery History");
                DashboardActivity.h.sendEmptyMessage(0);
                break;
        }
    }

    public void showOptions() {
      final Dialog dialog = new Dialog(mContext);
        final DialogStatusAlertBinding dialogCaptureImageBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_status_alert, null, false);
        dialog.setContentView(dialogCaptureImageBinding.getRoot());
        if (mBinding.switchStatus.isChecked()) {
        dialogCaptureImageBinding.llChangeOrderStatus.setVisibility(View.GONE);
        dialogCaptureImageBinding.llChangeDeliveryBoyStatus.setVisibility(View.VISIBLE);

            dialogCaptureImageBinding.rgStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (dialogCaptureImageBinding.rbUnavail.isChecked()) {
                        strStatusId = "4";
                    } else {
                        strStatusId = "3";
                    }


                }
            });
        } else {
            dialogCaptureImageBinding.llChangeOrderStatus.setVisibility(View.VISIBLE);
            dialogCaptureImageBinding.llChangeDeliveryBoyStatus.setVisibility(View.GONE);
            dialogCaptureImageBinding.txtNo.setVisibility(View.VISIBLE);
            dialogCaptureImageBinding.txtMessage.setText("Do you really want to change status Available?");
            strStatusId = "1";
        }


        dialogCaptureImageBinding.txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBinding.switchStatus.isChecked()) {
                    if (dialogCaptureImageBinding.rgStatus.getCheckedRadioButtonId() == -1) {
                        mEvonixUtil.TES("Please select status from above");
                    } else {
                        dialog.dismiss();
                        loadDeliveryBoyStatus(strStatusId, dialogCaptureImageBinding.edtComment.getText().toString().trim());
                    }
                } else {
                    dialog.dismiss();
                    loadDeliveryBoyStatus(strStatusId, dialogCaptureImageBinding.edtComment.getText().toString().trim());
                }
            }
        });

        dialogCaptureImageBinding.txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    dialog.dismiss();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }


    public void loadAccountData() {
        mEvonixUtil.showLoader(mContext);
        Call<DeliveryBoyDetailsWrapper>
                call = RestAdapter.createAPI().getDeliveryBoyDetails(
                Constant.ANDROID,
                mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token), ""));

        call.enqueue(new Callback<DeliveryBoyDetailsWrapper>() {
            @Override
            public void onResponse(Call<DeliveryBoyDetailsWrapper> call, Response<DeliveryBoyDetailsWrapper> response) {
                try {
                    if (response.isSuccessful()) {
                        mEvonixUtil.hideLoader();
                        mWrapper = response.body();
                        if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK && mWrapper.getDeliveryBoyDetailsList() != null) {
                            try {
                                List<DeliveryBoyDetailsWrapper.DeliveryBoyDetails> deliveryBoyDetails = mWrapper.getDeliveryBoyDetailsList();
                                for (DeliveryBoyDetailsWrapper.DeliveryBoyDetails data : deliveryBoyDetails) {
                                    mEvonixUtil.setSPString(mContext.getResources().getString(R.string.profile_delivery_boy_profile_image),data.getImage());
                                    mBinding.setDeliveryboyWrapper(data);
                                }
                                DashboardActivity.setProfileImage();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        mEvonixUtil.hideLoader();
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        if (rootObject.getString("status").equals("520")) {
                            mEvonixUtil.setLogout();
                            mContext.finish();
                            mContext.startActivity(new Intent(mContext, SplashActivity.class));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DeliveryBoyDetailsWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
    }


    public void loadDeliveryBoyStatus(String strStatusId, String comment) {
        mEvonixUtil.showLoader(mContext);
        Call<UpdateDeliveryBoyStatusWrapper>
                call = RestAdapter.createAPI().updateDeliveryPersonStatus(
                Constant.ANDROID,
                mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token), ""),
                strStatusId,
                comment);

        call.enqueue(new Callback<UpdateDeliveryBoyStatusWrapper>() {
            @Override
            public void onResponse(Call<UpdateDeliveryBoyStatusWrapper> call, Response<UpdateDeliveryBoyStatusWrapper> response) {
                try {
                    if (response.isSuccessful()) {
                        mEvonixUtil.hideLoader();
                        UpdateDeliveryBoyStatusWrapper mWrapper = response.body();
                        if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK && mWrapper.getDeliveryBoyStatus() != null) {
                            try {
                                List<UpdateDeliveryBoyStatusWrapper.DeliveryBoyStatus> deliveryBoyDetails = mWrapper.getDeliveryBoyStatus();
                                for (UpdateDeliveryBoyStatusWrapper.DeliveryBoyStatus data : deliveryBoyDetails) {
                                    mEvonixUtil.setSPString(mContext.getResources().getString(R.string.profile_delivery_boy_status), data.getStatusId());
                                }
                                getLocalData();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        mEvonixUtil.hideLoader();
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        if (rootObject.getString("status").equals("520")) {
                            mEvonixUtil.setLogout();
                            mContext.finish();
                            mContext.startActivity(new Intent(mContext, SplashActivity.class));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UpdateDeliveryBoyStatusWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
    }
}