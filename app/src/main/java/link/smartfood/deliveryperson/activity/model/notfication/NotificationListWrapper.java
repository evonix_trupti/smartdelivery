package link.smartfood.deliveryperson.activity.model.notfication;

import androidx.databinding.BaseObservable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NotificationListWrapper {
    @SerializedName("status")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("offset")
    @Expose
    private int offset;
    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("notificationStatus")
    @Expose
    private List<NotificationList> notificationList = new ArrayList();

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<NotificationList> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<NotificationList> notificationList) {
        this.notificationList = notificationList;
    }


    public class NotificationList extends BaseObservable {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("order_status")
        @Expose
        private String order_status;

        @SerializedName("order_id")
        @Expose
        private String order_id;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("message")
        @Expose
        private String message;

        @SerializedName("module")
        @Expose
        private String module;

        @SerializedName("color")
        @Expose
        private String color;

        @SerializedName("created_at")
        @Expose
        private String created_at;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getModule() {
            return module;
        }

        public void setModule(String module) {
            this.module = module;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
