package link.smartfood.deliveryperson.activity.viewModel.auth;

import android.app.Activity;
import android.graphics.Typeface;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.login.TermsofUseItem;
import link.smartfood.deliveryperson.databinding.ActivityPrivacyPolicyBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EvonixUtil;
import link.smartfood.deliveryperson.util.font.FontHelper;
import retrofit2.Call;
import retrofit2.Retrofit;

public class PrivacyPolicyViewModel extends ViewModel {
    public ActivityPrivacyPolicyBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    ArrayList<TermsofUseItem> TermsofUseItem = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    JSONArray jsonArray;


    public PrivacyPolicyViewModel(Activity mContext, ActivityPrivacyPolicyBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    public void setContentView() {
        mLayoutManager = new LinearLayoutManager(mContext);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        setFullBodyFont();

        jsonArray = new JSONArray();
        if (mEvonixUtil.isNetworkConnected()) {
            jumpActivity();
        } else {
            mEvonixUtil.TEL(mContext.getResources().getString(R.string.internet_is_not_connected));
        }
        mBinding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                jsonArray = new JSONArray();
                TermsofUseItem.clear();
                jumpActivity();
            }
        });
        mBinding.swipeRefresh.setColorSchemeResources(android.R.color.holo_green_light,
                android.R.color.holo_blue_bright,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mBinding.swipeRefresh.setRefreshing(true);
    }

    private void setFullBodyFont() {
        FontHelper.applyFont(mContext, mBinding.llMainView, "font/open_sans_semibold.ttf");
    }

    public void jumpActivity() {
        mBinding.swipeRefresh.setRefreshing(true);

        try {
            Call<JsonElement>
                    call = RestAdapter.createAPI().getContentPrivatePolicy(
                    Constant.APP_TYPE,
                    "1"                             //1 for consumer and 2 for restaurant
            );

            call.enqueue(new retrofit2.Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, retrofit2.Response<JsonElement> response) {
                    try {
                        mBinding.swipeRefresh.setRefreshing(false);
                        JSONObject rootObject = new JSONObject(response.body().toString());
                        if (response.isSuccessful() && rootObject.has("status")) {

                            jsonArray = rootObject.getJSONArray("contentStatus");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject innerObject = jsonArray.getJSONObject(i);
                                String id = innerObject.getString("id");
                                String heading = innerObject.getString("heading").trim();
                                String description = innerObject.getString("description").trim();

                                TermsofUseItem item = new TermsofUseItem();
                                item.setId(id);
                                item.setHeading(heading);
                                item.setDescription(description);
                                TermsofUseItem.add(item);
                            }
                            setAdapter(TermsofUseItem);
                        } else {
                            rootObject = new JSONObject(response.errorBody().string());
                            mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        }
                    } catch (Exception e) {
                        mEvonixUtil.LE(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    try {
                        mBinding.swipeRefresh.setRefreshing(false);
                        mEvonixUtil.TEL(R.string.error_to_process);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            mBinding.swipeRefresh.setRefreshing(false);
            mEvonixUtil.TEL(R.string.error_to_process);
            mEvonixUtil.LE(e.getMessage());
        }
    }

    private void setAdapter(ArrayList<link.smartfood.deliveryperson.activity.model.login.TermsofUseItem> termsofUseItem) {
        try {
            /*if (termsofUseItem != null) {
                PrivacyPolicyAdapter adapter = new PrivacyPolicyAdapter(mContext, termsofUseItem);
                mBinding.recyclerView.setAdapter(adapter);
                try {
                    mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                    mBinding.recyclerView.setLayoutManager(mLayoutManager);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
