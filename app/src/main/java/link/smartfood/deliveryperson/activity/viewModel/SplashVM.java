package link.smartfood.deliveryperson.activity.viewModel;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;

import androidx.lifecycle.ViewModel;

import org.json.JSONObject;

import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.tracking.TrackingIntervalsWrapper;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.activity.view.auth.LoginActivity;
import link.smartfood.deliveryperson.activity.view.dashboard.DashboardActivity;
import link.smartfood.deliveryperson.databinding.ActivitySplashscreenBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static link.smartfood.deliveryperson.network.Constant.DELIVERY_TRACKING_INTERVAL;
import static link.smartfood.deliveryperson.network.Constant.DELIVERY_TRACKING_SENT_INTERVAL;

public class SplashVM extends ViewModel {
    public ActivitySplashscreenBinding binding;
    private int SPLASH_TIME_OUT = 2000;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;

    public SplashVM(Activity mContext, ActivitySplashscreenBinding binding) {
        this.binding = binding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    public void setSplash() {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mEvonixUtil.LE("AuthToken: "+mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token),""));
                    mEvonixUtil.LE("InstanceID: "+mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_fcm),""));
                    nextActivity();
                }//end of run method

            }, SPLASH_TIME_OUT);
        }

    private void nextActivity() {
        try {
            if (mEvonixUtil.getSPBoolean(mContext.getResources().getString(R.string.evonix_util_is_login), false)) {
                mContext.startActivity(new Intent(mContext, DashboardActivity.class));
            } else {
                mContext.startActivity(new Intent(mContext, LoginActivity.class));
            }
        } catch (Exception e) {
        }
    }

    private void getTrackingIntervals() {
        mEvonixUtil.showLoader(mContext);
        Call<TrackingIntervalsWrapper>
                call = RestAdapter.createAPI().getTrackingIntervals(
                Constant.ANDROID,
                mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token), ""));

        call.enqueue(new Callback<TrackingIntervalsWrapper>() {
            @Override
            public void onResponse(Call<TrackingIntervalsWrapper> call, Response<TrackingIntervalsWrapper> response) {
                try {
                    if (response.isSuccessful()) {
                        mEvonixUtil.hideLoader();
                        TrackingIntervalsWrapper mWrapper = response.body();
                        if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK) {
                            try {
                                List<TrackingIntervalsWrapper.TrackingIntervals> loginList = mWrapper.getIntervalsData();
                                for (TrackingIntervalsWrapper.TrackingIntervals intervalsData : loginList) {
                                    DELIVERY_TRACKING_INTERVAL = Long.parseLong(intervalsData.getDeliveryTrackingInterval());
                                    DELIVERY_TRACKING_SENT_INTERVAL = Long.parseLong(intervalsData.getDeliveryTrackingSentInterval());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        mEvonixUtil.hideLoader();
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        if (rootObject.getString("status").equals("520")) {
                            mEvonixUtil.setLogout();
                            mContext.finish();
                            mContext.startActivity(new Intent(mContext, SplashActivity.class));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TrackingIntervalsWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
    }

}
