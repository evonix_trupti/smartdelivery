package link.smartfood.deliveryperson.activity.viewModel.dashboard;

import android.Manifest;
import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import androidx.appcompat.app.AlertDialog;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.databinding.ActivityDashboardBinding;
import link.smartfood.deliveryperson.util.EvonixUtil;

public class DashboardVM extends ViewModel {
    ActivityDashboardBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;

    public DashboardVM(Activity mContext, ActivityDashboardBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    public void requestMultiplePermissions() {
        Dexter.withActivity(mContext)
                .withPermissions(Manifest.permission.CALL_PHONE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }
}
