package link.smartfood.deliveryperson.activity.view;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.SplashVM;
import link.smartfood.deliveryperson.databinding.ActivitySplashscreenBinding;
import link.smartfood.deliveryperson.util.EvonixBaseActivity;

public class SplashActivity extends EvonixBaseActivity {
    SplashVM mViewModel;
    ActivitySplashscreenBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        bindingOfWidgets();
    }

    private void bindingOfWidgets() {
        mContext = SplashActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splashscreen);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new SplashVM(((Activity) mContext), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(SplashVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setSplashViewModel(mViewModel);
        initUI();
    }

    public void initUI() {
               mViewModel.setSplash();
    }
}
