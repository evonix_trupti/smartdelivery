package link.smartfood.deliveryperson.activity.viewModel;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.order.OrderList;
import link.smartfood.deliveryperson.databinding.ActivityMapsBinding;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsVM extends ViewModel implements OnMapReadyCallback,
        LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private GoogleMap mMap;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    public ActivityMapsBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    public List<OrderList> orderList;
    public OrderList singleData;
    MarkerOptions markerOptions;

    final String TAG = "MapsVM";
    ArrayList<LatLng> markerPoints;
    Marker addMarker = null;
    private LatLngBounds bounds;
    private LatLngBounds.Builder builder;

    public MapsVM(Activity mContext, ActivityMapsBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    public void setMap(SupportMapFragment mapFragment, ArrayList<OrderList> dataModel, OrderList singleData) {
        mapFragment.getMapAsync(this);
        orderList = dataModel;
        this.singleData = singleData;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                showSettingsDialog();
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            showSettingsDialog();
        }
    }

    public void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.dialog_permission_title));
        builder.setMessage(mContext.getResources().getString(R.string.dialog_permission_message));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                openSettings();
            }

            // navigating user to app settings
            private void openSettings() {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
                intent.setData(uri);
                mContext.startActivityForResult(intent, 101);
            }
        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        mContext.onBackPressed();
                        dialog.cancel();
                    }
                });
        builder.show();

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        markerOptions = new MarkerOptions();
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
        builder = new LatLngBounds.Builder();
        if (orderList != null) {
          /*  markerOptions.position(latLng);
            markerOptions.title("Your Location");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
            mCurrLocationMarker = mMap.addMarker(markerOptions);*/
            for (int i = 0; i < orderList.size(); i++) {
                OrderList orderModel = orderList.get(i);
                if (orderModel.getLatitude() != 0 && orderModel.getLongitude() != 0) {
                    createMarker(orderModel.getLatitude(), orderModel.getLongitude(), orderModel.getName(), orderModel.getDelivery_address(), "Delivery");
                }
            }
        } else if (singleData != null) {
            if (singleData.getListType().equalsIgnoreCase("home")) {
                createMarker(singleData.getLatitude(), singleData.getLongitude(), singleData.getName(), singleData.getDelivery_address(), "Delivery");
                createMarker(singleData.getRestaurant_lat(), singleData.getRestaurant_long(), singleData.getRestaurant_name(), "", "Restaurant");
                createMarker(location.getLatitude(), location.getLongitude(), "Your Location", "", "Delivery");
                LatLng latLngPosition = null;
                LatLngBounds latLngBounds = null;
                ArrayList<LatLng> points = new ArrayList<>();
                latLngPosition = new LatLng(singleData.getLatitude(), singleData.getLongitude());
                points.add(latLngPosition);
                latLngPosition = new LatLng(singleData.getRestaurant_lat(), singleData.getRestaurant_long());
                points.add(latLngPosition);
                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                for (LatLng latLngPoint : points)
                    boundsBuilder.include(latLngPoint);
                latLngBounds = boundsBuilder.build();
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(latLngBounds, 10);
                mMap.animateCamera(cu);
            } else {
                getHistoryLocationData();
            }
        }

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker arg0) {
                View view = null;
                try {
                    view = mContext.getLayoutInflater().inflate(R.layout.layout_marker_window, null);
                    if (!arg0.getTitle().equals("")) {
                        ((TextView) view.findViewById(R.id.txtName)).setText(arg0.getTitle());
                    } else {
                        ((TextView) view.findViewById(R.id.txtName)).setVisibility(View.GONE);
                    }
                    if (!arg0.getSnippet().equals("")) {
                        ((TextView) view.findViewById(R.id.txtAddress)).setText(arg0.getSnippet());
                    } else {
                        ((LinearLayout) view.findViewById(R.id.llAddress)).setVisibility(View.GONE);
                    }
                } catch (Exception ev) {
                    System.out.print(ev.getMessage());
                }
                return view;
            }
        });
    }

    protected void createMarker(double latitude, double longitude, String name, String address, String type) {
        addMarker = mMap.addMarker(markerOptions
                .position(new LatLng(latitude, longitude))
                .title(name)
                .snippet(address)
                .icon(bitmapDescriptorFromVector(mContext, type)));
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, String strType) {
        Drawable background = null;
        if (strType.equals("Delivery")) {
            background = ContextCompat.getDrawable(context, R.drawable.ic_placeholder_for_map);
        } else {
            background = ContextCompat.getDrawable(context, R.drawable.ic_restaurant);
        }
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    public void getHistoryLocationData() {
        try {
            JsonObject jsonObjectMain = new JsonObject();
            jsonObjectMain.addProperty("restaurant_id", singleData.getRestaurant_id());
            jsonObjectMain.addProperty("order_id", singleData.getId());
           /* jsonObjectMain.addProperty("restaurant_id", "2");
            jsonObjectMain.addProperty("order_id", "112233");*/
            Call<JsonElement> call = RestAdapter.createAPI2().getHistoryLocationData(jsonObjectMain);
            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    if (response.isSuccessful()) {
                        mEvonixUtil.LE(response.message() + " " + " Success");
                        ArrayList<LatLng> points = new ArrayList<>();
                        PolylineOptions lineOptions = new PolylineOptions();
                        try {
                            LatLng latLngPosition = null;
                            LatLngBounds latLngBounds = null;
                            JSONArray jsonArray = new JSONArray(String.valueOf(response.body()));

                            if (jsonArray != null && jsonArray.length() != 0) {
                                String originName = mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_restaurant_name), "");
                                String destName = jsonArray.getJSONObject(jsonArray.length() - 1).getString("consumer_name");

                                String originAddress = jsonArray.getJSONObject(0).getString("restaurant_address");
                                String destAddress = jsonArray.getJSONObject(jsonArray.length() - 1).getString("delivery_address");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    double lat = Double.parseDouble(jsonObject.getString("latitude"));
                                    double lng = Double.parseDouble(jsonObject.getString("longitude"));
                                    latLngPosition = new LatLng(lat, lng);
                                    points.add(latLngPosition);

                                }
                                LatLng origin = points.get(0);
                                LatLng dest = points.get(points.size() - 1);
                                mMap.addMarker(new MarkerOptions()
                                        .position(origin)
                                        .title(originName)
                                        .snippet(originAddress)
                                        .icon(bitmapDescriptorFromVector(mContext, "Delivery")));


                                mMap.addMarker(new MarkerOptions()
                                        .position(dest)
                                        .title(destName)
                                        .snippet(destAddress)
                                        .icon(bitmapDescriptorFromVector(mContext, "Delivery")));

                                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                                for (LatLng latLngPoint : points)
                                    boundsBuilder.include(latLngPoint);
                                latLngBounds = boundsBuilder.build();
                                // Adding all the points in the route to LineOptions
                                lineOptions.addAll(points);
                                lineOptions.width(10);
                                lineOptions.color(Color.RED);
                                if (lineOptions != null && latLngBounds != null) {
                                    mMap.addPolyline(lineOptions);
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 10));
//                                    mMap.animateCamera(CameraUpdateFactory.zoomTo(12));

                                /*List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dot(), new Gap(10f));
                                polyline.setPattern(pattern);*/
                                }
                            } else {
                                mEvonixUtil.TES("No data found");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        mEvonixUtil.LE(response.message() + " " + " faaaill");
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    mEvonixUtil.TES(t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
