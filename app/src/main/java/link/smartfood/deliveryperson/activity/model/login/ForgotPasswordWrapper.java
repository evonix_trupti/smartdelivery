package link.smartfood.deliveryperson.activity.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ForgotPasswordWrapper {
    @SerializedName("status")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("forgot_data")
    @Expose
    private List<ForgotPassword> forgotPasswordData = new ArrayList();

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ForgotPassword> getForgotPassword() {
        return forgotPasswordData;
    }

    public void setForgotPassword(List<ForgotPassword> forgotPasswordData) {
        this.forgotPasswordData = forgotPasswordData;
    }

    public class ForgotPassword {
        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("OTP")
        @Expose
        private String OTP;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getOTP() {
            return OTP;
        }

        public void setOTP(String OTP) {
            this.OTP = OTP;
        }
    }
}
