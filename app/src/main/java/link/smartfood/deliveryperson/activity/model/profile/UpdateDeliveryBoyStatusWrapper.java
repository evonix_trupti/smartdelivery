package link.smartfood.deliveryperson.activity.model.profile;

import androidx.databinding.BaseObservable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UpdateDeliveryBoyStatusWrapper {
    @SerializedName("status")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("deliverBoyStatus")
    @Expose
    private List<DeliveryBoyStatus> deliveryBoyStatus = new ArrayList();

    public List<DeliveryBoyStatus> getDeliveryBoyStatus() {
        return deliveryBoyStatus;
    }

    public void setDeliveryBoyStatus(List<DeliveryBoyStatus> deliveryBoyStatus) {
        this.deliveryBoyStatus = deliveryBoyStatus;
    }

    public class DeliveryBoyStatus extends BaseObservable {
        @SerializedName("statusId")
        @Expose
        private String statusId;

        @SerializedName("status")
        @Expose
        private String status;

        public String getStatusId() {
            return statusId;
        }

        public void setStatusId(String statusId) {
            this.statusId = statusId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }
}
