package link.smartfood.deliveryperson.activity.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CountryCodeWrapper {
    @SerializedName("status")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("countryCodeList")
    @Expose
    private List<CountryCode> countryCode = new ArrayList();

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CountryCode> getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(List<CountryCode> countryCode) {
        this.countryCode = countryCode;
    }

    public class CountryCode {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("code")
        @Expose
        private String code;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
