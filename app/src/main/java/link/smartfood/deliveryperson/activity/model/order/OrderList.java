package link.smartfood.deliveryperson.activity.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;


public class OrderList extends Observable implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("restaurant_id")
    @Expose
    private String restaurant_id;

    @SerializedName("consumer_name")
    @Expose
    private String name;

    @SerializedName("delivery_address")
    @Expose
    private String delivery_address;

    @SerializedName("store_address")
    @Expose
    private String store_address;

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getStore_address() {
        return store_address;
    }

    public void setStore_address(String store_address) {
        this.store_address = store_address;
    }

    @SerializedName("order_date")
    @Expose
    private String date;

    @SerializedName("order_time")
    @Expose
    private String time;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("restaurant_phone")
    @Expose
    private String restaurant_phone;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("timer")
    @Expose
    private String timer;

    @SerializedName("total")
    @Expose
    private String total;

    @SerializedName("subtotal")
    @Expose
    private String subtotal;

    @SerializedName("delivery_fee")
    @Expose
    private String delivery_fee;

    @SerializedName("order_status")
    @Expose
    private String order_status;

    @SerializedName("status_id")
    @Expose
    private String status_id;

    @SerializedName("status_color")
    @Expose
    private String status_color;

    @SerializedName("delivery_time")
    @Expose
    private String delivery_time;

    @SerializedName("listType")
    @Expose
    private String listType;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    @SerializedName("consumer_id")
    @Expose
    private String consumer_id;

    @SerializedName("consumer_email")
    @Expose
    private String consumer_email;

    @SerializedName("restaurant_name")
    @Expose
    private String restaurant_name;

    @SerializedName("customer_address_url")
    @Expose
    private String customer_address_url;

    @SerializedName("store_address_url")
    @Expose
    private String store_address_url;

    public String getCustomer_address_url() {
        return customer_address_url;
    }

    public void setCustomer_address_url(String customer_address_url) {
        this.customer_address_url = customer_address_url;
    }

    public String getStore_address_url() {
        return store_address_url;
    }

    public void setStore_address_url(String store_address_url) {
        this.store_address_url = store_address_url;
    }

    @SerializedName("order_description")
    @Expose
    private String order_description;

    @SerializedName("restaurant_lat")
    @Expose
    private double restaurant_lat;

    @SerializedName("restaurant_long")
    @Expose
    private double restaurant_long;

    @SerializedName("order_detail_array")
    @Expose
    private List<FoodList> foodList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(String delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    public String getStatus_color() {
        return status_color;
    }

    public void setStatus_color(String status_color) {
        this.status_color = status_color;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public List<FoodList> getFoodList() {
        return foodList;
    }

    public void setFoodList(List<FoodList> foodList) {
        this.foodList = foodList;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getConsumer_id() {
        return consumer_id;
    }

    public void setConsumer_id(String consumer_id) {
        this.consumer_id = consumer_id;
    }

    public String getConsumer_email() {
        return consumer_email;
    }

    public void setConsumer_email(String consumer_email) {
        this.consumer_email = consumer_email;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getOrder_description() {
        return order_description;
    }

    public void setOrder_description(String order_description) {
        this.order_description = order_description;
    }

    public String getRestaurant_phone() {
        return restaurant_phone;
    }

    public void setRestaurant_phone(String restaurant_phone) {
        this.restaurant_phone = restaurant_phone;
    }

    public double getRestaurant_lat() {
        return restaurant_lat;
    }

    public void setRestaurant_lat(double restaurant_lat) {
        this.restaurant_lat = restaurant_lat;
    }

    public double getRestaurant_long() {
        return restaurant_long;
    }

    public void setRestaurant_long(double restaurant_long) {
        this.restaurant_long = restaurant_long;
    }
}
