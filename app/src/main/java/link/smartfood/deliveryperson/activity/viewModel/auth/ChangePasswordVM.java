package link.smartfood.deliveryperson.activity.viewModel.auth;

import android.app.Activity;

import androidx.lifecycle.ViewModel;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;

import org.json.JSONObject;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.login.ChangePasswordWrapper;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.databinding.ActivityChangePasswordBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordVM extends ViewModel {
    public ActivityChangePasswordBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;

    public ChangePasswordVM(Activity mContext, ActivityChangePasswordBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    public void onClick() {
        if (validate()) {
            loadChangePassword();
        }
    }

    public void setTextWatcherMethods() {
        mBinding.inputPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mBinding.inputLayoutPassword.setError(null);
//                mBinding.inputLayoutPassword.setPasswordVisibilityToggleEnabled(true);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mBinding.inputCurrentPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mBinding.inputLayoutCurrentPassword.setError(null);
//                mBinding.inputLayoutCurrentPassword.setPasswordVisibilityToggleEnabled(true);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.inputConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mBinding.inputLayoutConfirmPassword.setError(null);
//                mBinding.inputLayoutConfirmPassword.setPasswordVisibilityToggleEnabled(true);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private boolean validate() {
        if (mBinding.inputCurrentPassword.getText().toString().trim().equals("")) {
            mBinding.inputCurrentPassword.requestFocus();
            mBinding.inputCurrentPassword.setFocusable(true);
            mBinding.inputLayoutCurrentPassword.setError("Please enter current Password");
//            mBinding.inputLayoutCurrentPassword.setPasswordVisibilityToggleEnabled(false);
            return false;
        } else if (mBinding.inputCurrentPassword.getText().toString().trim().length() < 6) {
            mBinding.inputCurrentPassword.requestFocus();
            mBinding.inputCurrentPassword.setFocusable(true);
            mBinding.inputLayoutCurrentPassword.setError("Password should be of 6 digits");
//            mBinding.inputLayoutCurrentPassword.setPasswordVisibilityToggleEnabled(false);
            return false;
        } else if (mBinding.inputPassword.getText().toString().trim().equals("")) {
            mBinding.inputPassword.requestFocus();
            mBinding.inputPassword.setFocusable(true);
            mBinding.inputLayoutPassword.setError("Please enter new Password");
//            mBinding.inputLayoutPassword.setPasswordVisibilityToggleEnabled(false);
            return false;
        } else if (mBinding.inputPassword.getText().toString().trim().length() < 6) {
            mBinding.inputPassword.requestFocus();
            mBinding.inputPassword.setFocusable(true);
            mBinding.inputLayoutPassword.setError("Password should be of 6 digits");
//            mBinding.inputLayoutPassword.setPasswordVisibilityToggleEnabled(false);
            return false;
        } else if (mBinding.inputConfirmPassword.getText().toString().trim().equals("")) {
            mBinding.inputConfirmPassword.requestFocus();
            mBinding.inputConfirmPassword.setFocusable(true);
            mBinding.inputLayoutConfirmPassword.setError("Please enter confirm Password");
//            mBinding.inputLayoutConfirmPassword.setPasswordVisibilityToggleEnabled(false);
            return false;
        } else if (mBinding.inputConfirmPassword.getText().toString().trim().length() < 6) {
            mBinding.inputConfirmPassword.requestFocus();
            mBinding.inputConfirmPassword.setFocusable(true);
            mBinding.inputLayoutConfirmPassword.setError("Password should be of 6 digits");
//            mBinding.inputLayoutConfirmPassword.setPasswordVisibilityToggleEnabled(false);
            return false;
        } else if (!mBinding.inputPassword.getText().toString().trim().equals(mBinding.inputConfirmPassword.getText().toString().trim())) {
            mBinding.inputConfirmPassword.requestFocus();
            mBinding.inputConfirmPassword.setFocusable(true);
//            mBinding.inputLayoutConfirmPassword.setPasswordVisibilityToggleEnabled(false);
            mBinding.inputLayoutConfirmPassword.setError("Password and confirm password must be same");
            return false;
        }
        return true;
    }

    private void loadChangePassword() {
        mEvonixUtil.showLoader(mContext);
        Call<ChangePasswordWrapper>
                call = RestAdapter.createAPI().changePasssword(
                Constant.ANDROID,
                mEvonixUtil.getSPString(mContext.getResources().getString(R.string.evonix_util_token), ""),
                mBinding.inputCurrentPassword.getText().toString().trim(),
                mBinding.inputPassword.getText().toString().trim(),
                mBinding.inputConfirmPassword.getText().toString().trim());

        call.enqueue(new Callback<ChangePasswordWrapper>() {
            @Override
            public void onResponse(Call<ChangePasswordWrapper> call, Response<ChangePasswordWrapper> response) {
                try {
                    if (response.isSuccessful()) {
                        mEvonixUtil.hideLoader();
                        ChangePasswordWrapper mWrapper = response.body();
                        if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK) {
                            try {
                                mEvonixUtil.TES(mWrapper.getMessage());
                                mContext.onBackPressed();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        mEvonixUtil.hideLoader();
                        JSONObject rootObject = new JSONObject(response.errorBody().string());
                        mEvonixUtil.TES(rootObject.getString("errorMessage"));
                        if (rootObject.getString("status").equals("520")) {
                            mEvonixUtil.setLogout();
                            mContext.finish();
                            mContext.startActivity(new Intent(mContext, SplashActivity.class));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordWrapper> call, Throwable t) {
                mEvonixUtil.hideLoader();
                mEvonixUtil.TES(mContext.getResources().getString(R.string.error_to_api));
            }
        });
    }


}
