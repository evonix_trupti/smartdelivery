package link.smartfood.deliveryperson.activity.view.dashboard;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.dashboard.ProfileFragmentVM;
import link.smartfood.deliveryperson.databinding.FragmentProfileBinding;
import link.smartfood.deliveryperson.util.EvonixUtil;


public class ProfileFragment extends Fragment {
    ProfileFragmentVM mViewModel;
    public FragmentProfileBinding mBinding;
    private String mParam1;
    private String mParam2;

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(EvonixUtil.ARG_PARAM1, param1);
        args.putString(EvonixUtil.ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(EvonixUtil.ARG_PARAM1);
            mParam2 = getArguments().getString(EvonixUtil.ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new ProfileFragmentVM((getActivity()), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(ProfileFragmentVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setProfileFragmentVM(mViewModel);
        initUI();
    }

    private void initUI() {
        mViewModel.getLocalData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel.getAllOrder();
    }
}