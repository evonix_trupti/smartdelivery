package link.smartfood.deliveryperson.activity.viewModel.auth;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;

import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.login.TermsofUseItem;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.adapter.TermsofUseAdapter;
import link.smartfood.deliveryperson.databinding.ActivityPrivacyPolicyBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
import link.smartfood.deliveryperson.network.RetrofitApi;
import link.smartfood.deliveryperson.util.EvonixUtil;
import link.smartfood.deliveryperson.util.font.FontHelper;
import retrofit2.Call;

public class TermsOfUseViewModel extends ViewModel {
    public ActivityPrivacyPolicyBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    ArrayList<link.smartfood.deliveryperson.activity.model.login.TermsofUseItem> TermsofUseItem = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    JSONArray jsonArray;

    public TermsOfUseViewModel(Activity mContext, ActivityPrivacyPolicyBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil(mContext);
    }


    public void setContentView() {
        mLayoutManager = new LinearLayoutManager(mContext);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        setFullBodyFont();
        jsonArray = new JSONArray();
        if (mEvonixUtil.isNetworkConnected()) {
            jumpActivity();
        } else {
            mEvonixUtil.TEL(mContext.getResources().getString(R.string.internet_is_not_connected));
        }
        mBinding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                jsonArray = new JSONArray();
                TermsofUseItem.clear();
                if (mEvonixUtil.isNetworkConnected()) {
                    jumpActivity();
                } else {
                    mEvonixUtil.TEL(mContext.getResources().getString(R.string.internet_is_not_connected));
                }
            }
        });
        mBinding.swipeRefresh.setColorSchemeResources(android.R.color.holo_green_light,
                android.R.color.holo_blue_bright,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mBinding.swipeRefresh.setRefreshing(true);
    }

    private void setFullBodyFont() {
        FontHelper.applyFont(mContext, mBinding.llMainView, "font/open_sans_semibold.ttf");
    }

    public void jumpActivity() {
        mBinding.swipeRefresh.setRefreshing(true);
        try {
            RetrofitApi retrofit = RestAdapter.createAPI();
            Call<JsonElement> call = retrofit.getContentTermsAndCond(
                    Constant.APP_TYPE,
                    "1"//1 for consumer and 2 for restaurant
            );
            call.enqueue(new retrofit2.Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, retrofit2.Response<JsonElement> response) {
                    try {
                        mBinding.swipeRefresh.setRefreshing(false);
                        switch (response.code()) {
                            case Constant.RESULT_OK:

                                JSONObject rootObject = new JSONObject(response.body().toString());
                                if (rootObject.has("contentStatus") && !rootObject.isNull("contentStatus")) {
                                    jsonArray = rootObject.getJSONArray("contentStatus");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject innerObject = jsonArray.getJSONObject(i);
                                        String id = innerObject.getString("id");
                                        String heading = innerObject.getString("heading").trim();
                                        String description = innerObject.getString("description").trim();

                                        TermsofUseItem item = new TermsofUseItem();
                                        item.setId(id);
                                        item.setHeading(heading);
                                        item.setDescription(description);
                                        TermsofUseItem.add(item);
                                    }
                                    loadPizza(TermsofUseItem);
                                } else {
                                    rootObject = new JSONObject(response.errorBody().string());
                                    mEvonixUtil.TES(rootObject.getString("errorMessage"));
                                }
                                break;

                            case Constant.RESULT_FAIL:

                                rootObject = new JSONObject(response.errorBody().string());
                                mEvonixUtil.TES(rootObject.getString("errorMessage"));
                                break;
                            case Constant.RESULT_LOGOUT:

                                rootObject = new JSONObject(response.errorBody().string());
                                mEvonixUtil.TES(rootObject.getString("errorMessage"));
                                mEvonixUtil.setLogout();
                                mContext.finish();
                                mContext.startActivity(new Intent(mContext, SplashActivity.class));
                                break;
                        }
                    } catch (Exception e) {
                        mBinding.swipeRefresh.setRefreshing(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    try {

                        mBinding.swipeRefresh.setRefreshing(false);
                        mEvonixUtil.TEL(R.string.error_to_process);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            mBinding.swipeRefresh.setRefreshing(false);
            mEvonixUtil.TEL(R.string.error_to_process);
            mEvonixUtil.LE(e.getMessage());
        }
    }


    private void loadPizza(ArrayList<TermsofUseItem> TermsofUseItem) {
        try {
            if (TermsofUseItem != null) {
                TermsofUseAdapter adapter = new TermsofUseAdapter(mContext, TermsofUseItem);
                mBinding.recyclerView.setAdapter(adapter);
                try {
                    mLayoutManager = new LinearLayoutManager(mContext);
                    mBinding.recyclerView.setLayoutManager(mLayoutManager);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}

