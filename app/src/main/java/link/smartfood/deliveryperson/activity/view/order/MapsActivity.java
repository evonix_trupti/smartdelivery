package link.smartfood.deliveryperson.activity.view.order;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.order.OrderList;
import link.smartfood.deliveryperson.activity.viewModel.MapsVM;
import link.smartfood.deliveryperson.databinding.ActivityMapsBinding;
import link.smartfood.deliveryperson.util.EvonixBaseActivity;
import link.smartfood.deliveryperson.util.EvonixUtil;

public class MapsActivity extends EvonixBaseActivity {
    ActivityMapsBinding mBinding;
    MapsVM mViewModel;
    private ArrayList<OrderList> orderList = new ArrayList<>();
    private  OrderList dataModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingOfWidgets();
    }

    private void bindingOfWidgets() {
        mContext = MapsActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_maps);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new MapsVM(((Activity) mContext), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(MapsVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setMapsVM(mViewModel);
        mEvonixUtil = new EvonixUtil(mContext);
        getIntentData();
        setToolBar();
        initUI();
    }

    private void getIntentData() {
        orderList = (ArrayList<OrderList>) getIntent().getSerializableExtra("listData");
        dataModel = (OrderList) getIntent().getSerializableExtra("data");
    }

    private void setToolBar() {
        setSupportActionBar(mBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }

    public void initUI() {
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mViewModel.setMap(mapFragment, orderList, dataModel);
    }
}
