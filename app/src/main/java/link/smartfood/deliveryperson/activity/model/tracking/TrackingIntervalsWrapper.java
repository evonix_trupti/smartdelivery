package link.smartfood.deliveryperson.activity.model.tracking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TrackingIntervalsWrapper {
    @SerializedName("status")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("timeInterval")
    @Expose
    private List<TrackingIntervals> intervalsData = new ArrayList();

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TrackingIntervals> getIntervalsData() {
        return intervalsData;
    }

    public void setIntervalsData(List<TrackingIntervals> intervalsData) {
        this.intervalsData = intervalsData;
    }

    public class TrackingIntervals {
        @SerializedName("deliveryTrackingInterval")
        @Expose
        private String deliveryTrackingInterval;

        @SerializedName("deliveryTrackingSentInterval")
        @Expose
        private String deliveryTrackingSentInterval;

        public String getDeliveryTrackingInterval() {
            return deliveryTrackingInterval;
        }

        public void setDeliveryTrackingInterval(String deliveryTrackingInterval) {
            this.deliveryTrackingInterval = deliveryTrackingInterval;
        }

        public String getDeliveryTrackingSentInterval() {
            return deliveryTrackingSentInterval;
        }

        public void setDeliveryTrackingSentInterval(String deliveryTrackingSentInterval) {
            this.deliveryTrackingSentInterval = deliveryTrackingSentInterval;
        }

    }
}
