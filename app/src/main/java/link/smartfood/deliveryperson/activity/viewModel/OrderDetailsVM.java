package link.smartfood.deliveryperson.activity.viewModel;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import link.smartfood.deliveryperson.BuildConfig;
import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.order.FoodList;
import link.smartfood.deliveryperson.activity.model.order.OrderList;
import link.smartfood.deliveryperson.activity.model.status.UpdateStatusWrapper;
import link.smartfood.deliveryperson.activity.view.SplashActivity;
import link.smartfood.deliveryperson.activity.view.dashboard.HomeFragment;
import link.smartfood.deliveryperson.activity.view.order.OrderDetailsActivity;
import link.smartfood.deliveryperson.activity.viewModel.dashboard.HomeVM;
import link.smartfood.deliveryperson.adapter.FoodAdapter;
import link.smartfood.deliveryperson.databinding.ActivityOrderDetailsBinding;
import link.smartfood.deliveryperson.databinding.DialogStatusAlertBinding;
import link.smartfood.deliveryperson.network.Constant;
import link.smartfood.deliveryperson.network.RestAdapter;
//import link.smartfood.deliveryperson.service.LocationMonitoringService;
import link.smartfood.deliveryperson.util.EvonixUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsVM extends ViewModel implements ActivityCompat.OnRequestPermissionsResultCallback {
    public ActivityOrderDetailsBinding mBinding;
    private Activity mContext;
    private EvonixUtil mEvonixUtil;
    private FoodAdapter adapter;
    private String strOrderId = "", strStatusId = "", strStoreAddress = "", strConsumerAddress = "", strStoreAddressUrl = "", strConsumerAddressUrl = "";
    private CountDownTimer myCountDownTimer;
    // location last updated time
    private static final String TAG = OrderDetailsVM.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    LocationManager locationManager;
    String latitude, longitude;
    private static final int REQUEST_LOCATION = 1;

    public OrderDetailsVM(Activity mContext, ActivityOrderDetailsBinding mBinding) {
        this.mBinding = mBinding;
        this.mContext = mContext;
        mEvonixUtil = new EvonixUtil( mContext );
    }

    public void setStatusColor(OrderList dataModel) {
        if (strStatusId.equals( "7" )) {
            mBinding.txtOutForDelivery.setBackgroundColor( mContext.getResources().getColor( R.color.grey_700 ) );
        }
        if (strStatusId.equals( "8" )) {
            mBinding.txtOutForDelivery.setBackgroundColor( mContext.getResources().getColor( R.color.grey_700 ) );
            mBinding.txtDelivered.setBackgroundColor( mContext.getResources().getColor( R.color.grey_700 ) );
        }

        if (strStatusId.equals( "13" )) {
            mBinding.txtOutForDelivery.setBackgroundColor( mContext.getResources().getColor( R.color.grey_700 ) );
            mBinding.txtCustUnavail.setBackgroundColor( mContext.getResources().getColor( R.color.grey_700 ) );
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onClick(int click) {
        switch (click) {
            case 1:

                if (strStatusId.equals( "16" )) {
                    showAlertDialog( "order pickup", "7" ); //Comment: confirmation for Out For Delivery
//                    showAlertDialog( mBinding.txtOutForDelivery.getText().toString(), "7" ); //Comment: confirmation for Out For Delivery
//                    mBinding.txtOutForDelivery.setBackgroundColor( mContext.getResources().getColor( R.color.grey_700 ) );
                } else {
//                    mBinding.txtOutForDelivery.setClickable( false );
//                    mBinding.txtOutForDelivery.setBackgroundColor( mContext.getResources().getColor( R.color.grey_700 ) );
//                    mEvonixUtil.TEL( "To make order status " + mBinding.txtOutForDelivery.getText().toString() + ", the order status should be Ready for pickup" );
                }
                break;
            case 2:
                if (strStatusId.equals( "15" ) || strStatusId.equals( "7" )) {
                    showAlertDialog( "order delivery", "8" ); //Comment: confirmation for Delivered
//                    showAlertDialog( mBinding.txtDelivered.getText().toString(), "8" ); //Comment: confirmation for Delivered
                } else {
                    if (strStatusId.equals( "13" )) {
                        mEvonixUtil.TEL( "You cannot change the order status to delivered if customer is unavailable" );
                    }else{
                        mEvonixUtil.TEL( "You cannot change the order status until it get picked up" );
                    }

//                    mBinding.txtDelivered.setClickable( false );
//                    mBinding.txtDelivered.setBackgroundColor( mContext.getResources().getColor( R.color.grey_700 ) );
//                    mEvonixUtil.TEL( "To make order status " + mBinding.txtDelivered.getText().toString() + ", the order status should be Overdue or Out for delivery" );
                }
                break;
            case 3:
                if (strStatusId.equals( "15" ) || strStatusId.equals( "7" )) {
                    showAlertDialog( mBinding.txtCustUnavail.getText().toString(), "13" ); //Comment:confirmation for Cust Unavail
                } else {
                    if (strStatusId.equals( "8" )) {
                        mEvonixUtil.TEL( "These order is already delivered" );
                    }
                    else if (strStatusId.equals( "13" )) {
                        mEvonixUtil.TEL( "Order status is customer not responding" );
                    } else {
//                        mEvonixUtil.TEL( "To make order status " + mBinding.txtCustUnavail.getText().toString() + ", the order status should be Overdue or Out for delivery" );
                        mEvonixUtil.TEL( "You cannot change the order status until it get picked up" );

//                        mBinding.txtCustUnavail.setClickable( false );
//                        mBinding.txtCustUnavail.setBackgroundColor( mContext.getResources().getColor( R.color.grey_700 ) );
                    }
                }
                break;
            case 4:

            case 5:
                if (isPermissionGranted()) {
                    try {
                        final Dialog dialog = new Dialog( mContext );
                        final DialogStatusAlertBinding dialogCaptureImageBinding = DataBindingUtil.inflate( LayoutInflater.from( mContext ), R.layout.dialog_status_alert, null, false );
                        dialog.setContentView( dialogCaptureImageBinding.getRoot() );
                        dialogCaptureImageBinding.llChangeDeliveryBoyStatus.setVisibility( View.GONE );
                        dialogCaptureImageBinding.llChangeOrderStatus.setVisibility( View.VISIBLE );
                        dialogCaptureImageBinding.txtNo.setVisibility( View.VISIBLE );
                        dialogCaptureImageBinding.txtHeader.setText( "Call Alert" );
                        dialogCaptureImageBinding.txtMessage.setText( "Are you sure you want to call this customer?" );

                        dialogCaptureImageBinding.txtYes.setOnClickListener( new View.OnClickListener() {
                            @SuppressLint("MissingPermission")
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                Intent callIntent = new Intent( Intent.ACTION_CALL );
                                callIntent.setData( Uri.parse( "tel:" + mBinding.txtRestMobileNo.getText().toString().trim() ) );
                                mContext.startActivity( callIntent );
                            }
                        } );

                        dialogCaptureImageBinding.txtNo.setOnClickListener( new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    dialog.dismiss();
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        } );
                        dialog.setCancelable( true );
                        dialog.show();
                    } catch (Exception e) {
                        mEvonixUtil.TES( "Unable to connect for a moment" );
                        e.printStackTrace();
                    }

                }
                break;
            case 6:
                if (isPermissionGranted()) {
                    try {
                        final Dialog dialog = new Dialog( mContext );
                        final DialogStatusAlertBinding dialogCaptureImageBinding = DataBindingUtil.inflate( LayoutInflater.from( mContext ), R.layout.dialog_status_alert, null, false );
                        dialog.setContentView( dialogCaptureImageBinding.getRoot() );
                        dialogCaptureImageBinding.llChangeDeliveryBoyStatus.setVisibility( View.GONE );
                        dialogCaptureImageBinding.llChangeOrderStatus.setVisibility( View.VISIBLE );
                        dialogCaptureImageBinding.txtNo.setVisibility( View.VISIBLE );
                        dialogCaptureImageBinding.txtHeader.setText( "Call Alert" );
                        dialogCaptureImageBinding.txtMessage.setText( "Are you sure you want to call this customer?" );

                        dialogCaptureImageBinding.txtYes.setOnClickListener( new View.OnClickListener() {
                            @SuppressLint("MissingPermission")
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                Intent callIntent = new Intent( Intent.ACTION_CALL );
                                callIntent.setData( Uri.parse( "tel:" + mBinding.txtMobileNo.getText().toString().trim() ) );
                                mContext.startActivity( callIntent );
                            }
                        } );

                        dialogCaptureImageBinding.txtNo.setOnClickListener( new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    dialog.dismiss();
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        } );
                        dialog.setCancelable( true );
                        dialog.show();
                    } catch (Exception e) {
                        mEvonixUtil.TES( "Unable to connect for a moment" );
                        e.printStackTrace();
                    }

                }
                break;
            case 7:
                Intent intent = new Intent( android.content.Intent.ACTION_VIEW,
                        Uri.parse( strStoreAddressUrl ) );
                ((Activity) mContext).startActivity( intent );
                break;
            case 8:
                Intent intent1 = new Intent( android.content.Intent.ACTION_VIEW,
                        Uri.parse( strConsumerAddressUrl ) );
                ((Activity) mContext).startActivity( intent1 );
                break;

        }
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission( mContext, android.Manifest.permission.CALL_PHONE )
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v( "TAG", "Permission is granted" );
                return true;
            } else {
                Log.v( "TAG", "Permission is revoked" );
                ActivityCompat.requestPermissions( (Activity) mContext, new String[]{Manifest.permission.CALL_PHONE}, 1 );
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v( "TAG", "Permission is granted" );
            return true;
        }
    }


    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder( mContext );
        builder.setMessage( "Enable GPS" ).setCancelable( false ).setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mContext.startActivity( new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS ) );
            }
        } ).setNegativeButton( "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        } );
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                mContext, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mContext, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( mContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION );
        } else {
            if (mContext.checkSelfPermission( Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && mContext.checkSelfPermission( Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
            Location locationGPS = locationManager.getLastKnownLocation( LocationManager.GPS_PROVIDER );
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf( lat );
                longitude = String.valueOf( longi );
                Log.d( "YourLocation:", "\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude );
            } else {
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText( mContext, "Permission granted", Toast.LENGTH_SHORT ).show();
                    //call_action();
                } else {
                    Toast.makeText( mContext, "Permission denied", Toast.LENGTH_SHORT ).show();
                }
                return;
            }
            case REQUEST_PERMISSIONS_REQUEST_CODE:
                if (grantResults.length <= 0) {
                    Log.i( TAG, "User interaction was cancelled." );
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.i( TAG, "Permission granted, updates requested, starting location updates" );
                    startStep3();

                } else {
                    showSnackbar( "Needed permission",
                            "Settings", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // Build intent that displays the App settings screen.
                                    Intent intent = new Intent();
                                    intent.setAction(
                                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS );
                                    Uri uri = Uri.fromParts( "package",
                                            BuildConfig.APPLICATION_ID, null );
                                    intent.setData( uri );
                                    intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                                    mContext.startActivity( intent );
                                }
                            } );
                }
        }
    }

    private void showAlertDialog(final String strStatus, final String strId) {
        final Dialog dialog = new Dialog( mContext );
        final DialogStatusAlertBinding dialogStatusAlertBinding = DataBindingUtil.inflate( LayoutInflater.from( mContext ), R.layout.dialog_status_alert, null, false );
        dialog.setContentView( dialogStatusAlertBinding.getRoot() );
        dialogStatusAlertBinding.llChangeOrderStatus.setVisibility( View.VISIBLE );
        dialogStatusAlertBinding.llChangeDeliveryBoyStatus.setVisibility( View.GONE );
        dialogStatusAlertBinding.txtMessage.setText( "Do you want to confirm "+ strStatus + "?" );
//        dialogStatusAlertBinding.txtMessage.setText( "Can mark an order " + " " + strStatus + "?" );
        dialogStatusAlertBinding.txtNo.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        } );
        dialogStatusAlertBinding.txtYes.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    locationManager = (LocationManager) mContext.getSystemService( Context.LOCATION_SERVICE );
                    if (!locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER )) {
                        OnGPS();
                    } else {
                        getLocation();
                    }
                }
                loadStatusUpdate();
                dialog.dismiss();
            }

            private void loadStatusUpdate() {
                mEvonixUtil.showLoader( mContext );
                Call<UpdateStatusWrapper>
                        call = RestAdapter.createAPI().updateOrderStatus(
                        Constant.ANDROID,
                        mEvonixUtil.getSPString( mContext.getResources().getString( R.string.evonix_util_token ), "" ),
                        strOrderId,
                        strId,
                        latitude,
                        longitude,
                        "" );

                call.enqueue( new Callback<UpdateStatusWrapper>() {
                    @Override
                    public void onResponse(Call<UpdateStatusWrapper> call, Response<UpdateStatusWrapper> response) {
                        try {
                            if (response.isSuccessful()) {
                                mEvonixUtil.hideLoader();
                                UpdateStatusWrapper mWrapper = response.body();

                                if (mWrapper != null && mWrapper.getSuccess() == Constant.RESULT_OK) {
                                    try {
                                        /*if (strId.e("out for delivery")) {
                                            startStep1();
                                        } else {
                                            mContext.stopService(new Intent(mContext, LocationMonitoringService.class));
                                        }*/

                                        try {
                                            HomeFragment.h.sendEmptyMessage( 2 );
                                            mContext.onBackPressed();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

//                                        HomeVM.h.sendEmptyMessage( 2 );
//                                        mEvonixUtil.TES( mWrapper.getMessage() );
//                                        mContext.onBackPressed();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                mEvonixUtil.hideLoader();
                                dialog.dismiss();
                                JSONObject rootObject = new JSONObject( response.errorBody().string() );
                                mEvonixUtil.TES( rootObject.getString( "errorMessage" ) );
                                if (rootObject.getString( "status" ).equals( "520" )) {
                                    mEvonixUtil.setLogout();
                                    mContext.finish();
                                    mContext.startActivity( new Intent( mContext, SplashActivity.class ) );
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateStatusWrapper> call, Throwable t) {
                        mEvonixUtil.hideLoader();
                        mEvonixUtil.TES( mContext.getResources().getString( R.string.error_to_api ) );
                    }
                } );
            }
        } );

        dialog.setCancelable( true );
        dialog.show();
    }

    public void setRecycler(final List<FoodList> foodList) {
        adapter = new FoodAdapter( mContext, foodList );
        mBinding.recyclerView.setAdapter( adapter );
    }

    public void setTimer(OrderList dataModel, String strTimer) {
        strOrderId = dataModel.getId();                 //Comment: to get orderId for status update, no link with timer
        strStatusId = dataModel.getStatus_id();
        strStoreAddressUrl = dataModel.getStore_address_url();
        strConsumerAddressUrl = dataModel.getCustomer_address_url();
        try {
            if (myCountDownTimer != null) {
                myCountDownTimer.cancel();
            }
            if (strTimer.equals( "00:00" )) {
                mBinding.txtTimer.setText( strTimer );
            } else {
                String namepass[] = strTimer.split( ":" );
                int minsInt = Integer.parseInt( namepass[0] );
                int secInt = Integer.parseInt( namepass[1] );
                int longTime = ((minsInt * 60) + secInt) * 1000;
                myCountDownTimer = new CountDownTimer( longTime, 1000 ) { // adjust the milli seconds here
                    public void onTick(long millisUntilFinished) {
                        mBinding.txtTimer.setText( "" + String.format( "%02d : %02d",
                                TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished ),
                                TimeUnit.MILLISECONDS.toSeconds( millisUntilFinished ) -
                                        TimeUnit.MINUTES.toSeconds( TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished ) ) ) );
                    }

                    public void onFinish() {
                        mBinding.txtTimer.setText( "00:00" );
                        if (myCountDownTimer != null) {
                            myCountDownTimer.cancel();
                        }
                        mContext.onBackPressed();
                    }
                }.start();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    /**
     * Step 1: Check Google Play services
     */
    private void startStep1() {
        //Check whether this user has installed Google play service which is being used by Location updates.
        if (isGooglePlayServicesAvailable()) {
            //Passing null to indicate that it is executing for the first time.
            startStep2( null );
        } else {
            Toast.makeText( mContext, "Google service unavailable", Toast.LENGTH_LONG ).show();
        }
    }


    /**
     * Step 2: Check & Prompt Internet connection
     */
    private Boolean startStep2(DialogInterface dialog) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            promptInternetConnect();
            return false;
        }


        if (dialog != null) {
            dialog.dismiss();
        }

        //Yes there is active internet connection. Next check Location is granted by user or not.
        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.
            startStep3();
        } else {  //No user has not granted the permissions yet. Request now.
            requestPermissions();
        }
        return true;
    }

    /**
     * Show A Dialog with button to refresh the internet state.
     */
    private void promptInternetConnect() {
        AlertDialog.Builder builder = new AlertDialog.Builder( mContext );
        builder.setTitle( "No Internet" );
        builder.setMessage( "No Internet" );

        String positiveText = "Refresh";
        builder.setPositiveButton( positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Block the Application Execution until user grants the permissions
                        if (startStep2( dialog )) {
                            //Now make sure about location permission.
                            if (checkPermissions()) {
                                startStep3();
                            } else if (!checkPermissions()) {
                                requestPermissions();
                            }
                        }
                    }
                } );
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Step 3: Start the Location Monitor Service
     */
    private void startStep3() {
//        Intent intent = new Intent(mContext, LocationMonitoringService.class);
//        mContext.startService(intent);
    }

    /**
     * Return the availability of GooglePlayServices
     */
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable( mContext );
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError( status )) {
                googleApiAvailability.getErrorDialog( mContext, status, 2404 ).show();
            }
            return false;
        }
        return true;
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission( mContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION );
        int permissionState2 = ActivityCompat.checkSelfPermission( mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION );
        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Start permissions requests.
     */
    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale( mContext,
                        android.Manifest.permission.ACCESS_FINE_LOCATION );

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale( mContext,
                        Manifest.permission.ACCESS_COARSE_LOCATION );

        if (shouldProvideRationale || shouldProvideRationale2) {
            showSnackbar( "Permission needed",
                    "OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions( mContext,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE );
                        }
                    } );
        } else {
            ActivityCompat.requestPermissions( mContext,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE );
        }
    }

    private void showSnackbar(final String mainTextStringId, final String actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                mBinding.llStatus,
                mainTextStringId,
                Snackbar.LENGTH_INDEFINITE )
                .setAction( actionStringId, listener ).show();
    }


}