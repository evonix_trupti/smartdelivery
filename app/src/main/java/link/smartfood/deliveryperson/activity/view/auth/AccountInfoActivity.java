package link.smartfood.deliveryperson.activity.view.auth;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.viewModel.auth.AccountInfoVM;
import link.smartfood.deliveryperson.databinding.ActivityUpdateAccountBinding;
import link.smartfood.deliveryperson.util.EvonixBaseActivity;
import link.smartfood.deliveryperson.util.ImagePickerActivity;

public class AccountInfoActivity extends EvonixBaseActivity {
    public static String strImageData = "";
    AccountInfoVM mViewModel;
    public ActivityUpdateAccountBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingOfWidgets();
    }

    private void bindingOfWidgets() {
        mContext = AccountInfoActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_update_account);
        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new AccountInfoVM(((Activity) mContext), mBinding);
            }
        };
        mViewModel = ViewModelProviders.of(this, factory).get(AccountInfoVM.class);
        mBinding.setLifecycleOwner(this);
        mBinding.setAccountInfoViewModel(mViewModel);
        initUI();
    }

    public void initUI() {
        setToolBar();
        ImagePickerActivity.clearCache(this);
        mViewModel.loadAccountData();

    }

    private void setToolBar() {
        setSupportActionBar(mBinding.toolbar);
        if (getSupportActionBar() != null) {
            mBinding.toolbar.setTitle("Add Delivery Person Details");
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mViewModel.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        strImageData = "";
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}

