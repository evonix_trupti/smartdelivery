package link.smartfood.deliveryperson.util.font;

import android.app.Activity;
import android.graphics.Typeface;

/**
 * Class Name        :  <b>FontTypeFace.java<b/>
 * Purpose           :  Util class for Font Type Face.
 * Developed By      :  <b>@raghu</b>
 * Created Date      :  <b>05.06.2018</b>
 * <p>
 * Modified Reason   :  <b></b>
 * Modified By       :  <b>@raghu</b>
 * Modified Date     :  <b></b>
 * <p>
 */

public class FontTypeFace {

    private Activity activity;

    public FontTypeFace(Activity context) {
        this.activity = context;
    }

    public Typeface RobotoBold() {
        Typeface robotoBold = Typeface.createFromAsset(activity.getAssets(), "Roboto-Bold.ttf");
        return robotoBold;
    }

    public Typeface RobotoRegular() {
        Typeface robotoRegular = Typeface.createFromAsset(activity.getAssets(), "Roboto-Regular.ttf");
        return robotoRegular;
    }

    public Typeface RobotoThin() {
        Typeface robotoRegular = Typeface.createFromAsset(activity.getAssets(), "Roboto-Thin.ttf");
        return robotoRegular;
    }

    public Typeface Bigserif() {
        Typeface bigSerif = Typeface.createFromAsset(activity.getAssets(), "AC_BIGSERIF_ONE.OTF");
        return bigSerif;
    }
}
