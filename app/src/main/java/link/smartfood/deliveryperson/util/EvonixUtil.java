package link.smartfood.deliveryperson.util;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.network.Constant;

/**
 * Class Name        :  <b>EvonixUtil .java<b/>
 * Purpose           :  EvonixUtil  is  Class have all the util method for the app.
 * Developed By      :  <b>@</b>
 * Created Date      :  <b></b>
 * <p/>
 * Modified Reason   :  <b></b>
 * Modified By       :  <b>@raghu</b>
 * Modified Date     :  <b></b>
 * <p/>
 */
public class EvonixUtil extends EvonixBaseActivity {

    public static final String ARG_PARAM1 = "param1";
    public static final String ARG_PARAM2 = "param2";
    public final String APP_TYPE = "ANDROID";
    private final String TAG = EvonixUtil.class.getSimpleName();
    private final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();
    private static Context context;
    private static SharedPreferences pref;
    private int PRIVATE_MODE = 0;
    private ProgressDialog progressDialog;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    KProgressHUD progressHUD;

    public EvonixUtil(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(context.getResources().getString(R.string.evonix_util_pref_name), PRIVATE_MODE);
    }


    public static String getCurrentDateTimeStamp(String format) {
        DateFormat dateFormatter = new SimpleDateFormat(format);
        dateFormatter.setLenient(false);
        Date today = new Date();
        String s = dateFormatter.format(today);
        return s;
    }

    public static String getCurrentDateTimeStamp(String format, int time) {
        DateFormat dateFormatter = new SimpleDateFormat(format);
        dateFormatter.setLenient(false);
        Date today = new Date(System.currentTimeMillis() - time * 60 * 1000);
        String s = dateFormatter.format(today);
        return s;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    //Util Methods for Internet network checking
    public boolean isConnectingToInternet() {
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Network[] networks = connectivityManager.getAllNetworks();
                NetworkInfo networkInfo;
                for (Network mNetwork : networks) {
                    networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                    if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                        return true;
                    }
                }
            } else {
                if (connectivityManager != null) {
                    //noinspection deprecation
                    NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                    if (info != null) {
                        for (NetworkInfo anInfo : info) {
                            if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                                Log.d("Network", "NETWORKNAME: " + anInfo.getTypeName());
                                return true;
                            }
                        }
                    }
                }
            }
//            TEL("No Internet connection found...!");
            return false;
        }
        return false;
    }

    public boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isMyServiceRunning(Class<?> serviceClass, Activity mContext) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static String resizedBitmapEncodeTobase64(Bitmap image, int size) {
        System.gc();
        Bitmap immagex = null;
        if (image.getHeight() > 0 && image.getWidth() > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float newHeight = ((float) height / (float) width) * 200;
            float newWidth = ((float) width / (float) height) * 250;
            float scaleWidth = newWidth / width;
            float scaleHeight = newHeight / height;
            Matrix matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);
            try {
                immagex = Bitmap.createBitmap(image, 0, 0, width, height, matrix, false);
            } catch (Exception e) {
                e.printStackTrace();
                immagex = image;
            }
        } else {
            immagex = image;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (image.getWidth() > 2000) {
            immagex.compress(Bitmap.CompressFormat.JPEG, 30, baos);
        } else {
            immagex.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        }
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    public int rgb(String hex) {
        int color = (int) Long.parseLong(hex.replace("#", ""), 16);
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color >> 0) & 0xFF;
        return Color.rgb(r, g, b);
    }

    public void hideKeyboard() {
        Activity activity = (Activity) context;

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    //Util Methods for Internet network checking
    public boolean isConnectingToInternet(boolean showToast) {
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Network[] networks = connectivityManager.getAllNetworks();
                NetworkInfo networkInfo;
                for (Network mNetwork : networks) {
                    networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                    if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                        return true;
                    }
                }
            } else {
                if (connectivityManager != null) {
                    //noinspection deprecation
                    NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                    if (info != null) {
                        for (NetworkInfo anInfo : info) {
                            if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                                Log.d("Network", "NETWORKNAME: " + anInfo.getTypeName());
                                return true;
                            }
                        }
                    }
                }
            }
            if (showToast) {
//                LTU.TEL(context, context.getString(R.string.internet_is_not_connected));
            }
        }
        return false;
    }

    public boolean isInternet(boolean showToast) {
        if (context != null) {
            if (isConnectingToInternet(false)) {

            }
        }
        return false;
    }

    // Method for info log
    public void LI(String msg) {
        // TODO to display messages in project
        // msg1 is caller(class or function ) and msg2 is message
        Log.i("Evonix", msg);
    }

    public void ShowProgressDialog(String title, String msg, boolean cancelable) {
        // TODO to display messages in project
        progressDialog = ProgressDialog.show(context, title, msg, true);
        progressDialog.setCancelable(cancelable);
    }

    public void DismissProgressDialog() {
        // TODO to display messages in project
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Method for error log
    public void LE(String msg) {
        // TODO to display error during exception in project
        // msg1 is caller(class or function ) and msg2 is message
        if (Constant.isIsDev()) {
            Log.e("Evonix", msg);
        }
    }

    // Method for toast
    public void TIS(String msg) {
        // TODO to display Toast messages and log
        // msg1 is caller(class or function ) and msg2 is message
        Log.i("Evonix", "Toast : " + msg);
        if (context != null) {
            Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
//        Toast.makeText(context, msg1, Toast.LENGTH_SHORT).show();
        }
    }

    public void TIL(String msg) {
        // TODO to display error during exception in project
        // msg1 is caller(class or function ) and msg2 is message
        Log.i("Evonix", "Toast : " + msg);
        if (context != null) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }

    public void TIL(int msgId) {
        // TODO to display error during exception in project
        // msg1 is caller(class or function ) and msg2 is message
        Log.i("Evonix", "Toast : " + context.getString(msgId));
        if (context != null) {
            Toast.makeText(context, context.getString(msgId), Toast.LENGTH_LONG).show();
        }
    }

    public void TES(String msg) {
        // TODO to display error during exception in project
        // msg1 is caller(class or function ) and msg2 is message
        Log.e("Evonix", "Toast : " + msg);
        if (context != null) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        }
    }

    public void TES(int msgId) {
        // TODO to display error during exception in project
        // msg1 is caller(class or function ) and msg2 is message
        if (isConnectingToInternet(false)) {
            if (context != null) {
                Toast.makeText(context, context.getString(msgId), Toast.LENGTH_SHORT).show();
                Log.e("Evonix", "Toast : " + context.getString(msgId));
            }
        }
    }

    public void TEL(int msgId) {
        // TODO to display error during exception in project
        // msg1 is caller(class or function ) and msg2 is message
        if (isConnectingToInternet(false)) {
            if (context != null) {
                Toast.makeText(context, context.getString(msgId), Toast.LENGTH_LONG).show();
                Log.e("Evonix", "Toast : " + context.getString(msgId));
            }
        }
    }

    public void TEL(String msg) {
        Log.e("Evonix", "Toast : " + msg);
        if (context != null) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }

    public void errorToast(boolean isShowToast) {
        // TODO to display error during exception in project
        // msg1 is caller(class or function ) and msg2 is message
        if (context != null && isShowToast) {
//            Toast.makeText(context, context.getString(R.string.error_to_process), Toast.LENGTH_LONG).show();
        }
    }

    public void noDataFoundToast() {
        // TODO to display error during exception in project
        // msg1 is caller(class or function ) and msg2 is message
        if (context != null) {
            Toast.makeText(context, context.getString(R.string.data_not_found), Toast.LENGTH_LONG).show();
        }
    }

    public void TCS(String msg) {
        Log.i("Evonix", "Toast : " + msg);
        if (context != null) {
            Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public void TCS(int msgId) {
        if (context != null) {
            Log.i("Evonix", "Toast : " + context.getString(msgId));
            Toast toast = Toast.makeText(context, context.getString(msgId), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public void TCL(String msg) {
        // TODO to display error during exception in project
        Log.i("Evonix", "Toast : " + msg);
        if (context != null) {
            Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    //Sp methods to store string to sp
    public Boolean setSPString(String tag, String value) {
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(tag, value);
            editor.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //Sp methods to store string to sp
    public static Boolean setSPString(int tagID, String value) {
        try {
            if (context != null) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(context.getString(tagID), value);
                editor.commit();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //Sp methods to get string from sp
    public String getSPString(String tag, String defltValue) {
        try {
            return pref.getString(tag, defltValue);
        } catch (Exception e) {
            e.printStackTrace();
            return defltValue;
        }
    }

    //Sp methods to get string from sp
    public String getSPString(int tagID, String defltValue) {
        try {
            if (context != null) {
                return pref.getString(context.getString(tagID), defltValue);
            }
            return defltValue;
        } catch (Exception e) {
            e.printStackTrace();
            return defltValue;
        }
    }

    //Sp methods to store boolean to sp
    public Boolean getSPBoolean(String tag, boolean defltValue) {
        try {

            return pref.getBoolean(tag, defltValue);
        } catch (Exception e) {
            e.printStackTrace();
            return defltValue;
        }
    }

    public Boolean getSPBoolean(int tagID, boolean defltValue) {
        try {
            if (context != null) {
                return pref.getBoolean(context.getString(tagID), defltValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return defltValue;
        }
        return false;
    }

    public Boolean setSPBoolean(String tag, boolean value) {
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(tag, value);
            editor.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Boolean setSPBoolean(int tagID, boolean value) {
        try {
            if (context != null) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(context.getString(tagID), value);
                editor.commit();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //method for logout session
    public void setLogout() {
        if (context != null) {
            try {
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(context.getResources().getString(R.string.evonix_util_is_login), false);
                mEvonixUtil.setSPString(R.string.profile_delivery_boy_profile_image, "");
               /* mEvonixUtil.setSPString(R.string.evonix_util_login_id, "");
                mEvonixUtil.setSPString(R.string.evonix_util_token, "");
                mEvonixUtil.setSPString(R.string.evonix_mobile, "");
                mEvonixUtil.setSPString(R.string.evonix_util_email, "");
                mEvonixUtil.setSPString(R.string.evonix_restaurant_name, "");
                mEvonixUtil.setSPString(R.string.evonix_util_full_name, "");*/
                editor.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //method for login session
    public boolean setIsLogin(String Name, String contactNo, String emailId, String gender, String DoB,
                              String prn_no) {
        try {
            SharedPreferences.Editor editor = pref.edit();
//            editor.putBoolean(context.getResources().getString(R.string.evonix_util_is_login), true);
//            editor.putString(context.getResources().getString(R.string.evonix_util_name), Name);
//            editor.putString(context.getResources().getString(R.string.evonix_util_contact), contactNo);
//            editor.putString(context.getResources().getString(R.string.evonix_util_email_id), emailId);
//            editor.putString(context.getResources().getString(R.string.evonix_util_gender), gender);
//            editor.putString(context.getResources().getString(R.string.evonix_util_dob), DoB);
//            editor.putString(context.getResources().getString(R.string.evonix_util_prn_no), prn_no);

            editor.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Bitmap decodeFromBase64ToBitmap(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;

    }

    public void showLoader(Activity mContext){
        progressHUD = KProgressHUD.create(mContext)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(mContext.getResources().getColor(R.color.transparentColor))
                .setLabel(mContext.getResources().getString(R.string.please_wait))
                .setCancellable(false)
                .setDimAmount(0.5f);
        progressHUD.show();
    }

    public void hideLoader(){
        if (progressHUD.isShowing()){
            progressHUD.dismiss();
        }
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


}
