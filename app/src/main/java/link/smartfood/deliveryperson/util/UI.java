package link.smartfood.deliveryperson.util;
/**
 * Class Name        :  <b>UI.java<b/>
 * Purpose           :  UI is image loader class.
 * Developed By      :  <b>@raghu</b>
 * Created Date      :  <b>08.10.2016</b>
 * <p>
 * Modified Reason   :  <b></b>
 * Modified By       :  <b></b>
 * Modified Date     :  <b></b>
 * <p>
 */

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import link.smartfood.deliveryperson.R;


public class UI {

    public static void ResizeImageLoader(Context context, String url, ImageView imgView, int width, int height) {
        int placeholder = R.drawable.orangeloader;
        try {
            if (width == 0 && height == 0) {
                Picasso.get().load(url.trim()).placeholder(placeholder).into(imgView);
            } else {
                Picasso.get().load(url.trim()).placeholder(placeholder).resize(width, height).into(imgView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ResizeImageLoaderRotate(Context context, String url, int placeholder,
                                               ImageView imgView, int width, int height) {
//        int placeholder = R.drawable.magazine_place_holder;
        try {
            if (width == 0 && height == 0) {
                Picasso.get()
                        .load(url.trim())
                        .placeholder(placeholder)
                        .error(placeholder)
                        .fit()
                        .rotate(90f)
                        .into(imgView);
            } else {
                Picasso.get()
                        .load(url.trim())
                        .resize(width, height)
                        .placeholder(placeholder)
                        .error(placeholder)
                        .fit()
                        .into(imgView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void ResizeImageLoader(Context context, String url, int placeholder, ImageView imgView, ProgressBar progressBar) {
        try {
            Picasso.get().load(url.trim())
                    .resize(550, 250)
                    .into(imgView);
            progressBar.setVisibility(View.GONE);
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
        }
    }

    public static void ResizeImageLoader(Context context, String imageUrl, int place_holder, ImageView imgMain, final ProgressBar progressBar2, int i, int i1) {
//        Glide.with(context)
//                .load(imageUrl)
//                .listener(new RequestListener<String, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        progressBar2.setVisibility(View.GONE);
//                        return false;
//                    }
//                })
//                .error(R.drawable.place_holder)
//                .into(imgMain);
    }
}
