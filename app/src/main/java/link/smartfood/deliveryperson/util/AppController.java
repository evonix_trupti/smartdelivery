package link.smartfood.deliveryperson.util;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.util.Log;

//import com.crashlytics.android.Crashlytics;
//
//import io.fabric.sdk.android.Fabric;

public class AppController extends Application {
    public static final String CHANNEL_ORDER = "order_received";
    public static final String CHANNEL_CONTRACT = "contract";
    public static final String CHANNEL_DEAL = "deal";
    public static final String CHANNEL_FOODITEM = "fooditem";
    public static final String CHANNEL_PAYMENT = "payment";
    public static final String CHANNEL_UNBLOCK = "unblock";
    public static final String CHANNEL_ACCEPTED_PERM = "permission";
    public static final String CHANNEL_OUTOFSTOCK = "outofstock";
    public static final String CHANNEL_HISTORY = "history";
    public static final String CHANNEL_ORDER_DELIVERY = "order";
    private static AppController mInstance;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        createNotificationChanel();
        configDeepLink();
    }

    private void configDeepLink() {
//        IntentFilter intentFilter = new IntentFilter(DeepLinkHandler.ACTION);
//        LocalBroadcastManager.getInstance(this).registerReceiver(new DeepLinkReceiver(), intentFilter);
    }

    private void createNotificationChanel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ORDER_DELIVERY,
                    "SmartFood Order",
                    NotificationManager.IMPORTANCE_HIGH
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.d("AppController: ", "Application on Low memory.");
    }

}