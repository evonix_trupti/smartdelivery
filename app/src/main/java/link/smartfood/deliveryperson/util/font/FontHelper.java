package link.smartfood.deliveryperson.util.font;
/**
 * Class Name        :  <b>FontHelper.java<b/>
 * Purpose           :  FontHelper class is to apply the font to all view in the layout.
 * Developed By      :  <b>@raghu</b>
 * Created Date      :  <b>08.10.2016</b>
 * <p>
 * Modified Reason   :  <b></b>
 * Modified By       :  <b>@raghu</b>
 * Modified Date     :  <b></b>
 * <p>
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Hashtable;


public class FontHelper {
    private static final String TAG = FontHelper.class.getSimpleName();
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    public static void applyFont(final Context context, final View root, final String fontPath) {
        try {
            if (root instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) root;
                int childCount = viewGroup.getChildCount();
                for (int i = 0; i < childCount; i++)
                    applyFont(context, viewGroup.getChildAt(i), fontPath);
            } else if (root instanceof TextView)
                //((TextView) root).setTypeface(Typeface.createFromAsset(context.getAssets(), fontPath));
                //extra with cache
                ((TextView) root).setTypeface(get(context, fontPath));
        } catch (Exception e) {
            Log.e(TAG, String.format("Error occured when trying to apply %s font for %s view", fontPath, root));
            e.printStackTrace();
        }

    }

    public static Typeface get(Context c, String assetPath) {
        synchronized (cache) {
            if (!cache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(c.getAssets(), assetPath);
                    cache.put(assetPath, t);
                    Log.e(TAG, "Typeface Success");
                } catch (Exception e) {
                    Log.e(TAG, "Could not get typeface '" + assetPath
                            + "' because " + e.getMessage());
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }

}
