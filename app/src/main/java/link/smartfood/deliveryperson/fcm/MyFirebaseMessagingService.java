package link.smartfood.deliveryperson.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.view.dashboard.DashboardActivity;
import link.smartfood.deliveryperson.util.EvonixUtil;

import static link.smartfood.deliveryperson.util.AppController.CHANNEL_ORDER_DELIVERY;

//import android.support.v4.app.NotificationCompat;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
        private final String TAG = "MyFirebaseMsgService";
    Context mContext;
//    public EvonixUtil mEvonixUtil = new EvonixUtil(MyFirebaseMessagingService.this);
        String strRefreshToken = "";
        Random rand = new Random();

        @Override
        public void onNewToken(String s) {
            super.onNewToken(s);
            mContext = MyFirebaseMessagingService.this;
            Log.e("NEW_TOKEN", s);
            strRefreshToken = s;
            EvonixUtil.setSPString(R.string.evonix_util_fcm, s);
            Log.e("FCMtoken: ", strRefreshToken);
        }

        @Override
        public void onMessageReceived(RemoteMessage remoteMessage) {
            super.onMessageReceived(remoteMessage);
            Log.d(TAG, "From: " + remoteMessage.getFrom());
            Log.d(TAG, "DATAAAnoti: " + remoteMessage.getData());
            try {
                mContext = MyFirebaseMessagingService.this;
                /*if (mEvonixUtil == null) {
                    mEvonixUtil = new EvonixUtil(MyFirebaseMessagingService.this);
                }
*/
                sendNotification(remoteMessage.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    // TODO test notifications with flow
    private void sendNotification(Map<String, String> data) throws Exception {

        String strTitle = this.getResources().getString(R.string.app_name), strDetail = "Check the order list";
        int notificationCount = 0;
        PendingIntent pendingIntent;
        NotificationCompat.Builder mBuilder = null;
        Intent intent = null;
        FCMConstant.requestCode = rand.nextInt(50);

        FCMConstant.requestCode += 1;
        strTitle = data.get("title");
        strDetail = data.get("message");
        String module = data.get("module");
        Log.d("strTitle", module);

        intent = new Intent(getApplicationContext(), DashboardActivity.class);
        mBuilder = new NotificationCompat.Builder(mContext, CHANNEL_ORDER_DELIVERY);

        switch (module) {
           /*  case "4":
               intent = new Intent(getApplicationContext(), MyOrderActivity.class);
                mBuilder = new NotificationCompat.Builder(mContext, CHANNEL_ORDER_DECLINED);
                mEvonixUtil.setSPString("CHANNEL", CHANNEL_ORDER_DECLINED);
                break;
            case "5":
                intent = new Intent(getApplicationContext(), MyOrderActivity.class);
                mBuilder = new NotificationCompat.Builder(mContext, CHANNEL_ORDER);
                mEvonixUtil.setSPString("CHANNEL", CHANNEL_ORDER);
                break;
            case "9":
                intent = new Intent(getApplicationContext(), ClientDashboardActivity.class);
                mBuilder = new NotificationCompat.Builder(mContext, CHANNEL_UNBLOCK);
                mEvonixUtil.setSPString("CHANNEL", CHANNEL_UNBLOCK);
                break;*/

        }

        pendingIntent = PendingIntent.getActivity(this, FCMConstant.requestCode, intent, PendingIntent.FLAG_ONE_SHOT);

        mBuilder.setContentTitle(strTitle)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentText(strDetail)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setShowWhen(true)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setSmallIcon(R.drawable.logo_new);
            mBuilder.setColor(getResources().getColor(R.color.green_900));
        } else {
            mBuilder.setSmallIcon(R.drawable.logo_new);
        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ORDER_DELIVERY,
                    "Notification Management",
                    NotificationManager.IMPORTANCE_HIGH
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
            mBuilder.setChannelId(CHANNEL_ORDER_DELIVERY);
        }

        notificationManager.notify(FCMConstant.requestCode, mBuilder.build());
    }
}
