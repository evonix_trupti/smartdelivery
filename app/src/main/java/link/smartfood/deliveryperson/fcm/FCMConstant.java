package link.smartfood.deliveryperson.fcm;

import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Class Name        :  <b>MyFirebaseInstanceIDService.java<b/>
 * Purpose           :  MyFirebaseInstanceIDService .
 * Developed By      :  <b>@raghu</b>
 * Created Date      :  <b>09.11.2017</b>
 * <p>
 * Modified Reason   :  <b></b>
 * Modified By       :  <b></b>
 * Modified Date     :  <b></b>
 * <p>
 */

public class FCMConstant {
    public static final int NEW_NOTIFICATION = 11111;
    public static final int PENDING_NOTIFICATION = 22222;
    public static final int CALL_NOTIFICATION = 0;
    public static int requestCode = 0;
    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();

}
