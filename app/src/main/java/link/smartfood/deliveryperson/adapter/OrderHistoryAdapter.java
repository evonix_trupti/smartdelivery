package link.smartfood.deliveryperson.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import java.util.ArrayList;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.order.OrderList;
import link.smartfood.deliveryperson.activity.view.order.OrderDetailsActivity;
import link.smartfood.deliveryperson.databinding.ItemHistoryOrderBinding;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<OrderList> orderList;
    ItemHistoryOrderBinding mBinding;

    public OrderHistoryAdapter(Context context, ArrayList<OrderList> orderList) {
        this.mContext = context;
        this.orderList = orderList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_history_order, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderList dataModel = orderList.get(position);
        holder.bind(dataModel);
        mBinding.cardMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, OrderDetailsActivity.class));
                ((Activity)mContext).overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemHistoryOrderBinding mBinding;

        public ViewHolder(ItemHistoryOrderBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(Object obj) {
            mBinding.setVariable(BR.orderWrapper, obj);
            mBinding.executePendingBindings();
        }
    }
}