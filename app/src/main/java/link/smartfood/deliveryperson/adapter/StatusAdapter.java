package link.smartfood.deliveryperson.adapter;

import android.content.Context;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.status.OrderStatusWrapper;
import link.smartfood.deliveryperson.databinding.ItemStatusBinding;
import link.smartfood.deliveryperson.util.EvonixUtil;

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.ViewHolder> {
    EvonixUtil mEvonixUtil;
    private Context mContext;
    private List<OrderStatusWrapper.Status> statusList;
    ItemStatusBinding mBinding;


    public StatusAdapter(Context mContext, List<OrderStatusWrapper.Status> statusList) {
        this.mContext = mContext;
        this.statusList = statusList;
        mEvonixUtil = new EvonixUtil(mContext);
        Log.e("orderLisstt", String.valueOf(statusList));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_status, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderStatusWrapper.Status dataModel = statusList.get(position);
        holder.bind(dataModel);
       /* mBinding.cardMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, OrderDetailsActivity.class));
                ((Activity)mContext).overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return statusList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemStatusBinding mBinding;

        public ViewHolder(ItemStatusBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(Object obj) {
            mBinding.setVariable(BR.statusWrapper, obj);
            mBinding.executePendingBindings();
        }
    }
}

