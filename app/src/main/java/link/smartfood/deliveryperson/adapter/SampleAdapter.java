package link.smartfood.deliveryperson.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.order.OrderList;
import link.smartfood.deliveryperson.activity.view.order.OrderDetailsActivity;
import link.smartfood.deliveryperson.databinding.DialogStatusAlertBinding;
import link.smartfood.deliveryperson.databinding.ItemHomeBinding;
import link.smartfood.deliveryperson.databinding.ItemSampleBinding;
import link.smartfood.deliveryperson.util.EvonixUtil;

public class SampleAdapter extends RecyclerView.Adapter<SampleAdapter.ViewHolder>  implements ActivityCompat.OnRequestPermissionsResultCallback {
    EvonixUtil mEvonixUtil;
    private Context mContext;
    public ArrayList<OrderList> orderList;
    public ArrayList<String> listCheckId = new ArrayList<String>();
    public static ArrayList<String> listCheckStatus = new ArrayList<String>();
//    ItemHomeBinding mBinding;
    ItemSampleBinding mBinding;


    public SampleAdapter(Context mContext, ArrayList<OrderList> orderList) {
        this.mContext = mContext;
        this.orderList = orderList;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_sample, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final OrderList dataModel = orderList.get(position);
//        holder.mBinding.setOrderWrapper(dataModel);    //Comment: alternate solution instead of bind ()
        holder.bind(dataModel);
//        holder.mBinding.viewStatusColor.setBackgroundColor(Color.parseColor(dataModel.getStatus_color()));
//        try {
//            if (holder.myCountDownTimer != null) {
//                holder.myCountDownTimer.cancel();
//            }
//            if (dataModel.getTimer().equalsIgnoreCase("00:00")) {
//                holder.mBinding.txtTimer.setText(dataModel.getTimer());
//            } else {
//                int timer = Integer.parseInt(dataModel.getTimer());
//                holder.myCountDownTimer = new CountDownTimer(timer * 1000, 1000) { // adjust the milli seconds here
//                    public void onTick(long millisUntilFinished) {
//                        holder.mBinding.txtTimer.setText("" + String.format("%02d:%02d",
//                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
//                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
//                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
//                    }
//                    public void onFinish() {
//                        holder.mBinding.txtTimer.setText("00:00");
//                        holder.myCountDownTimer.cancel();
//                        notifyDataSetChanged();
//                    }
//                }.start();
//            }
//
            holder.mBinding.imgMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(mContext, MapsActivity.class);
//                    intent.putExtra("data", dataModel);
//                    mContext.startActivity(intent);
//                    ((Activity) mContext).overridePendingTransition(R.anim.enter, R.anim.exit);
                    Intent intent = new Intent( Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?q=loc:18.6011,73.7641"));
                    ((Activity) mContext).startActivity(intent);
                }
            });
        holder.mBinding.imgMap1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Intent intent = new Intent(mContext, MapsActivity.class);
//                    intent.putExtra("data", dataModel);
//                    mContext.startActivity(intent);
//                    ((Activity) mContext).overridePendingTransition(R.anim.enter, R.anim.exit);
                Intent intent = new Intent( Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?q=loc:18.6011,73.7641"));
                ((Activity) mContext).startActivity(intent);
            }
        });
//
//            holder.mBinding.txtMobileNo.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (isPermissionGranted()) {
//                        try {
//                            final Dialog dialog = new Dialog(mContext);
//                            final DialogStatusAlertBinding dialogCaptureImageBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_status_alert, null, false);
//                            dialog.setContentView(dialogCaptureImageBinding.getRoot());
//                            dialogCaptureImageBinding.llChangeDeliveryBoyStatus.setVisibility(View.GONE);
//                            dialogCaptureImageBinding.llChangeOrderStatus.setVisibility(View.VISIBLE);
//                            dialogCaptureImageBinding.txtNo.setVisibility(View.VISIBLE);
//                            dialogCaptureImageBinding.txtHeader.setText("Call Alert");
//                            dialogCaptureImageBinding.txtMessage.setText("Are you sure you want to call this customer?");
//
//                            dialogCaptureImageBinding.txtYes.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
//                                    callIntent.setData(Uri.parse("tel:" + dataModel.getPhone()));
//                                    mContext.startActivity(callIntent);
//                                }
//                            });
//
//                            dialogCaptureImageBinding.txtNo.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    try {
//                                        dialog.dismiss();
//                                    } catch (Exception ex) {
//                                        ex.printStackTrace();
//                                    }
//                                }
//                            });
//                            dialog.setCancelable(true);
//                            dialog.show();
//
//                        } catch (Exception e) {
//                            mEvonixUtil.TES("Unable to connect for a moment");
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            });
//
//            holder.mBinding.cardMain.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(mContext, OrderDetailsActivity.class);
//                    intent.putExtra("listData", orderList.get(position));
//                    intent.putExtra("timer", holder.mBinding.txtTimer.getText().toString().trim());
//                    mContext.startActivity(intent);
//                    ((Activity) mContext).overridePendingTransition(R.anim.enter, R.anim.exit);
//                }
//            });
//            holder.mBinding.cbStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                try {
//                    if (listCheckId.contains(dataModel.getId())) {
//                        listCheckId.remove(dataModel.getId());
//                        listCheckStatus.remove(dataModel.getStatus_id());
//                    } else {
//                        listCheckId.add(dataModel.getId());
//                        listCheckStatus.add(dataModel.getStatus_id());
//                    }
//                    setColor();
////                    HomeVM.h.sendEmptyMessage(1);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            private void setColor() {
//                    if (listCheckId.contains(dataModel.getId())) {
//                        holder.mBinding.llMainRoot.setBackgroundColor(ContextCompat.getColor(mContext, R.color.indigo_50));
//                    } else {
//                        holder.mBinding.llMainRoot.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_1000));
//                    }
//
//            }
//        });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        holder.mBinding.llMainRoot.setTag(position);
//        holder.mBinding.llCheck.setTag(position);
//        holder.mBinding.cbStatus.setTag(position);
//        holder.mBinding.cardMain.setTag(position);
//        holder.mBinding.txtTimer.setTag(position);

    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(mContext, "Permission granted", Toast.LENGTH_SHORT).show();
                    //call_action();
                } else {
                    Toast.makeText(mContext, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
    public String getOrderId() {
        String strOrderId = String.valueOf(listCheckId).replace("[", "").replace("]", "");
        Log.e("IDDDDDDDDDDD", strOrderId);
        return strOrderId;
    }

    public void setList(ArrayList<OrderList> orderList) {
        this.orderList = orderList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList == null ? 0 : orderList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
//        ItemHomeBinding mBinding;
        ItemSampleBinding mBinding;
        public CountDownTimer myCountDownTimer;

        public ViewHolder(ItemSampleBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(Object obj) {
            mBinding.setVariable(BR.orderWrapper, obj);
            mBinding.executePendingBindings();
        }
    }
}

