package link.smartfood.deliveryperson.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.notfication.NotificationListWrapper;
import link.smartfood.deliveryperson.databinding.ItemNotificationBinding;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private Context mContext;
    public List<NotificationListWrapper.NotificationList> notificationList;
    ItemNotificationBinding mBinding;

    public NotificationAdapter(Context context, List<NotificationListWrapper.NotificationList> notificationList) {
        this.mContext = context;
        this.notificationList = notificationList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_notification, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationListWrapper.NotificationList dataModel = notificationList.get(position);
        holder.bind(dataModel);
//        holder.mBinding.viewStatusColor.setBackgroundColor(Color.parseColor(dataModel.getColor()));
    }

    public void setList(List<NotificationListWrapper.NotificationList> notificationList) {
        this.notificationList = notificationList;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemNotificationBinding mBinding;

        public ViewHolder(ItemNotificationBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(Object obj) {
            mBinding.setVariable(BR.notificationWrapper, obj);
            mBinding.executePendingBindings();
        }
    }
}