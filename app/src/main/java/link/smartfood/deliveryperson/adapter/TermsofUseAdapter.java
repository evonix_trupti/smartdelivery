package link.smartfood.deliveryperson.adapter;

import android.content.Context;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.login.TermsofUseItem;

public class TermsofUseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<TermsofUseItem> arrayList = new ArrayList<>();
    private Context context;
    private JSONArray jsonArray;

    public TermsofUseAdapter(Context context, ArrayList<TermsofUseItem> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_privacy_policy, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder myHolder = (ViewHolder) holder;
        setImagePost(myHolder, position);
    }


    private void setImagePost(final ViewHolder myHolder, final int position) {
        try {
//            Html.fromHtml(arrayList.get(position).getDescription());
            myHolder.txtDescItem.setText(Html.fromHtml(arrayList.get(position).getDescription()));
            myHolder.txtTitle.setText(arrayList.get(position).getHeading());
            myHolder.txtTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (myHolder.txtDesc.getVisibility() == View.GONE) {
                        myHolder.txtDesc.setVisibility(View.VISIBLE);

                        myHolder.txtTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.orange_up, 0);
                    } else if (myHolder.txtDesc.getVisibility() == View.VISIBLE) {
                        myHolder.txtTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.orange_down, 0);
                        myHolder.txtDesc.setVisibility(View.GONE);
                    }
                }
            });
            myHolder.txtDesc.setTag(position);
            myHolder.txtTitle.setTag(position);
            myHolder.main_root.setTag(position);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
//        if (jsonArray != null && jsonArray.length() > 0) {
        return (arrayList.size());
//        }
//        return 5;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout main_root;
        RelativeLayout txtDesc;
        ImageView imgMain;
        TextView txtTitle, txtDescItem;

        public ViewHolder(View itemView) {
            super(itemView);
            this.main_root = (LinearLayout) itemView.findViewById(R.id.main_root);
            this.txtDesc = (RelativeLayout) itemView.findViewById(R.id.txtDesc);
            this.imgMain = (ImageView) itemView.findViewById(R.id.imgMain);
            this.txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            this.txtDescItem = (TextView) itemView.findViewById(R.id.txtDescItem);

        }
    }
}