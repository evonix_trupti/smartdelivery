package link.smartfood.deliveryperson.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.W3WActivity;
import link.smartfood.deliveryperson.activity.model.order.OrderList;
import link.smartfood.deliveryperson.activity.view.order.OrderDetailsActivity;
import link.smartfood.deliveryperson.databinding.DialogStatusAlertBinding;
import link.smartfood.deliveryperson.databinding.ItemHomeBinding;
import link.smartfood.deliveryperson.util.EvonixUtil;
import com.what3words.androidwrapper.What3WordsV3;
import com.what3words.javawrapper.request.BoundingBox;
import com.what3words.javawrapper.request.Coordinates;
import com.what3words.javawrapper.response.APIResponse;
import com.what3words.javawrapper.response.Autosuggest;
import com.what3words.javawrapper.response.ConvertTo3WA;
import com.what3words.javawrapper.response.ConvertToCoordinates;
import com.what3words.javawrapper.response.GridSection;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder>  implements ActivityCompat.OnRequestPermissionsResultCallback {
    EvonixUtil mEvonixUtil;
    private Context mContext;
    public ArrayList<OrderList> orderList;
    public ArrayList<String> listCheckId = new ArrayList<String>();
    public static ArrayList<String> listCheckStatus = new ArrayList<String>();
    ItemHomeBinding mBinding;


    public HomeAdapter(Context mContext, ArrayList<OrderList> orderList) {
        this.mContext = mContext;
        this.orderList = orderList;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_home, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final OrderList dataModel = orderList.get(position);
        holder.mBinding.setOrderWrapper(dataModel);    //Comment: alternate solution instead of bind ()
        holder.bind(dataModel);
        holder.mBinding.viewStatusColor.setBackgroundColor(Color.parseColor(dataModel.getStatus_color()));
        try {
            if (holder.myCountDownTimer != null) {
                holder.myCountDownTimer.cancel();
            }
            if (dataModel.getTimer().equalsIgnoreCase("00:00")) {
                holder.mBinding.txtTimer.setText(dataModel.getTimer());
            } else {
                int timer = Integer.parseInt(dataModel.getTimer());
                holder.myCountDownTimer = new CountDownTimer(timer * 1000, 1000) { // adjust the milli seconds here
                    public void onTick(long millisUntilFinished) {
                        holder.mBinding.txtTimer.setText("" + String.format("%02d:%02d",
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                    }
                    public void onFinish() {
                        holder.mBinding.txtTimer.setText("00:00");
                        holder.myCountDownTimer.cancel();
                        notifyDataSetChanged();
                    }
                }.start();
            }



            holder.mBinding.llMap2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    //code for redirecting to w3w app
//                    Intent intent = ((Activity) mContext).getPackageManager().getLaunchIntentForPackage("com.what3words.android");
//                    intent.setAction(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse("w3w://show?threewords=processes.gaps.theory"));
//                    ((Activity) mContext).startActivity(intent);
//                    Intent intent = new Intent(mContext, W3WActivity.class);
////                    intent.putExtra("data", "w3w://show?threewords=processes.gaps.theory");
//                    mContext.startActivity(intent);
//                    What3WordsV3 api = new What3WordsV3("4RGXD5E5", ((Activity) mContext));
//                    ConvertTo3WA words = api.convertTo3wa(new Coordinates(51.508344, -0.12549900))
//                            .language("en")
//                            .execute();
//                    System.out.println("Words: " + words);
//                    ConvertToCoordinates coordinates = api.convertToCoordinates("filled.count.soap")
//                            .execute();
//                    System.out.println("Coordinates: " + coordinates);
                    newprint();

                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse(dataModel.getCustomer_address_url()));
                    ((Activity) mContext).startActivity(intent);
                }
            });
            holder.mBinding.llMap1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(mContext, MapsActivity.class);
//                    intent.putExtra("data", dataModel);
//                    mContext.startActivity(intent);
//                    ((Activity) mContext).overridePendingTransition(R.anim.enter, R.anim.exit);
//                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                            Uri.parse("http://maps.google.com/maps?q=loc:18.6011,73.7641"));
//                    ((Activity) mContext).startActivity(intent);

                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse(dataModel.getStore_address_url()));
                    ((Activity) mContext).startActivity(intent);
                }
            });

            holder.mBinding.txtMobileNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isPermissionGranted()) {
                        try {
                            final Dialog dialog = new Dialog(mContext);
                            final DialogStatusAlertBinding dialogCaptureImageBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_status_alert, null, false);
                            dialog.setContentView(dialogCaptureImageBinding.getRoot());
                            dialogCaptureImageBinding.llChangeDeliveryBoyStatus.setVisibility(View.GONE);
                            dialogCaptureImageBinding.llChangeOrderStatus.setVisibility(View.VISIBLE);
                            dialogCaptureImageBinding.txtNo.setVisibility(View.VISIBLE);
                            dialogCaptureImageBinding.txtHeader.setText("Call Alert");
                            dialogCaptureImageBinding.txtMessage.setText("Are you sure you want to call this customer?");

                            dialogCaptureImageBinding.txtYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                    callIntent.setData(Uri.parse("tel:" + dataModel.getPhone()));
                                    mContext.startActivity(callIntent);
                                    dialog.dismiss();
                                }
                            });

                            dialogCaptureImageBinding.txtNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    try {
                                        dialog.dismiss();
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            });
                            dialog.setCancelable(true);
                            dialog.show();

                        } catch (Exception e) {
                            mEvonixUtil.TES("Unable to connect for a moment");
                            e.printStackTrace();
                        }
                    }
                }
            });
            holder.mBinding.txtRestMobileNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isPermissionGranted()) {
                        try {
                            final Dialog dialog = new Dialog(mContext);
                            final DialogStatusAlertBinding dialogCaptureImageBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_status_alert, null, false);
                            dialog.setContentView(dialogCaptureImageBinding.getRoot());
                            dialogCaptureImageBinding.llChangeDeliveryBoyStatus.setVisibility(View.GONE);
                            dialogCaptureImageBinding.llChangeOrderStatus.setVisibility(View.VISIBLE);
                            dialogCaptureImageBinding.txtNo.setVisibility(View.VISIBLE);
                            dialogCaptureImageBinding.txtHeader.setText("Call Alert");
                            dialogCaptureImageBinding.txtMessage.setText("Are you sure you want to call this customer?");

                            dialogCaptureImageBinding.txtYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                    callIntent.setData(Uri.parse("tel:" + dataModel.getRestaurant_phone()));
                                    mContext.startActivity(callIntent);
                                    dialog.dismiss();
                                }
                            });

                            dialogCaptureImageBinding.txtNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    try {
                                        dialog.dismiss();
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            });
                            dialog.setCancelable(true);
                            dialog.show();

                        } catch (Exception e) {
                            mEvonixUtil.TES("Unable to connect for a moment");
                            e.printStackTrace();
                        }
                    }
                }
            });

            holder.mBinding.cardMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                    intent.putExtra("listData", orderList.get(position));
                    intent.putExtra("timer", holder.mBinding.txtTimer.getText().toString().trim());
                    mContext.startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.mBinding.llMainRoot.setTag(position);
        holder.mBinding.cardMain.setTag(position);
        holder.mBinding.txtTimer.setTag(position);
    }

    private void newprint() {
        new Thread(new Runnable() {
            public void run() {
                // For all requests a what3words API key is needed
                What3WordsV3 api = new What3WordsV3("WN58T4WI", mContext);

                Autosuggest autosuggest = api.autosuggest("freshen.overlook.clo")
                        .clipToCountry("FR")
                        .focus(new Coordinates(48.856618, 2.3522411))
                        .nResults(1)
                        .execute();

                if (autosuggest.isSuccessful()) {
                    String words = autosuggest.getSuggestions().get(0).getWords();
                    System.out.printf("Top 3 word address match: %s%n", words);

                    ConvertToCoordinates convertToCoordinates = api.convertToCoordinates(words).execute();
                    if (convertToCoordinates.isSuccessful()) {
                        System.out.printf("WGS84 Coordinates: %f, %f%n",
                                convertToCoordinates.getCoordinates().getLat(),
                                convertToCoordinates.getCoordinates().getLng());
                        System.out.printf("Nearest Place: %s%n", convertToCoordinates.getNearestPlace());
                    } else {
                        APIResponse.What3WordsError error = autosuggest.getError();
                        if (error == APIResponse.What3WordsError.INTERNAL_SERVER_ERROR) { // Server Error
                            System.out.println("InternalServerError: " + error.getMessage());

                        } else if (error == APIResponse.What3WordsError.NETWORK_ERROR) { // Network Error
                            System.out.println("NetworkError: " + error.getMessage());

                        }
                    }

                    // Obtain a grid section within the provided bounding box
                    GridSection gridSection = api.gridSection(
                            new BoundingBox(
                                    new Coordinates(51.515900, -0.212517),
                                    new Coordinates(51.527649, -0.191746)
                            )
                    ).execute();
                    System.out.println("Grid section: " + gridSection);

                } else {
                    APIResponse.What3WordsError error = autosuggest.getError();

                    if (error == APIResponse.What3WordsError.BAD_CLIP_TO_COUNTRY) { // Invalid country clip is provided
                        System.out.println("BadClipToCountry: " + error.getMessage());

                    } else if (error == APIResponse.What3WordsError.BAD_FOCUS) { // Invalid focus
                        System.out.println("BadFocus: " + error.getMessage());

                    } else if (error == APIResponse.What3WordsError.BAD_N_RESULTS) { // Invalid number of results
                        System.out.println("BadNResults: " + error.getMessage());

                    } else if (error == APIResponse.What3WordsError.INTERNAL_SERVER_ERROR) { // Server Error
                        System.out.println("InternalServerError: " + error.getMessage());

                    } else if (error == APIResponse.What3WordsError.NETWORK_ERROR) { // Network Error
                        System.out.println("NetworkError: " + error.getMessage());

                    } else {
                        System.out.println(error + ": " + error.getMessage());

                    }
                }
            }
        }).start();

    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(mContext, "Permission granted", Toast.LENGTH_SHORT).show();
                    //call_action();
                } else {
                    Toast.makeText(mContext, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
    public String getOrderId() {
        String strOrderId = String.valueOf(listCheckId).replace("[", "").replace("]", "");
        Log.e("IDDDDDDDDDDD", strOrderId);
        return strOrderId;
    }

    public void setList(ArrayList<OrderList> orderList) {
        this.orderList = orderList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList == null ? 0 : orderList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemHomeBinding mBinding;
        public CountDownTimer myCountDownTimer;

        public ViewHolder(ItemHomeBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(Object obj) {
            mBinding.setVariable(BR.orderWrapper, obj);
            mBinding.executePendingBindings();
        }
    }
}

