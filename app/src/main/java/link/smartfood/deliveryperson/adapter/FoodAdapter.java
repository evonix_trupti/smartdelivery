package link.smartfood.deliveryperson.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import java.util.List;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.model.order.FoodList;
import link.smartfood.deliveryperson.databinding.ItemFoodListBinding;
import link.smartfood.deliveryperson.util.EvonixUtil;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.ViewHolder> {
    EvonixUtil mEvonixUtil;
    private Context mContext;
    private List<FoodList> foodList;
    ItemFoodListBinding mBinding;


    public FoodAdapter(Context mContext, List<FoodList> foodList) {
        this.mContext = mContext;
        this.foodList = foodList;
        mEvonixUtil = new EvonixUtil(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_food_list, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FoodList dataModel = foodList.get(position);
        holder.bind(dataModel);
    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemFoodListBinding mBinding;

        public ViewHolder(ItemFoodListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(Object obj) {
            mBinding.setVariable(BR.foodList, obj);
            mBinding.executePendingBindings();
        }
    }
}

