package link.smartfood.deliveryperson.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;

import link.smartfood.deliveryperson.R;
import link.smartfood.deliveryperson.activity.view.dashboard.DashboardActivity;
import link.smartfood.deliveryperson.util.EvonixUtil;
import link.smartfood.deliveryperson.util.font.AwesomeFont;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.DrawerViewHolder> {
    private JSONArray jsonArray;
    private Context mContext;
    private EvonixUtil mEvonixUtil;

    public DrawerAdapter(Context mContext, JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        this.mContext = mContext;
        this.mEvonixUtil = new EvonixUtil(mContext);
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_navigation_drawer, parent, false);
        return new DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DrawerViewHolder holder, final int position) {
        try {
            mEvonixUtil.setSPString(R.string.evonix_util_navigation_selection, "Home");
            holder.title.setText(jsonArray.getJSONObject(position).getString("title"));

            holder.llIcon.setVisibility(View.GONE);
            if (!jsonArray.getJSONObject(position).getString("icon").equals("")) {
                holder.awesomeFontIcon.setText(jsonArray.getJSONObject(position).getString("icon"));
                holder.llIcon.setVisibility(View.VISIBLE);
            }
            holder.llMainRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        mEvonixUtil.setSPString(R.string.evonix_util_navigation_selection, jsonArray.getJSONObject(position).getString("title"));
                        DashboardActivity.h.sendEmptyMessage(position);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    holder.llSelected.setBackgroundColor(mContext.getResources().getColor(R.color.grey_600));

                }
            });
            holder.title.setTag(position);
            holder.llMainRoot.setTag(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        View viewDivider;
        AwesomeFont awesomeFontIcon;
        LinearLayout llMainRoot, llIcon, llSelected;

        public DrawerViewHolder(View itemView) {
            super(itemView);
            llMainRoot = (LinearLayout) itemView.findViewById(R.id.llMainRoot);
            llIcon = (LinearLayout) itemView.findViewById(R.id.llIcon);
            llSelected = (LinearLayout) itemView.findViewById(R.id.llSelected);
            title = (TextView) itemView.findViewById(R.id.title);
            awesomeFontIcon = (AwesomeFont) itemView.findViewById(R.id.awesomeFontIcon);
            viewDivider = (View) itemView.findViewById(R.id.viewDivider);
        }
    }
}
